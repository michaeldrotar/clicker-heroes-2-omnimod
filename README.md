# Clicker Heroes 2 - Omnimod

Omnimod aims to provide an assortment of new features that complement and enhance the core game.

**Note:** While it's always a good idea to backup your characters before using mods, Omnimod takes your saved character data very seriously and works to provide features that won't permanently ruin your data so you can simply remove Omnimod and all core characters should revert to functioning as if Omnimod was never used.


## Getting Started

If this is your first time installing a mod, refer to the official [Installing Mods](https://www.clickerheroes2.com/installing_mods.php) instructions to familiarize yourself with the process and to locate the files and folders on your specific device.

Once you're ready, you can use the link below to download the `.swf` file. Clicker Heroes 2 is still in beta so things can change drastically between versions. To protect your data, each version of Omnimod is targeted at a specific version of the game. Find your game version in the chart below and use the associated link to download the proper version of Omnimod!

| Clicker Heroes 2 Version | Omnimod Version                                 |
| ------------------------:|:----------------------------------------------- |
|                  v0.10.9 | [Download v1.8.5](/../raw/v1.8.5/Omnimod.swf)   |

**Need more help?**

- When you first start Clicker Heroes 2, the version is in the top-left corner of the menu or you can open the menu with the top-right cog button.
- Use the appropriate Download link and place `Omnimod.swf` into your `mods/active` folder.
- Add `Omnimod.swf` to a new line at the end of your `mods.txt` file so the game knows to load it last.
- Launch the game!


## Major Features

### Monsters and AoE Skills

The loading art for the game shows an endless tide of tightly packed monsters moving towards Cid, but the actual gameplay doesn't really capture that feeling.

Monsters now spawn in groups to increase density, lower travel time, and keep a steadier pace. This also helps limit the camera from jarring as much during rapid storms. To combat the groups, Cid has also received some area of effect options to her skills, like Huge Click and Powersurge.

### Ruby Shop

The Ruby Shop is exciting to see, but its options are often lacking and it's easy to find yourself hoarding thousands of rubies.

Omnimod provides new options like skill points and energy drinks. Omnimod also makes it clear that magical brew and energy drinks can be bought more than once and allows them to overflow their resource caps to help encourage new strategies like trying to stack 1k energy before unleashing the longest clickstorm ever!

Omnimod uses a new weighted scale system for Ruby Shop purchase options. Now every purchase is valuable to lower the pool of available items and increase the odds of getting those rarer, more powerful items.


## More Information

To follow released and upcoming changes, or to get more details, please refer to the [Changelog](CHANGELOG.md)

Questions? Bugs? Feedback? Open an [issue](https://gitlab.com/michaeldrotar/clicker-heroes-2-omnimod/issues) or reach me at [/u/michaeldrotar](https://www.reddit.com/user/michaeldrotar) on Reddit. I'm also on the [Clicker Heroes 2 Discord server](http://discord.gg/playsaurus) if you're into that sort of thing!
