package
{
	import ModLoader;
	import omnimodlib.Extension;
	import omnimodlib.Extensions;
	import Hooks;
	import omnimodlib.Util;
	import flash.display.Sprite;
	import heroclickerlib.CH2;
	import heroclickerlib.LevelGraph;
	import heroclickerlib.managers.SoundManager;
	import heroclickerlib.world.CharacterDisplay;
	import models.Character;
	import models.Characters;

	public class BerserkerMain extends Sprite
	{
		public static var instance:BerserkerMain;

		public const modName:String = 'Berserker';
		public const modDescription:String = 'The Berserker possesses incredible speed and force to manipulate her enemies.';
		public const modVersion:Number = 2;
		public const modAuthor:String = 'Michael Drotar';

		public const characterName:String = modName;
		public const characterDescription:String = modDescription;
		public const characterAssetGroupName:String = 'HelpfulAdventurer';

		public const characterFlavorName:String = 'Cid';
		public const characterFlavorClass:String = 'The Berserker';

		public var MOD_INFO:Object = {};

		public var currentCharacter:Character;
		public var extensions:omnimodlib.Extensions;
		public var hooks:Hooks;

		private var _characterTracker:Character;
		private var _trackedProperties:Array = ['level'];

		private var _modLoadOrder:int = -1;

		private var _defaultItemStatIds:Array = []; // generated during game execution
		private var _desiredItemStatIds:Array = [CH2.STAT_BONUS_GOLD_CHANCE, CH2.STAT_BOSS_GOLD, CH2.STAT_CLICKABLE_CHANCE, CH2.STAT_CLICKABLE_GOLD, CH2.STAT_GOLD, CH2.STAT_TREASURE_CHEST_CHANCE, CH2.STAT_TREASURE_CHEST_GOLD];

		public function BerserkerMain()
		{
			instance = this;

			MOD_INFO['name'] = modName;
			MOD_INFO['description'] = modDescription;
			MOD_INFO['version'] = modVersion;
			MOD_INFO['author'] = modAuthor;

			currentCharacter = new Character();
			extensions = new omnimodlib.Extensions();
			hooks = new Hooks();

			extensions.forEach(function(extension:omnimodlib.Extension):void
			{
				call(extension, 'initialize');
			});

			trigger('onExtensionsCreated');
		}

		public function onCharacterDisplayCreated(display:CharacterDisplay):void
		{
			// display.playDash = dashAnimation;
			display.msBetweenStepSounds = 375;

			// Load audio
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/huge_click');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/critical_hit');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/hit');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/get_hit');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/Big Click Activated');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/Big Click Hits');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/clickdash0');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/clickdash1');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/huge_click_activate');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/power_surge_activate');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/reload');
			SoundManager.instance.loadAudioClass('audio/HelpfulAdventurer/energize');
		}

		public function get modLoadOrder():int
		{
			if (_modLoadOrder == -1)
			{
				var modNames:Array = ModLoader.instance.loadedModNames;
				for (var i:int = 0; i < modNames.length; i++)
				{
					if (modNames[i] == modName)
					{
						_modLoadOrder = i + 1;
						break;
					}
				}
			}

			return _modLoadOrder
		}

		public function call(object:Object, methodName:String, ... args):*
		{
			if (methodName in object && typeof(object[methodName]) === 'function')
			{
				return object[methodName].apply(object, args);
			}
		}

		public function trigger(name:String, ... args):void
		{
			args.unshift(name);
			args.unshift(null);
			extensions.forEach(function(extension:omnimodlib.Extension):void
			{
				args[0] = extension
				call.apply(this, args.slice());
			});
		}

		public function onStartup(game:IdleHeroMain):void
		{
			var characterTemplate:Character = new Character();
			characterTemplate.name = characterName;
			characterTemplate.assetGroupName = characterAssetGroupName;
			characterTemplate.flavor = characterDescription;
			characterTemplate.flavorName = characterFlavorName;
			characterTemplate.flavorClass = characterFlavorClass;
			characterTemplate.defaultSaveName = Util.underscore(characterName);
			Characters.startingDefaultInstances[characterName] = characterTemplate;

			characterTemplate.availableForCreation = true;
			characterTemplate.visibleOnCharacterSelect = true;
			characterTemplate.characterSelectOrder = modLoadOrder;

			characterTemplate.startingSkills = [];
			characterTemplate.recommendedLevelsForWorlds = {'1': 0};
			characterTemplate.onCharacterDisplayCreated = null;
			characterTemplate.upgradeableStats = Character.DEFAULT_UPGRADEABLE_STATS;
			characterTemplate.levelGraphNodeTypes = {};
			characterTemplate.levelGraph = new LevelGraph();

			trigger('onCharacterTemplateCreated', characterTemplate);
		}

		public function onCharacterCreated(character:Character):void
		{
			if (character.name == characterName)
			{
				character.modDependencies[modName] = true;
				character.assetGroupName = characterAssetGroupName;
				character.onCharacterDisplayCreated = this['onCharacterDisplayCreated'];

				currentCharacter = character;
				trigger('onCharacterCreated');
				currentCharacter = null;

				hookCharacterUpdate(character);
			}

			var generateCatalog:Function = character.generateCatalog;
			character.generateCatalog = function():void
			{
				var statId:uint;
				if (_defaultItemStatIds.length === 0)
				{
					for (statId = 0; statId < CH2.STATS.length; statId++)
					{
						if (CH2.STATS[statId] && CH2.STATS[statId].appearsInAllItemSlots)
						{
							_defaultItemStatIds.push(statId);
						}
					}
				}
				if (character.name == characterName)
				{
					for (statId = 0; statId < CH2.STATS.length; statId++)
					{
						Util.trace('-- ' + statId + ' ' + _desiredItemStatIds.indexOf(statId));
						CH2.STATS[statId].appearsInAllItemSlots = _desiredItemStatIds.indexOf(statId) !== -1;
					}
				}
				else
				{
					for (statId = 0; statId < CH2.STATS.length; statId++)
					{
						CH2.STATS[statId].appearsInAllItemSlots = _defaultItemStatIds.indexOf(statId) !== -1;
					}
				}
				generateCatalog();
			}
		}

		public function hookCharacterUpdate(character:Character):void
		{
			var hookedUpdate:Function = character.update;
			character.update = function(dt:int):void
			{
				var property:String;
				if (currentCharacter != CH2.currentCharacter)
				{
					currentCharacter = CH2.currentCharacter;
					_characterTracker = new Character();
					for each (property in _trackedProperties)
					{
						_characterTracker[property] = currentCharacter[property];
					}

					trigger('onCharacterStarted');
				}
				for each (property in _trackedProperties)
				{
					if (_characterTracker[property] != currentCharacter[property])
					{
						var oldValue:* = _characterTracker[property];
						var newValue:* = currentCharacter[property];
						_characterTracker[property] = newValue;
						trigger('onCharacterPropertyChanged', {character: currentCharacter, property: property, oldValue: oldValue, newValue: newValue});
					}
				}
				hookedUpdate(dt);
				trigger('onUpdate', dt);
			};
		}

		public function onUICreated():void
		{
			trigger('onUICreated');
		}
	}
}
