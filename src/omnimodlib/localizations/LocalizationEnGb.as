package omnimodlib.localizations
{

	public class LocalizationEnGb
	{
		public var locale:String = 'en-GB';

		public function populate(_:Object):void
		{
			_['formats.meters'] = {one: '{ count } metre', other: '{ count } metres'};
		}
	}
}