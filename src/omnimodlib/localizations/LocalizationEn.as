package omnimodlib.localizations
{
	import com.playsaurus.numbers.BigNumber;

	public class LocalizationEn
	{
		public var locale:String = 'en';

		public function populate(_:Object):void
		{
			_['_pluralsFn'] = function(count:BigNumber, plurals:Object):String
			{
				// NOTE: count can be NaN if the number is formatted to a percentage
				// which may be good because you'd have 1% things rather than
				// 1% thing

				var text:String;
				if (count.eqN(0))
				{
					text = plurals.zero;
				}
				else if (count.eqN(1))
				{
					text = plurals.one;
				}
				if (!text)
				{
					text = plurals.other;
				}
				return text;
			};

			_['labels.autoAttacks'] = {one: '{ count } auto-attack', other: '{ count } auto-attacks'};
			_['labels.autoAttacksPerSecond'] = {one: '{ count } auto-attack per second', other: '{ count } auto-attacks per second'};
			_['labels.clicks'] = {one: '{ count } click', other: '{ count } clicks'};
			_['labels.clicksPerSecond'] = {one: '{ count } click per second', other: '{ count } clicks per second'};
			_['labels.damage'] = '{ count } damage';
			_['labels.energy'] = '{ count } energy';
			_['labels.energyPerAutoAttack'] = '{ count } energy per auto-attack';
			_['labels.energyPerClick'] = '{ count } energy per click';
			_['labels.energyPerSecond'] = '{ count } energy per second';
			_['labels.mana'] = '{ count } mana';
			_['labels.manaPerAutoAttack'] = '{ count } mana per auto-attack';
			_['labels.manaPerClick'] = '{ count } mana per click';
			_['labels.manaPerSecond'] = '{ count } mana per second';
			_['labels.meters'] = {one: '{ count } meter', other: '{ count } meters'};

			_['Autoattackstorm'] = 'Auto-attackstorm';
			_['GoldenClicks'] = 'Golden Clicks';
		}
	}
}