package omnimodlib
{
	import com.playsaurus.random.Random;
	import flash.utils.getQualifiedClassName;
	import heroclickerlib.CH2;
	import models.Character;
	import omnimodlib.Extensions;
	import omnimodlib.managers.OmniSaveManager;
	import omnimodlib.managers.OmniTemplateManager;

	public class Extension
	{
		protected var _uuid:String;

		public function Extension()
		{

		}

		public function get currentCharacter():Character
		{
			return mod.currentCharacter;
		}

		public function get data():Data
		{
			return extensions.data;
		}

		public function get extensions():omnimodlib.Extensions
		{
			return mod.extensions;
		}

		public function get fn():Fn
		{
			return mod.fn;
		}

		public function get uuid():String
		{
			if (!_uuid)
			{
				var baseClassName:String = getQualifiedClassName(this);
				_uuid = Util.toId([modName, baseClassName.replace(/^.+::/, '')]);
			}
			return _uuid;
		}

		public function get mod():OmnimodMain
		{
			return OmnimodMain.instance;
		}

		public function get modName():String
		{
			return OmnimodMain.MOD_NAME;
		}

		public function get modVersion():Number
		{
			return OmnimodMain.MOD_VERSION;
		}

		public function get roller():Random
		{
			return CH2.roller.modRoller;
		}

		public function get saveManager():OmniSaveManager
		{
			return mod.saveManager;
		}

		public function get templateManager():OmniTemplateManager
		{
			return mod.templateManager;
		}

		public function _(text:*, ... args):String
		{
			if (args.length === 0)
			{
				// If no args given, use its own properties by their names
				args = [this];
			}
			return mod._.apply(mod, [].concat(text, args));
		}

		public function after(originalFunction:Function, callbackFunction:Function):Function
		{
			return mod.hooks.after(originalFunction, callbackFunction);
		}

		public function before(originalFunction:Function, callbackFunction:Function):Function
		{
			return mod.hooks.before(originalFunction, callbackFunction);
		}

		public function createTooltip(header:String, body:String):Object
		{
			return {header: header, body: body};
		}

		public function hook(originalFunction:Function, callbackFunction:Function):Function
		{
			return mod.hooks.hook(originalFunction, callbackFunction);
		}

		public function hookOverride(object:*, overrideName:String, callbackFunction:Function):void
		{
			return mod.hooks.hookOverride(object, overrideName, callbackFunction)
		}

		public function trace(... args):void
		{
			Util.trace.apply(Util, args);
		}

		public function traceLine():void
		{
			Util.traceLine();
		}

		public function trigger(name:String, ... args):void
		{
			mod.trigger.apply(mod, [].concat(name, args));
		}
	}
}
