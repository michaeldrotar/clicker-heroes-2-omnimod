package omnimodlib
{
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;
	import ui.CH2UI;

	public class IconViewer extends Extension
	{
		public var updateInterval:int = 8000;
		public var updateTimer:int = updateInterval;
		public var iconCount:int = 24;
		public var iconMin:int = 1;
		public var iconMax:int = 202;
		public var iconCurrent:int = iconMin;

		override public function get id():String
		{
			return modName + '_Next';
		}

		public function updateBuff(offset:int):void
		{
			var buff:Buff;
			buff = new Buff();
			buff.name = 'ICON_' + offset;
			buff.iconId = Math.min(iconCurrent + offset, iconMax);

			buff.duration = 0;
			buff.isUntimedBuff = true;

			buff.tooltipFunction = function():Object
			{
				return {'header': 'Icon Viewer', body: buff.iconId};
			}

			currentCharacter.buffs.addBuff(buff);
		}

		public function updateIcons():void
		{
			for (var i:int = 0; i < iconCount; i++)
			{
				updateBuff(i);
			}
			//currentCharacter.buffs.updateBuffs(1);
			CH2UI.instance.redrawBuffBar();
			iconCurrent += iconCount;
			if (iconCurrent > iconMax)
			{
				iconCurrent = iconMin;
			}
		}

		public function onStartup(game:IdleHeroMain):void
		{
			var nextSkill:Skill = new Skill();
			nextSkill.name = id;
			nextSkill.iconId = 5;
			nextSkill.modName = modName;
			nextSkill.castTime = 0;
			nextSkill.energyCost = 0;
			nextSkill.manaCost = 0;
			nextSkill.cooldown = 0;
			nextSkill.ignoresGCD = true;
			nextSkill.minimumRange = 0;
			nextSkill.maximumRange = 9000;
			nextSkill.tooltipFunction = function():Object
			{
				return {header: 'Next', body: 'Go to next page of icons'};
			};
			nextSkill.effectFunction = function():void
			{
				updateIcons();
			};
			Character.staticSkillInstances[nextSkill.name] = nextSkill;
		}

		public function onCharacterStarted():void
		{
			currentCharacter.activateSkill(id);
			updateIcons();
		}
	}
}
