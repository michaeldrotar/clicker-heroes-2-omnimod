package omnimodlib
{
	import heroclickerlib.LevelGraph;
	import models.Character;
	import models.Characters;

	public class CharacterTemplateExtension extends CoreCharacterTemplateExtension
	{
		public var name:String = '';
		public var className:String = '';
		public var description:String = '';

		public var assetGroupName:String = '';
		public var available:Boolean = false;
		public var visible:Boolean = false;

		public var startingSkills:Array = [];
		public var upgradeableStats:Array = Character.DEFAULT_UPGRADEABLE_STATS;

		public var levelGraphNodeTypes:Object = {};
		public var levelGraph:LevelGraph = new LevelGraph();

		public function CharacterTemplateExtension()
		{
			super();
		}

		public function get defaultSaveName()
		{
			return Util.underscore(templateName);
		}

		public function onStartup(game:IdleHeroMain):void
		{
			var characterTemplate:Character = new Character();
			characterTemplate.name = templateName;
			characterTemplate.flavorName = name;
			characterTemplate.flavorClass = className;
			characterTemplate.flavor = description;

			characterTemplate.assetGroupName = assetGroupName;
			characterTemplate.defaultSaveName = defaultSaveName;

			characterTemplate.availableForCreation = available;
			characterTemplate.visibleOnCharacterSelect = visible;

			characterTemplate.startingSkills = startingSkills;
			characterTemplate.upgradeableStats = upgradeableStats;
			characterTemplate.levelGraphNodeTypes = levelGraphNodeTypes;
			characterTemplate.levelGraph = levelGraph;

			characterTemplate.characterSelectOrder = nextAvailableCharacterSlot();

			Characters.startingDefaultInstances[templateName] = characterTemplate;
		}

		static function nextAvailableCharacterSlot():int
		{
			var highest:int = 0;
			var templates:Object = Characters.startingDefaultInstances;
			var key:String;
			for (key in templates)
			{
				highest = Math.max(highest, templates[key].characterSelectOrder || 0);
			}
			return highest + 1;
		}
	}
}