package omnimodlib
{
	import com.playsaurus.random.Random;
	import models.ExtendedVariables;

	public class OmniExtendedVariables extends ExtendedVariables
	{
		public var roller:Random = new Random();

		public function OmniExtendedVariables()
		{
			super();
			registerDynamicChild('roller', Random);
		}
	}
}