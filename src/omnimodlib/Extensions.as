package omnimodlib
{
	import omnimodlib.Data;
	import omnimodlib.IconViewer;
	import omnimodlib.Migrations;
	import omnimodlib.characters.BerserkerExtension;
	import omnimodlib.characters.berserker.skills.BashExtension;
	import omnimodlib.characters.berserker.skills.BerserkExtension;
	import omnimodlib.core.AutomatorExtension;
	import omnimodlib.core.BuffsExtension;
	import omnimodlib.core.LevelGraphConnectionsOverrideExtension;
	import omnimodlib.core.MonstersExtension;
	import omnimodlib.core.OmniSkillPoints;
	import omnimodlib.systems.OmniAutomatorExtension;
	import omnimodlib.ui.OmniSkillsSubPanelExtension;
	import omnimodlib.core.OmniStatPoints;
	import omnimodlib.core.SkillsExtension;
	import omnimodlib.core.character.AutomatorPoints;
	import omnimodlib.core.character.Energy;
	import omnimodlib.core.character.Gilding;
	import omnimodlib.core.character.GildingNodeUnlockerExtension;
	import omnimodlib.core.character.LevelGraphExtension;
	import omnimodlib.core.character.Mana;
	import omnimodlib.core.RubyShop;
	import berserkerlib.SkillTree;
	import berserkerlib.character.CharacterAttack;
	import berserkerlib.character.CharacterMonsters;
	import berserkerlib.gems.items.BuyCheapestItemGem;
	import berserkerlib.gems.items.UpgradeCheapestItemFullGem;
	import berserkerlib.gems.items.UpgradeCheapestItemHalfGem;
	import berserkerlib.gems.skills.BashSkillGem;
	import berserkerlib.gems.skills.CleaveSkillGem;
	import berserkerlib.gems.skills.EviscerateSkillGem;
	import berserkerlib.gems.skills.RuptureSkillGem;
	import berserkerlib.gems.skills.ShoveSkillGem;
	import berserkerlib.gems.skills.SliceAndDiceSkillGem;
	import berserkerlib.nodeTypes.IncreasePower;
	import berserkerlib.nodeTypes.IncreasePowerBig;
	import berserkerlib.nodeTypes.IncreasePowerSuper;
	import berserkerlib.nodeTypes.IncreasePrecision;
	import berserkerlib.nodeTypes.IncreasePrecisionBig;
	import berserkerlib.nodeTypes.IncreasePrecisionSuper;
	import berserkerlib.nodeTypes.IncreaseSpeed;
	import berserkerlib.nodeTypes.IncreaseSpeedBig;
	import berserkerlib.nodeTypes.IncreaseSpeedSuper;
	import berserkerlib.rubyShop.bonuses.LevelUpBonus;
	import omnimodlib.core.character.SkillPoints;
	import omnimodlib.core.monsters.MonsterSpawner;
	import omnimodlib.core.rubyShop.BagOfGoldPurchaseExtension;
	import omnimodlib.core.rubyShop.LuckRunePurchaseExtension;
	import omnimodlib.core.rubyShop.PowerRunePurchaseExtension;
	import omnimodlib.core.rubyShop.SkillPointPurchase;
	import berserkerlib.rubyShop.bonuses.SkillTreeResetBonus;
	import berserkerlib.rubyShop.catalogs.ItemGemCatalog;
	import berserkerlib.rubyShop.catalogs.SkillGemCatalog;
	import berserkerlib.rubyShop.catalogs.TimingStoneCatalog;
	import berserkerlib.skills.Bash;
	import berserkerlib.skills.Berserk;
	import berserkerlib.skills.Cleave;
	import berserkerlib.skills.Eviscerate;
	import berserkerlib.skills.Rupture;
	import berserkerlib.skills.Shove;
	import berserkerlib.skills.SliceAndDice;
	import berserkerlib.stats.Power;
	import berserkerlib.stats.Precision;
	import berserkerlib.stats.Speed;
	import berserkerlib.stones.timing.AlwaysStone;
	import berserkerlib.stones.timing.LongWaitStone;
	import berserkerlib.stones.timing.LongerWaitStone;
	import berserkerlib.stones.timing.LongestWaitStone;
	import berserkerlib.stones.timing.MediumWaitStone;
	import berserkerlib.stones.timing.ShortWaitStone;
	import berserkerlib.stones.timing.ShorterWaitStone;
	import berserkerlib.stones.timing.ShortestWaitStone;
	import berserkerlib.talents.Fury;
	import berserkerlib.talents.Rage;
	import berserkerlib.ui.StatPanel;
	import flash.utils.describeType;
	import omnimodlib.core.rubyShop.AncientShardPurchase;
	import omnimodlib.core.rubyShop.AutomatorPointPurchase;
	import omnimodlib.core.rubyShop.EnergyDrinkPurchase;
	import omnimodlib.core.rubyShop.MagicalBrewPurchase;
	import omnimodlib.core.rubyShop.SpeedRunePurchaseExtension;
	import omnimodlib.core.rubyShop.TimeMetalDetectorPurchaseExtension;
	import omnimodlib.core.rubyShop.ZoneMetalDetectorPurchaseExtension;
	import omnimodlib.ui.OmniAutomatorSubPanelExtension;
	import omnimodlib.ui.OmniStatsPanel;
	import omnimodlib.helpfulAdventurer.HelpfulAdventurer;
	import omnimodlib.helpfulAdventurer.StormsExtension;
	import omnimodlib.helpfulAdventurer.skills.AutoAttackstormExtension;
	import omnimodlib.helpfulAdventurer.skills.BigClicksExtension;
	import omnimodlib.helpfulAdventurer.skills.ClickstormExtension;
	import omnimodlib.helpfulAdventurer.skills.ClicktorrentExtension;
	import omnimodlib.helpfulAdventurer.skills.CritstormExtension;
	import omnimodlib.helpfulAdventurer.skills.EnergizeExtension;
	import omnimodlib.helpfulAdventurer.skills.GoldenClicksExtension;
	import omnimodlib.helpfulAdventurer.skills.HugeClickExtension;
	import omnimodlib.helpfulAdventurer.skills.ManaCritExtension;
	import omnimodlib.helpfulAdventurer.skills.ManagizeExtension;
	import omnimodlib.helpfulAdventurer.skills.MultiClickExtension;
	import omnimodlib.helpfulAdventurer.skills.PowersurgeExtension;
	import omnimodlib.helpfulAdventurer.skills.ReloadExtension;
	import omnimodlib.characters.Devourer;

	public class Extensions
	{
		public var data:Data = new Data();
		public var migrations:Migrations = new Migrations();
		public var characterPropertyTracker:CharacterPropertyTracker = new CharacterPropertyTracker();

		// Systems
		public var omniAutomator:OmniAutomatorExtension = new OmniAutomatorExtension();

		// Core
		public var omniSkillPoints:OmniSkillPoints = new OmniSkillPoints();
		public var omniStatPoints:OmniStatPoints = new OmniStatPoints();

		public var automator:AutomatorExtension = new AutomatorExtension();
		public var buffs:BuffsExtension = new BuffsExtension();
		public var monsters:MonstersExtension = new MonstersExtension();
		public var skills:SkillsExtension = new SkillsExtension();

		public var levelGraphConnectionsOverride:LevelGraphConnectionsOverrideExtension = new LevelGraphConnectionsOverrideExtension();
		//public var gildingNodeUnlocker:GildingNodeUnlockerExtension = new GildingNodeUnlockerExtension();

		//public var characterAttack:CharacterAttack = new CharacterAttack();
		public var automatorPoints:AutomatorPoints = new AutomatorPoints();
		public var energy:Energy = new Energy();
		public var levelGraph:LevelGraphExtension = new LevelGraphExtension();
		public var mana:Mana = new Mana();
		public var skillPoints:SkillPoints = new SkillPoints();
		//public var gilding:Gilding = new Gilding();
		//public var characterMonsters:CharacterMonsters = new CharacterMonsters();

		public var monsterSpawner:MonsterSpawner = new MonsterSpawner();

		//public var bashSkillGem:BashSkillGem = new BashSkillGem();
		//public var shoveSkillGem:ShoveSkillGem = new ShoveSkillGem();
		//public var cleaveSkillGem:CleaveSkillGem = new CleaveSkillGem();
		////public var ruptureSkillGem:RuptureSkillGem = new RuptureSkillGem();
		////public var sliceAndDiceSkillGem:SliceAndDiceSkillGem = new SliceAndDiceSkillGem();
		////public var eviscerateSkillGem:EviscerateSkillGem = new EviscerateSkillGem();
		//public var skillGemCatalog:SkillGemCatalog = new SkillGemCatalog();

		//public var buyCheapestItemGem:BuyCheapestItemGem = new BuyCheapestItemGem();
		//public var upgradeCheapestItemFullGem:UpgradeCheapestItemFullGem = new UpgradeCheapestItemFullGem();
		//public var upgradeCheapestItemHalfGem:UpgradeCheapestItemHalfGem = new UpgradeCheapestItemHalfGem();
		//public var itemGemCatalog:ItemGemCatalog = new ItemGemCatalog();

		//public var alwaysStone:AlwaysStone = new AlwaysStone();
		//public var shortestWaitStone:ShortestWaitStone = new ShortestWaitStone();
		//public var shorterWaitStone:ShorterWaitStone = new ShorterWaitStone();
		//public var shortWaitStone:ShortWaitStone = new ShortWaitStone();
		//public var mediumWaitStone:MediumWaitStone = new MediumWaitStone();
		//public var longWaitStone:LongWaitStone = new LongWaitStone();
		//public var longerWaitStone:LongerWaitStone = new LongerWaitStone();
		//public var longestWaitStone:LongestWaitStone = new LongestWaitStone();
		//public var timingStoneCatalog:TimingStoneCatalog = new TimingStoneCatalog();

		//public var ancientShardPurchase:AncientShardPurchase = new AncientShardPurchase();
		//public var automatorPointPurchase:AutomatorPointPurchase = new AutomatorPointPurchase();
		//public var skillPointPurchase:SkillPointPurchase = new SkillPointPurchase();
		//public var luckRunePurchase:LuckRunePurchaseExtension = new LuckRunePurchaseExtension();
		//public var powerRunePurchase:PowerRunePurchaseExtension = new PowerRunePurchaseExtension();
		//public var speedRunePurchase:SpeedRunePurchaseExtension = new SpeedRunePurchaseExtension();
		//public var timeMetalDetectorPurchase:TimeMetalDetectorPurchaseExtension = new TimeMetalDetectorPurchaseExtension();
		//public var zoneMetalDetectorPurchase:ZoneMetalDetectorPurchaseExtension = new ZoneMetalDetectorPurchaseExtension();
		//public var bagOfGoldPurchase:BagOfGoldPurchaseExtension = new BagOfGoldPurchaseExtension();
		//public var energyDrink:EnergyDrinkPurchase = new EnergyDrinkPurchase();
		//public var magicalBrew:MagicalBrewPurchase = new MagicalBrewPurchase();

		//public var levelUpBonus:LevelUpBonus = new LevelUpBonus();
		//public var skillPointBonus:SkillPointBonus = new SkillPointBonus();
		//public var skillTreeResetBonus:SkillTreeResetBonus = new SkillTreeResetBonus();

		//public var rubyShop:RubyShop = new RubyShop();

		public var helpfulAdventurer:HelpfulAdventurer = new HelpfulAdventurer();
		//public var storms:StormsExtension = new StormsExtension();

		//public var multiClick:MultiClickExtension = new MultiClickExtension();
		//public var bigClicks:BigClicksExtension = new BigClicksExtension();
		//public var hugeClick:HugeClickExtension = new HugeClickExtension();
		//public var manaCrit:ManaCritExtension = new ManaCritExtension();
		//public var clickstorm:ClickstormExtension = new ClickstormExtension();
		//public var critstorm:CritstormExtension = new CritstormExtension();
		//public var goldenClicks:GoldenClicksExtension = new GoldenClicksExtension();
		//public var autoAttackstorm:AutoAttackstormExtension = new AutoAttackstormExtension();
		//public var clicktorrent:ClicktorrentExtension = new ClicktorrentExtension();
		//public var energize:EnergizeExtension = new EnergizeExtension();
		//public var managize:ManagizeExtension = new ManagizeExtension();
		//public var reload:ReloadExtension = new ReloadExtension();
		//public var powersurge:PowersurgeExtension = new PowersurgeExtension();

		//public var rage:Rage = new Rage();
		//public var fury:Fury = new Fury();

		//public var power:Power = new Power();
		//public var precision:Precision = new Precision();
		//public var speed:Speed = new Speed();

		//public var increasePower:IncreasePower = new IncreasePower();
		//public var increasePowerBig:IncreasePowerBig = new IncreasePowerBig();
		//public var increasePowerSuper:IncreasePowerSuper = new IncreasePowerSuper();
		//public var increaseSpeed:IncreaseSpeed = new IncreaseSpeed();
		//public var increaseSpeedBig:IncreaseSpeedBig = new IncreaseSpeedBig();
		//public var increaseSpeedSuper:IncreaseSpeedSuper = new IncreaseSpeedSuper();
		//public var increasePrecision:IncreasePrecision = new IncreasePrecision();
		//public var increasePrecisionBig:IncreasePrecisionBig = new IncreasePrecisionBig();
		//public var increasePrecisionSuper:IncreasePrecisionSuper = new IncreasePrecisionSuper();

		//public var skillTree:SkillTree = new SkillTree();

		////public var statPanel:StatPanel = new StatPanel();

		// Skills
		public var bash:BashExtension = new BashExtension();
		public var berserk:BerserkExtension = new BerserkExtension();

		// UI
		public var omniAutomatorSubPanel:OmniAutomatorSubPanelExtension = new OmniAutomatorSubPanelExtension();
		public var omniStatsPanel:OmniStatsPanel = new OmniStatsPanel();
		public var omniSkillsSubPanel:OmniSkillsSubPanelExtension = new OmniSkillsSubPanelExtension();

		// Characters
		public var berserker:BerserkerExtension = new BerserkerExtension();
		public var devourer:Devourer = new Devourer();


		//public var iconViewer:IconViewer = new IconViewer();

		private var _extensionKeys:Array;

		public function Extensions()
		{

		}

		public function get extensionKeys():Array
		{
			if (!_extensionKeys)
			{
				var name:String;
				var pos:Number;
				var index:int;

				var data:Array = [];
				var variablesXml:XMLList = describeType(this)..variable;
				for (index = 0; index < variablesXml.length(); index++)
				{
					name = variablesXml[index].@name;
					pos = parseInt(variablesXml[index]..metadata..arg.@value, 10);
					if (isNaN(pos)) // safety check
					{
						pos = index;
					}
					data.push({name: name, pos: pos});
				}
				data.sortOn('pos', Array.NUMERIC);

				_extensionKeys = [];
				for (index = 0; index < data.length; index++)
				{
					_extensionKeys.push(data[index]['name']);
				}
			}
			return _extensionKeys;
		}

		public function forEach(callback:Function):void
		{
			for (var i:uint = 0; i < extensionKeys.length; i++)
			{
				callback(this[extensionKeys[i]]);
			}
		}
	}
}
