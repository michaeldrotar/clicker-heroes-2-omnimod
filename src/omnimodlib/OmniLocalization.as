package omnimodlib
{
	import com.playsaurus.numbers.BigNumber;
	import omnimodlib.OmniStringUtil;
	import omnimodlib.localizations.LocalizationEn;
	import omnimodlib.localizations.LocalizationEnGb;

	public class OmniLocalization
	{
		private static var _instance:OmniLocalization;

		private var _currentLocale:String;
		private var _localizations:Object;

		public function OmniLocalization()
		{
			loadLocale('en'); // Hard-code to `en` for now, later can call a method or read a config to get it
		}

		public static function get instance():OmniLocalization
		{
			_instance ||= new OmniLocalization();
			return _instance;
		}

		public function get currentLocale():String
		{
			return _currentLocale;
		}

		public function loadLocale(locale:String):void
		{
			_currentLocale = locale; // Store for now, later can write to a config

			_localizations = {};

			// Load default localization
			var en:LocalizationEn = new LocalizationEn();
			en.populate(_localizations);

			// Load localizations (order matters, latter ones override earlier ones)
			var localization:Object;
			var localizations:Array = [];
			localizations.push(new LocalizationEnGb());

			var index:uint;
			for (index = 0; index < localizations.length; index++)
			{
				localization = localizations[index];

				// `en-US` should load `en` and `en-US`, `en` should not load `en-US`
				if (locale.indexOf(localization['locale']) === 0)
				{
					localization['populate'](_localizations);
				}
			}

			// If called while game is running, extensions may need a way to refresh things in future
			OmnimodMain.instance.trigger('onLocalizationsLoaded');
		}

		public function localize(text:*, ... args):String
		{
			if (text is String)
			{
				// Handles keys like `localize('gildingHelpDescription')`
				// Handles raw text like `localize('Click to attack.')`
				if (_localizations[text])
				{
					text = _localizations[text];
				}
			}

			// Handles raw plurals like `localize({ one: '{ count} thing', other: '{ count } things'}, {count: 1})`
			// Handles plurals from keys like `localize('formats.meters', {count: 1})`
			if (args[0] && 'count' in args[0])
			{
				// Handles succinctly defining plurals with same singular form like `"1 energy"` or `"2 energy"`
				if (text is String)
				{
					text = {other: text};
				}

				var plurals:Object = (text as Object);
				var count:BigNumber = new BigNumber(args[0].count); // must pass an object with a `count` key to use plurals
				var other:String = plurals.other;

				// Handles if `localize` was passed a raw plural object to start with rather than a key, find the localization by the `other` key
				if (_localizations[other])
				{
					if (_localizations[other] is String)
					{
						plurals = {other: _localizations[other]};
					}
					else
					{
						plurals = _localizations[other];
					}
				}

				// Need to pluralize based on how the count displays rather than its real value.
				// For instance: "1.01 things" is correct, but 1.001 will display as "1" and "1 things" is not correct.
				if (count.ltN(OmniStringUtil.NUMBER_THRESHOLD))
				{
					count = OmniStringUtil.instance.roundNumber(count);
				}

				// Different localizations handle plurals differently.
				// English commonly has `one` or `other` (or `zero` for a special message like "No things" instead of "0 things")
				// Some languages have `two`, `few`, or `many`
				// The pluralsFn can use the given count and plurals object to determine which key to use
				// Every pluralization should provide an `other` as a last resort fallback
				text = _localizations._pluralsFn(count, plurals);
			}

			return OmniStringUtil.instance.formatString.apply(this, [].concat(text, args));
		}
	}
}