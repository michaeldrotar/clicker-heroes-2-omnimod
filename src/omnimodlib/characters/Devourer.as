package omnimodlib.characters
{
	import com.playsaurus.utils.ServerTimeKeeper;
	import heroclickerlib.CH2;
	import heroclickerlib.LevelGraph;
	import models.Character;
	import models.Characters;
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import ui.CH2UI;

	// NOTES from HelpfulAdventurer:
	// 347/656 nodes are for stats (52.89%), totalling 485 stat points available, or ~0.74 stat points per node
	// 282/347 award 1 stat point (81.27%)
	// 65/347 award multiple stat points (18.73%), totalling 203/485 (41.86%) stat points available
	// ~150 levels per gild, gaining ~150/656 (22.87%) of the available nodes, or ~111 stat points per gild

	// WEIGHTS:
	// STAT_BONUS_GOLD_CHANCE: 15
	// STAT_CLICKABLE_CHANCE: 29
	// STAT_CLICKABLE_GOLD: 23
	// STAT_CLICK_DAMAGE: 36
	// STAT_CRIT_CHANCE: 19
	// STAT_CRIT_DAMAGE: 55
	// STAT_GOLD: 49
	// STAT_HASTE: 58
	// STAT_ITEM_BACK_DAMAGE: 8
	// STAT_ITEM_CHEST_DAMAGE: 8
	// STAT_ITEM_COST_REDUCTION: 27
	// STAT_ITEM_FEET_DAMAGE: 7
	// STAT_ITEM_HANDS_DAMAGE: 8
	// STAT_ITEM_HEAD_DAMAGE: 8
	// STAT_ITEM_LEGS_DAMAGE: 8
	// STAT_ITEM_RING_DAMAGE: 8
	// STAT_ITEM_WEAPON_DAMAGE: 8
	// STAT_MANA_REGEN: 21
	// STAT_MONSTER_GOLD: 15
	// STAT_TOTAL_ENERGY: 23
	// STAT_TOTAL_MANA: 24
	// STAT_TREASURE_CHEST_CHANCE: 11
	// STAT_TREASURE_CHEST_GOLD: 17

	// GOLDEN AMOUNTS:
	// STAT_BONUS_GOLD_CHANCE: 3
	// STAT_CLICKABLE_CHANCE: 3
	// STAT_CLICKABLE_GOLD: 3
	// STAT_CLICK_DAMAGE: 3
	// STAT_CRIT_DAMAGE: 3
	// STAT_GOLD: 3
	// STAT_HASTE: 3
	// STAT_ITEM_COST_REDUCTION: 3
	// STAT_MANA_REGEN: 3
	// STAT_MONSTER_GOLD: 3
	// STAT_TOTAL_ENERGY: 4
	// STAT_TOTAL_MANA: 4
	// STAT_TREASURE_CHEST_CHANCE: 3
	// STAT_TREASURE_CHEST_GOLD: 3

	// UNAVAILABLE FOR GOLD:
	// STAT_CRIT_CHANCE: 1
	// STAT_ITEM_BACK_DAMAGE: 1
	// STAT_ITEM_CHEST_DAMAGE: 1
	// STAT_ITEM_FEET_DAMAGE: 1
	// STAT_ITEM_HANDS_DAMAGE: 1
	// STAT_ITEM_HEAD_DAMAGE: 1
	// STAT_ITEM_LEGS_DAMAGE: 1
	// STAT_ITEM_RING_DAMAGE: 1
	// STAT_ITEM_WEAPON_DAMAGE: 1

	// 246/656 nodes are for traits (37.50%), totalling 306 trait points available, or ~0.47 trait points per node
	// 216/246 award 1 trait point (87.80%)
	// 30/246 award multiple trait points (12.20%), totalling 90/306 (29.41%) trait points available
	// ~150 levels per gild, gaining ~150/656 (22.87%) of the available nodes, or ~71 trait points per gild

	// SKILLS
	// Autoattackstorm

	// Big Clicks
	//   CurseOfTheJuggernaut: 1
	//   DistributedBigClicks: 1 (=> Small Clicks)
	//     DistributedBigClicksScaling: 1
	//   LimitlessBigClicks: 1
	//   BigClickStacks: 16
	//   BigClicksDamage: 59

	// Clickstorm
	//   => Clicktorrent
	//     Stormbringer: 1
	//   => Critstorm
	//   => GoldenClicks
	//   HastenClickstorm: 21

	// Energize
	//   => Managize
	//   HastenEnergize: 10
	//   ImprovedEnergize: 10

	// Huge Click
	//   HecatonsEcho: 1
	//   HugeClickDiscount: 1
	//   HugeClickDamage: 55

	// Mana Crit
	//   BhaalsRise: 1 (reduce cooldown on crit)
	//   ImprovedManaCrit: 1 (crit chance increases damage, chance to refund mana)
	//   ManaCritOverflow: 1
	//   ManaCritDamage: 31

	// MultiClick
	//   Flurry: 1
	//   ExtraMulticlicks: 29

	// Powersurge
	//   CritKillPowerSurgeCooldown: 1
	//   PowerSurgeCritChance: 1
	//   HastenPowersurge: 8
	//   ImprovedPowersurge: 10
	//   SustainedPowersurge: 12

	// Reload
	//   Preload: 1
	//   QuickReload: 1
	//   ReloadRampage: 1
	//   HastenReload: 11
	//   ImprovedReload: 10

	// ONE-TIME TRAITS
	//   AutoAttackCritMana: 1
	//   Discharge: 1
	//   HighEnergyGoldBonus: 1
	//   KillingFrenzy: 1
	//   LimitlessHaste: 1
	//   LowEnergyDamageBonus: 1
	//   SpendManaHaste: 1
	//   Synchrony: 1

	public class Devourer extends Extension
	{
		public static const ASSET_GROUP_NAME:String = 'HelpfulAdventurer';
		public static const CHARACTER_NAME:String = 'Devourer';

		public var devourerTemplate:Character;

		public var statLevels:Array;

		private var _statsData:Array;

		public function Devourer()
		{
			super();

			this.statLevels = [];
			this.statLevels.push({id: CH2.STAT_CLICK_DAMAGE, weight: 36});
			this.statLevels.push({id: CH2.STAT_CRIT_CHANCE, weight: 19});
			this.statLevels.push({id: CH2.STAT_CRIT_DAMAGE, weight: 55});
			this.statLevels.push({id: CH2.STAT_HASTE, weight: 58});
			this.statLevels.push({id: CH2.STAT_TOTAL_ENERGY, weight: 23});
			this.statLevels.push({id: CH2.STAT_TOTAL_MANA, weight: 24});
			this.statLevels.push({id: CH2.STAT_MANA_REGEN, weight: 21});
			this.statLevels.push({id: CH2.STAT_MOVEMENT_SPEED});

			this.statLevels.push({id: CH2.STAT_GOLD, weight: 49});
			this.statLevels.push({id: CH2.STAT_BONUS_GOLD_CHANCE, weight: 15});
			this.statLevels.push({id: CH2.STAT_MONSTER_GOLD, weight: 15});
			this.statLevels.push({id: CH2.STAT_CLICKABLE_GOLD, weight: 23});
			this.statLevels.push({id: CH2.STAT_CLICKABLE_CHANCE, weight: 29});
			this.statLevels.push({id: CH2.STAT_TREASURE_CHEST_CHANCE, weight: 11});
			this.statLevels.push({id: CH2.STAT_TREASURE_CHEST_GOLD, weight: 17});
			this.statLevels.push({id: CH2.STAT_ITEM_COST_REDUCTION, weight: 27});

			this.statLevels.push({id: CH2.STAT_ITEM_WEAPON_DAMAGE, weight: 8});
			this.statLevels.push({id: CH2.STAT_ITEM_HEAD_DAMAGE, weight: 8});
			this.statLevels.push({id: CH2.STAT_ITEM_CHEST_DAMAGE, weight: 8});
			this.statLevels.push({id: CH2.STAT_ITEM_RING_DAMAGE, weight: 8});
			this.statLevels.push({id: CH2.STAT_ITEM_LEGS_DAMAGE, weight: 8});
			this.statLevels.push({id: CH2.STAT_ITEM_HANDS_DAMAGE, weight: 8});
			this.statLevels.push({id: CH2.STAT_ITEM_FEET_DAMAGE, weight: 7});
			this.statLevels.push({id: CH2.STAT_ITEM_BACK_DAMAGE, weight: 8});
		}

		public function onCreateCharacterTemplates():void
		{
			this.devourerTemplate = new Character();

			this.devourerTemplate.name = Devourer.CHARACTER_NAME;
			this.devourerTemplate.flavorName = 'Dy';
			this.devourerTemplate.flavorClass = 'Devourer of Worlds';
			this.devourerTemplate.flavor = "Travels from world to world, devouring all life.";
			this.devourerTemplate.characterSelectOrder = Util.keys(Characters.startingDefaultInstances).length + 1;
			this.devourerTemplate.availableForCreation = true;
			this.devourerTemplate.visibleOnCharacterSelect = false;
			this.devourerTemplate.defaultSaveName = "devourer";
			this.devourerTemplate.startingSkills = [];
			this.devourerTemplate.upgradeableStats = Character.DEFAULT_UPGRADEABLE_STATS;
			this.devourerTemplate.assetGroupName = Devourer.ASSET_GROUP_NAME;
			this.devourerTemplate.gildStartBuild = [];

			this.devourerTemplate.levelGraph = new LevelGraph(); // unused but game errors if left undefined

			Characters.startingDefaultInstances[Devourer.CHARACTER_NAME] = this.devourerTemplate;
		}

		public function onDevourerCharacterCreated():void
		{
			this.extensions.omniStatPoints.totalStatPoints = this.currentCharacter.level;
			this.extensions.omniSkillPoints.totalSkillPoints = this.currentCharacter.level;

			this.currentCharacter.onWorldFinishedHandler = this;
		}

		public function onCharacterStarted():void
		{
			if (this.currentCharacter.name == Devourer.CHARACTER_NAME)
			{
			}
		}

		public function onDevourerUICreated():void
		{
		}

		public function onUICreated():void
		{
			if (this.currentCharacter.name !== Devourer.CHARACTER_NAME)
			{
				return;
			}

			CH2UI.instance.mainUI.mainPanel.unregisterTab(1);
			this.extensions.omniStatsPanel.register({position: 1, statLevels: this.statLevels});
			CH2UI.instance.mainUI.mainPanel.unregisterTab(2);
			CH2UI.instance.mainUI.mainPanel.unregisterTab(3);
		}

		public function onWorldFinishedOverride():void
		{
			var slot:int = 0;
			this.currentCharacter.didFinishWorld = true;
			this.currentCharacter.highestMonstersKilled[this.currentCharacter.currentWorldId] = 0;
			if (this.currentCharacter.runsCompletedPerWorld.hasOwnProperty(this.currentCharacter.currentWorldId))
			{
				this.currentCharacter.runsCompletedPerWorld[this.currentCharacter.currentWorldId]++;
			}
			else
			{
				this.currentCharacter.runsCompletedPerWorld[this.currentCharacter.currentWorldId] = 1;
			}
			if (this.currentCharacter.currentWorldId > this.currentCharacter.highestWorldCompleted)
			{
				this.currentCharacter.highestWorldCompleted = this.currentCharacter.currentWorldId;
					//this.currentCharacter.shouldShowNewEtherealItemPopup = true;
					//if (this.currentCharacter.highestWorldCompleted <= 4)
					//{
					//slot = 7 - 2 * (this.currentCharacter.highestWorldCompleted - 1);
					//this.currentCharacter.etherealItemIndiciesForPopup.push(this.currentCharacter.addEtherealItemToInventory(this.currentCharacter.rollEtherealItem(this.currentCharacter.gilds, slot)));
					//this.currentCharacter.etherealItemIndiciesForPopup.push(this.currentCharacter.addEtherealItemToInventory(this.currentCharacter.rollEtherealItem(this.currentCharacter.gilds, slot - 1)));
					//}
					//else
					//{
					//this.currentCharacter.etherealItemIndiciesForPopup.push(this.currentCharacter.addEtherealItemToInventory(this.currentCharacter.rollEtherealItem(this.currentCharacter.gilds)));
					//this.currentCharacter.etherealItemIndiciesForPopup.push(this.currentCharacter.addEtherealItemToInventory(this.currentCharacter.rollEtherealItem(this.currentCharacter.gilds)));
					//}
			}
			if (this.currentCharacter.fastestWorldTimes.hasOwnProperty(this.currentCharacter.currentWorldId))
			{
				if (this.currentCharacter.fastestWorldTimes[this.currentCharacter.currentWorldId] > this.currentCharacter.timeSinceMostRecentRunBegan)
				{
					this.currentCharacter.fastestWorldTimes[this.currentCharacter.currentWorldId] = this.currentCharacter.timeSinceMostRecentRunBegan;
				}
			}
			else
			{
				this.currentCharacter.fastestWorldTimes[this.currentCharacter.currentWorldId] = this.currentCharacter.timeSinceMostRecentRunBegan;
			}
			CH2.user.remoteStatsTracking.addEvent({"type": "finishRun", "world": this.currentCharacter.currentWorldId, "runsInPreviousWorld": this.currentCharacter.runsCompletedPerWorld[this.currentCharacter.currentWorldId], "startTimestamp": ServerTimeKeeper.instance.secondsTimestamp - (CH2.user.totalMsecsPlayed - this.currentCharacter.timeOfLastRun) / 1000, "endTimestamp": ServerTimeKeeper.instance.secondsTimestamp, "characterLevel": this.currentCharacter.level, "ancientShardsPurchased": this.currentCharacter.ancientShards});
		}
	}
}
