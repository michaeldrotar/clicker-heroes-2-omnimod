package omnimodlib.characters.berserker.skills
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import models.Buff;
	import models.Character;
	import omnimodlib.Extension;
	import omnimodlib.Util;

	public class BerserkExtension extends Extension
	{
		public const ID:String = 'OMNIMOD_BERSERK';
		public const NAME:String = 'Berserk';

		public function BerserkExtension()
		{
			super();
		}

		public function addBuff():void
		{
			currentCharacter.buffs.addBuff(createBuff());
		}

		public function createBuff():Buff
		{
			var berserk:Buff = extensions.buffs.createBuff(ID);
			berserk.iconId = 21;
			berserk.tooltipFunction = getTooltip;
			berserk.isUntimedBuff = true;
			berserk.tickRate = 1000 / 60;
			berserk.unhastened = true;
			berserk.tickFunction = onTick;
			berserk.buffStat(CH2.STAT_GOLD, 0.1);
			berserk.onGoldGainedFunction = onGoldGained;
			return berserk;
		}

		public function resetBuff():void
		{
			currentCharacter.unarmedDamage = new BigNumber(1);
		}

		public function getTooltip():Object
		{
			return extensions.buffs.createTooltip(_(NAME), '');
		}

		public function onGoldGained(goldGained:BigNumber):void
		{
			var modifiedGoldReward:BigNumber = BigNumber.clone(goldGained);

			// Undo world bonus
			modifiedGoldReward = modifiedGoldReward.divide(currentCharacter.currentWorld.costMultiplier);

			// Raise to the 0.97 power (BigNumber#pow only works with integer powers)
			modifiedGoldReward.base = Math.pow(modifiedGoldReward.base, 0.97);
			modifiedGoldReward.power *= 0.97;

			// Add gold to damage
			CH2.currentCharacter.unarmedDamage.plusEquals(modifiedGoldReward);

			// Fix the power back into a whole number
			var exponentDecimal:Number = currentCharacter.unarmedDamage.power % 1;
			currentCharacter.unarmedDamage.base *= Math.pow(10, exponentDecimal);
			currentCharacter.unarmedDamage.power = Math.floor(currentCharacter.unarmedDamage.power);

			// Normalize for safety
			currentCharacter.unarmedDamage.normalize();
		}

		public function onTick():void
		{

		}
	}
}