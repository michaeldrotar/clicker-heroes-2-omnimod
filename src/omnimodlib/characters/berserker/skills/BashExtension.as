package omnimodlib.characters.berserker.skills
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import omnimodlib.systems.omniAutomator.OmniAutomatorControlNode;

	public class BashExtension extends Extension
	{
		public const ID:String = 'OMNIMOD_BASH';
		public const NAME:String = 'Bash';
		public const DESCRIPTION:String = 'Bashes the nearest target for { 0 | percent | label:damage }.\nDeals an additional { 1 | percent | label:damage } at { 2 | label:energy }.';

		public const BASE_DAMAGE_MULTIPLIER:Number = 3;
		public const BONUS_DAMAGE_MULTIPLIER:Number = 3;
		public const ENERGY_BONUS:Number = 3;
		public const ENERGY_THRESHOLD:Number = 100;

		public function BashExtension()
		{
			super();
		}

		public function onStartup(game:IdleHeroMain):void
		{
			var bash:Skill = extensions.skills.createSkill(ID);
			bash.iconId = 19;
			bash.tooltipFunction = getTooltip;
			bash.effectFunction = performEffect;

			bash.castTime = 0;
			bash.cooldown = 3 * 1000;
			bash.ignoresGCD = true;
			bash.minimumRange = 0;
			bash.maximumRange = 9000;

			bash.energyCost = -ENERGY_BONUS;
			bash.manaCost = 0;
			bash.usesMaxEnergy = false;
			bash.usesMaxMana = false;
			extensions.skills.register(bash);
			extensions.skills.registerRefresh(ID, refresh);

			var bashControlNode:OmniAutomatorControlNode = extensions.omniAutomator.createControlNodeForSkill(bash);
			extensions.omniAutomator.registerControlNode(bashControlNode);
		}

		public function getDamageMultiplier():Number
		{
			return BASE_DAMAGE_MULTIPLIER;
		}

		public function getBonusDamageMultiplier():Number
		{
			return BONUS_DAMAGE_MULTIPLIER;
		}

		public function getEnergyThreshold():Number
		{
			return ENERGY_THRESHOLD;
		}

		public function getTooltip():Object
		{
			return extensions.skills.createStandardTooltip(ID, _(NAME), _(DESCRIPTION, getDamageMultiplier(), getBonusDamageMultiplier(), getEnergyThreshold()));
		}

		public function refresh(bash:Skill):void
		{

		}

		public function performEffect():void
		{
			var isFull:Boolean = currentCharacter.energy >= getEnergyThreshold();


		}

		public function multiClickCount():int
		{
			var character:Character = CH2.currentCharacter;
			var buffClicks:int = 4 + (2 * character.getTrait("ExtraMulticlicks"));
			if (character.getTrait("Flurry"))
			{
				buffClicks = (buffClicks + 1) * character.hasteRating.numberValue() - 1;
			}
			return buffClicks + 1;
		}

		public function multiClickEffect():void
		{
			var character:Character = CH2.currentCharacter;
			var buff:Buff = new Buff();
			var buffClicks:int = multiClickCount() - 1;
			buff.name = "MultiClick";
			buff.iconId = 202;
			buff.tickRate = 50;
			buff.duration = buff.tickRate * buffClicks;
			buff.tickFunction = function():void
			{
				if (!character.isNextMonsterInRange && !character.getTrait("EtherealMultiClick"))
				{
					buff.timeSinceActivated += 0.2 * buff.timeLeft;
					if (buff.timeLeft < buff.timeSinceLastTick)
					{
						buff.timeSinceLastTick = buff.timeLeft;
					}
				}
				character.clickAttack(false);
			}
			buff.tooltipFunction = function():Object
			{
				return {"header": "MultiClick", "body": "Clicking " + Math.ceil((5 + (2 * CH2.currentCharacter.getTrait("ExtraMulticlicks"))) * (CH2.currentCharacter.getTrait("Flurry") ? CH2.currentCharacter.hasteRating.numberValue() : 1)) + " times, with " + Math.ceil(buff.timeLeft / buff.tickRate) + " remaining. Dashing consumes 20% of remaining clicks."};
			}
			character.buffs.addBuff(buff);
			character.clickAttack(false);
		}
	}
}