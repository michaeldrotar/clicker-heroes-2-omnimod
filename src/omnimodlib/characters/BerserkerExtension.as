package omnimodlib.characters
{
	import com.playsaurus.model.Model;
	import com.playsaurus.numbers.BigNumber;
	import com.playsaurus.random.Random;
	import heroclickerlib.LevelGraph;
	import models.AscensionWorld;
	import models.AscensionWorlds;
	import models.Automator;
	import models.AutomatorQueue;
	import models.AutomatorQueueArrow;
	import models.AutomatorQueueSlot;
	import models.Character;
	import models.Characters;
	import models.EtherealItem;
	import models.EtherealItemStat;
	import models.EtherealItemStatChoice;
	import models.Item;
	import models.ItemStat;
	import models.Items;
	import models.Roller;
	import models.RubyPurchase;
	import models.Skill;
	import models.Theme;
	import models.TrackedStat;
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import omnimodlib.systems.omniAutomator.OmniAutomatorControlNode;
	import omnimodlib.systems.omniAutomator.OmniAutomatorStartNode;
	import ui.CH2UI;

	public class BerserkerExtension extends Extension
	{
		public const CHARACTER_NAME:String = 'Berserker';

		public function onStartup(game:IdleHeroMain):void
		{
			var berserkerTemplate:Character = new Character();
			berserkerTemplate.name = CHARACTER_NAME;
			berserkerTemplate.assetGroupName = 'HelpfulAdventurer';
			berserkerTemplate.flavor = 'The Berserker posseses incredible speed and devastating fire magic.';
			berserkerTemplate.flavorName = 'Cid';
			berserkerTemplate.flavorClass = 'The Berserker';
			berserkerTemplate.defaultSaveName = 'berserker';

			berserkerTemplate.availableForCreation = true;
			berserkerTemplate.visibleOnCharacterSelect = true;
			berserkerTemplate.characterSelectOrder = Util.keys(Characters.startingDefaultInstances).length + 1;

			berserkerTemplate.startingSkills = [];
			berserkerTemplate.upgradeableStats = Character.DEFAULT_UPGRADEABLE_STATS;
			berserkerTemplate.levelGraphNodeTypes = {};
			berserkerTemplate.levelGraph = new LevelGraph();

			//OmniTemplateManager.instance.registerModelClass(Skill, [
			//'cooldown',
			//'staticSkills'
			//], 'id');
			//OmniTemplateManager.instance.registerModel(bash);
			//OmniTemplatemanager.isntance.getModel(Skill, 'bash')

			Characters.startingDefaultInstances['Berserker'] = berserkerTemplate;
			trigger('onCharacterTemplateCreated', berserkerTemplate);
		}

		public function onCharacterCreated():void
		{
			if (currentCharacter.name !== CHARACTER_NAME)
			{
				return;
			}

			currentCharacter.extendedVariables = new BerserkerExtendedVariables();
			currentCharacter.readExtendedVariables();

			var startNode:OmniAutomatorStartNode = currentCharacter.extendedVariables['startNode'];
			if (!startNode.target)
			{
				Util.trace('Creating startNode.target...')
				startNode.target = templateManager.createModel(OmniAutomatorControlNode, 'OMNIMOD_BASH');
				startNode.target.targets['SUCCESS'] = startNode.target;
			}
			Util.trace(startNode.target.targets);

			hookOverride(currentCharacter, 'populateEtherealItemStats', populateEtherealItemStats);
			hookOverride(currentCharacter, 'onAscension', onAscension);
			hookOverride(currentCharacter, 'onWorldStarted', onWorldStarted);

			hookOverride(currentCharacter, 'update', update);

			extensions.omniAutomator.register();
		}

		public function copyNode(controlNode:OmniAutomatorControlNode):OmniAutomatorControlNode
		{
			var copy:OmniAutomatorControlNode = new OmniAutomatorControlNode();
			copy.id = controlNode.id;
			copy.executionFunction = controlNode.executionFunction;
			copy.outputs = controlNode.outputs;
			return copy;
		}

		public function onCharacterStarted():void
		{
			if (currentCharacter.name !== CHARACTER_NAME)
			{
				return;
			}

			extensions.berserk.addBuff();

			currentCharacter.activateSkill(extensions.bash.ID);
			currentCharacter.energy = 0;
		}

		public function onUICreated():void
		{
			if (currentCharacter.name !== CHARACTER_NAME)
			{
				return;
			}

			CH2UI.instance.mainUI.mainPanel.unregisterTab(0); // equipment
			extensions.omniSkillsSubPanel.registerTab({position: 0});

			CH2UI.instance.mainUI.mainPanel.unregisterTab(1); // skill tree
			extensions.omniAutomatorSubPanel.registerTab({position: 1});

			CH2UI.instance.mainUI.mainPanel.unregisterTab(2); // skills
			CH2UI.instance.mainUI.mainPanel.registerTab(2, 'unused', null, 0, Util.falseFn, Util.falseFn, Util.noop);

			CH2UI.instance.mainUI.mainPanel.unregisterTab(3); // automator
			CH2UI.instance.mainUI.mainPanel.registerTab(3, 'unused', null, 0, Util.falseFn, Util.falseFn, Util.noop);

			CH2UI.instance.mainUI.mainPanel.showTabByIndex(0);
		}

		public function populateEtherealItemStats(characterInstance:Character, next:Function):void
		{
			next(characterInstance); // call to preserve the chain, default method does nothing

			var allSlots:Array = [0, 1, 2, 3, 4, 5, 6, 7];
			var etherealItemStats:Object = {};

			var etherealItemStatChoice:EtherealItemStatChoice = new EtherealItemStatChoice();
			etherealItemStatChoice.id = 'OMNIMOD_ETHEREAL';
			etherealItemStatChoice.key = 'OMNIMOD_ETHERAL_KEY';
			etherealItemStatChoice.isSpecial = false;
			etherealItemStatChoice.slots = allSlots;
			etherealItemStatChoice.valueFunction = Character.one();
			etherealItemStatChoice.exchangeRateFunction = Character.one();
			etherealItemStatChoice.weight = 1;
			etherealItemStatChoice.tooltipDescriptionFormat = "+%s foo per foo";
			etherealItemStatChoice.namePrefix = "Foo";
			etherealItemStatChoice.nameSuffix = "Foo";
			etherealItemStatChoice.params = {"sourceId": 'foo', "destinationId": 'foo'};
			etherealItemStats[etherealItemStatChoice.id] = etherealItemStatChoice;

			characterInstance.etherealItemStats = etherealItemStats;
		}

		public function onAscension(next:Function):void
		{
			next();
			extensions.berserk.resetBuff();
		}

		public function onWorldStarted(worldNumber:Number, next:Function):void
		{
			next(worldNumber);
			extensions.berserk.addBuff();
		}

		public function update(dt:int, next:Function):void
		{
			next(dt);
		}
	}
}
import omnimodlib.OmniExtendedVariables;
import omnimodlib.Util;
import omnimodlib.managers.OmniSaveManager;
import omnimodlib.systems.omniAutomator.OmniAutomatorStartNode;

class BerserkerExtendedVariables extends OmniExtendedVariables
{
	public var startNode:OmniAutomatorStartNode = new OmniAutomatorStartNode();

	public function BerserkerExtendedVariables()
	{
		super();
		registerDynamicString('json');
	}

	public function get json():String
	{
		return OmniSaveManager.instance.toJsonString({ startNode: startNode });
	}

	public function set json(value:String):void
	{
		var properties:Object = OmniSaveManager.instance.fromJsonString(value);
		if ('startNode' in properties)
		{
			startNode = properties['startNode']
		}
	}
}