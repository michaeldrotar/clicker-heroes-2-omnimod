package omnimodlib
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import lib.managers.TextManager;
	import models.Character;

	public class OmniStringUtil
	{
		public static const NUMBER_THRESHOLD:Number = 1000;
		public static const COLOR_REPLACEMENT_PATTERN:RegExp = /(?<!\|)\|c([0-9a-f]{6}|:\w+?:)(.+?)\|(?!\|)r/g;
		public static const VALUE_REPLACEMENT_PATTERN:RegExp = /(?<!{)\{(.+?)\}(?!})/g;
		public static const WILDCARD_PATTERN:RegExp = /([{}\|])\1/g;

		private static var _instance:OmniStringUtil;

		private var _formatters:Object;

		public function OmniStringUtil()
		{
			_formatters = {};
			_formatters.color = applyColorByName;
			_formatters.distance = formatDistance;
			_formatters.duration = formatDuration;
			_formatters.label = label;
			_formatters.number = formatNumber;
			_formatters.invert = invertNumber;
			_formatters.percent = formatPercent;
		}

		public static function get instance():OmniStringUtil
		{
			_instance ||= new OmniStringUtil();
			return _instance;
		}

		public function applyColor(value:*, colorString:String):String
		{
			return TextManager.textToColor(formatNumber(value), colorString);
		}

		public function applyColorByName(value:*, name:String):String
		{
			return applyColor(value, OmniColorPalette.instance.getColorString(name));
		}

		public function formatDistance(value:*):String
		{
			var distance:Number = (new BigNumber(value)).numberValue() / Character.ONE_METER_Y_DISTANCE;
			var text:String = OmniLocalization.instance.localize({one: '{ count } meter', other: '{ count } meters'}, {count: distance});
			return applyColorByName(text, 'value');
		}

		public function formatDuration(value:*):String
		{
			var text:String;
			var seconds:Number = (new BigNumber(value)).numberValue() / 1000;
			if (seconds <= 120)
			{
				text = OmniLocalization.instance.localize({one: '{ count } second', other: '{ count } seconds'}, {count: seconds});
			}
			else
			{
				var minutes:Number = seconds / 60;
				text = OmniLocalization.instance.localize({one: '{ count } minute', other: '{ count } minutes'}, {count: minutes});
			}
			return applyColorByName(text, 'value');
		}

		public function formatNumber(value:*, precision:uint = 2):String
		{
			if (value is Number || value is BigNumber)
			{
				var bigNumber:BigNumber = roundNumber(value, precision);

				// formattedNumber displays weird with smaller numbers in engineering notation
				// so only use it for bigger numbers
				if (bigNumber.gteN(NUMBER_THRESHOLD))
				{
					// If called too early, it crashes.. will have to deal without it or move calls to a later point in time
					if (CH2.game && CH2.user)
					{
						return CH2.game.formattedNumber(bigNumber);
					}
				}

				return Number(bigNumber.numberValue().toFixed(precision)).toString();
			}

			return value.toString();
		}

		public function formatPercent(value:*):String
		{
			if (value is Number)
			{
				value *= 100;
			}
			else if (value is BigNumber)
			{
				value = (value as BigNumber).multiplyN(100);
			}

			return formatNumber(value) + '%';
		}

		public function formatString(text:String, ... args):String
		{
			if (args.length === 0)
			{
				// No args given, no substitutions to make
				return text;
			}

			var output:String = text;
			output = output.replace(VALUE_REPLACEMENT_PATTERN, _generateValueReplacementFn(args[0], args));
			output = output.replace(COLOR_REPLACEMENT_PATTERN, _generateColorReplacementFn());

			// Collapse wildcards
			output = output.replace(WILDCARD_PATTERN, '$1');

			return output;
		}

		public function invertNumber(value:*):*
		{
			if (value is BigNumber)
			{
				return (value as BigNumber).negate();
			}
			return -Number(value);
		}

		public function label(value:*, label:String):String
		{
			var text:String = OmniLocalization.instance.localize('labels.' + label, {count: value});
			if (OmniColorPalette.instance.hasColor(label))
			{
				text = applyColorByName(text, label);
			}
			else
			{
				text = applyColorByName(text, 'value');
			}
			return text;
		}

		public function roundNumber(value:*, precision:uint = 2):BigNumber
		{
			var bigNumber:BigNumber;
			if (value is Number || value is BigNumber)
			{
				bigNumber = new BigNumber(value);
				if (bigNumber.gteN(NUMBER_THRESHOLD))
				{
					return bigNumber;
				}
			}

			if (value is BigNumber) // and less than threshold, needs to round off
			{
				value = (value as BigNumber).numberValue();
			}

			if (value is Number)
			{
				// Casting toFixed to Number handles chopping off unnecessary 0 decimals
				value = Number((value as Number).toFixed(precision));
			}

			return new BigNumber(value);
		}

		public function trim(text:String):String
		{
			return text.replace(/^\s+|\s+$/g, '');
		}

		private function _generateColorReplacementFn():Function
		{
			return function(... args):String
			{
				var match:String = args[0];
				var color:String = args[1];
				var text:String = args[2];

				if (color.charAt(0) === ':')
				{
					return applyColorByName(text, color.substring(1, color.length - 1));
				}
				return applyColor(text, '#' + color);
			}
		}

		private function _generateValueReplacementFn(owner:*, substitutions:Array):Function
		{
			return function(... args):String
			{
				var match:String = args[0];
				var parts:Array = args[1].split('|');

				var first:String = trim(parts[0]);
				var index:Number = Number(first); // may be NaN

				var formatters:Array = parts.slice(1);
				var formatter:String;
				var formatterParts:Array;
				var formatterName:String;
				var formatterArguments:Array;

				var value:*;

				if (isNaN(index))
				{
					if (first in owner)
					{
						value = owner[first];
					}
				}
				else
				{
					value = substitutions[index];
				}

				if (value === undefined)
				{
					// Couldn't find a value, don't manipulate the string
					return match;
				}

				// Reuse index
				for (index = 0; index < formatters.length; index++)
				{
					formatter = trim(formatters[index]);
					formatterParts = formatter.split(':');

					formatterName = formatterParts[0];
					formatterArguments = formatterParts.slice(1);

					if (!(formatterName in _formatters))
					{
						// Invalid formatter, don't manipulate the string
						return match;
					}

					value = _formatters[formatterName].apply(this, [].concat(value, formatterArguments));
				}

				// Always format numbers, leaves strings in-tact
				return formatNumber(value);
			}
		}
	}
}