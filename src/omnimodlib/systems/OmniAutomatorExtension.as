package omnimodlib.systems
{
	import models.Skill;
	import omnimodlib.Extension;
	import omnimodlib.systems.omniAutomator.OmniAutomatorControlNode;
	import omnimodlib.systems.omniAutomator.OmniAutomatorStartNode;

	public class OmniAutomatorExtension extends Extension
	{
		public var registeredCharacters:Array = [];

		public var startNode:OmniAutomatorStartNode;

		public function OmniAutomatorExtension()
		{
			super();
		}

		public function onStartup(game:IdleHeroMain):void
		{
			saveManager.registerClass(OmniAutomatorStartNode, ['destination', 'target']);

			templateManager.registerClass(OmniAutomatorControlNode, ['outputs', 'executionFunction'])
			saveManager.registerClass(OmniAutomatorControlNode, ['id', 'targets']);
		}

		public function register():void
		{
			registeredCharacters.push(currentCharacter);

			startNode = currentCharacter.extendedVariables['startNode'];

			hookOverride(currentCharacter, 'update', update);
		}

		public function registerControlNode(controlNode:OmniAutomatorControlNode):void
		{
			templateManager.registerModel(controlNode);
		}

		public function update(dt:int, next:Function):void
		{
			next(dt);

			startNode.update(dt);
		}

		public function createControlNodeForSkill(currentSkill:Skill):OmniAutomatorControlNode
		{
			var controlNode:OmniAutomatorControlNode = new OmniAutomatorControlNode();
			controlNode.id = currentSkill.uid;
			controlNode.outputs = ['SUCCESS', 'FAILURE'];

			controlNode.executionFunction = function():String
			{
				var currentSkill:Skill = extensions.skills.getCurrentSkill(currentSkill.uid);
				if (currentSkill && currentCharacter.canUseSkill(currentSkill))
				{
					currentSkill.useSkill();
					return 'SUCCESS';
				}
				else
				{
					return 'FAILURE';
				}
			};

			return controlNode;
		}
	}
}