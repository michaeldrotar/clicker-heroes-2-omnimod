package omnimodlib.systems.omniAutomator
{
	import omnimodlib.Util;

	public class OmniAutomatorStartNode
	{
		private static const MSEC_BETWEEN_NODES:uint = 2000;

		public static const STATE_WAITING:uint = 0;
		public static const STATE_APPROACHING_NODE:uint = 1;
		public static const STATE_EXECUTING_NODE:uint = 2;

		public var state:uint = STATE_WAITING;
		public var target:OmniAutomatorControlNode = null;
		public var destination:OmniAutomatorControlNode = null;
		public var executionTime:uint = 0;

		public var nodes:Array;

		public function OmniAutomatorStartNode()
		{
			super();
		}

		public function update(dt:int):void
		{
			if (!destination)
			{
				reset();
				return;
			}
			if (state === STATE_WAITING)
			{
				executionTime = 0;
				state = STATE_APPROACHING_NODE;
			}
			else
			{
				executionTime += dt;
			}
			if (state === STATE_APPROACHING_NODE)
			{
				if (executionTime >= MSEC_BETWEEN_NODES)
				{
					executionTime -= MSEC_BETWEEN_NODES;
					state = STATE_EXECUTING_NODE;
				}
			}
			if (state === STATE_EXECUTING_NODE)
			{
				var result:String = destination.executionFunction();
				if (result)
				{
					executionTime = 0;
					destination = destination.targets[result];
					if (destination)
					{
						state = STATE_APPROACHING_NODE;
					}
					else
					{
						state = STATE_WAITING;
					}
				}
			}
		}

		public function reset():void
		{
			state = STATE_WAITING;
			executionTime = 0;
			destination = target;
		}
	}
}

import com.playsaurus.model.Model;
import heroclickerlib.CH2;
import models.AutomatorQueue;
import models.AutomatorQueueSlot;

class AutomatorQueueArrowExample extends Model
{

	private static const START_OF_QUEUE:Number = -1;

	private static const MSEC_BETWEEN_QUEUE_SLOTS_UPWARD:Number = 150;

	private static const MSEC_BETWEEN_QUEUE_SLOTS_DOWNWARD:Number = 6000;

	public static const NO_EXECUTION_ON_FRAME:int = -1;

	public static const NO_SELECTED_STONE_PENDING_ACTIVATION:int = -1;

	public static const STATE_WAITING:int = 0;

	public static const STATE_APPROACHING_STONE:int = 1;

	public static const STATE_RETURNING_HOME:int = 2;

	public var state:int = 0;

	public var currentQueueProgress:Number = -1;

	public var indexExecuted:int = -1;

	public var indexActivated:Boolean = false;

	public var selectedStoneIndexToExecute:int = -1;

	private var executionCheckDelay:int = 0;

	public function AutomatorQueueArrowExample()
	{
		super();
	}

	public function get numActiveQueueSlots():int
	{
		return this.automatorQueue.queue.length;
	}

	public function get currentQueueSlotIndex():int
	{
		return Math.floor(this.currentQueueProgress);
	}

	public function get automatorQueue():AutomatorQueue
	{
		return CH2.currentCharacter.automator.currentQueue;
	}

	public function get msecBetweenQueueSlots():Number
	{
		return MSEC_BETWEEN_QUEUE_SLOTS_DOWNWARD / CH2.currentCharacter.automatorSpeed.numberValue() / CH2.currentCharacter.hasteRating.numberValue();
	}

	public function update(dtMsec:int):void
	{
		var amountPassedActivatedStone:Number = NaN;
		this.indexActivated = false;
		this.indexExecuted = NO_EXECUTION_ON_FRAME;
		if (CH2.currentCharacter.isPaused || this.numActiveQueueSlots == 0)
		{
			return;
		}
		var previousQueueSlotIndex:int = this.currentQueueSlotIndex;
		if (this.state == STATE_WAITING)
		{
			if (this.executionCheckDelay > 0)
			{
				this.executionCheckDelay = this.executionCheckDelay - dtMsec;
			}
			else
			{
				this.selectedStoneIndexToExecute = this.getExecutableStoneIndex();
				if (this.selectedStoneIndexToExecute != -1)
				{
					this.state = STATE_APPROACHING_STONE;
					this.executionCheckDelay = 0;
				}
				else
				{
					this.executionCheckDelay = 100;
				}
			}
		}
		else if (this.state == STATE_RETURNING_HOME)
		{
			this.currentQueueProgress = START_OF_QUEUE;
			if (this.currentQueueProgress <= START_OF_QUEUE)
			{
				this.selectedStoneIndexToExecute = this.getExecutableStoneIndex();
				if (this.selectedStoneIndexToExecute == -1)
				{
					this.state = STATE_WAITING;
				}
				else
				{
					this.state = STATE_APPROACHING_STONE;
					this.currentQueueProgress = (START_OF_QUEUE - this.currentQueueProgress) * MSEC_BETWEEN_QUEUE_SLOTS_UPWARD / this.msecBetweenQueueSlots;
					this.currentQueueProgress = Math.min(this.currentQueueProgress, START_OF_QUEUE);
				}
			}
		}
		else if (this.state == STATE_APPROACHING_STONE)
		{
			this.currentQueueProgress = this.currentQueueProgress + dtMsec * (this.selectedStoneIndexToExecute + 1) / this.msecBetweenQueueSlots;
			if (this.currentQueueSlotIndex >= this.selectedStoneIndexToExecute)
			{
				this.indexActivated = this.automatorQueue.queue[this.selectedStoneIndexToExecute].execute();
				this.indexExecuted = this.selectedStoneIndexToExecute;
				this.state = STATE_RETURNING_HOME;
				amountPassedActivatedStone = this.currentQueueProgress - this.selectedStoneIndexToExecute;
				this.currentQueueProgress = this.selectedStoneIndexToExecute - amountPassedActivatedStone * this.msecBetweenQueueSlots / MSEC_BETWEEN_QUEUE_SLOTS_UPWARD;
				if (this.currentQueueProgress <= START_OF_QUEUE)
				{
					this.currentQueueProgress = (START_OF_QUEUE - this.currentQueueProgress) * MSEC_BETWEEN_QUEUE_SLOTS_UPWARD / this.msecBetweenQueueSlots;
					this.currentQueueProgress = Math.min(this.currentQueueProgress, START_OF_QUEUE);
				}
			}
		}
	}

	private function getExecutableStoneIndex():int
	{
		for (var i:int = 0; i < this.automatorQueue.queue.length; i++)
		{
			if (this.automatorQueue.queue[i] && (this.automatorQueue.queue[i] as AutomatorQueueSlot).isExecutable())
			{
				return i;
			}
		}
		return -1;
	}

	public function decimalPercentToDestination():Number
	{
		if (this.state == STATE_APPROACHING_STONE)
		{
			return (this.currentQueueProgress + 0.5) / (0.5 + this.selectedStoneIndexToExecute);
		}
		if (this.state == STATE_RETURNING_HOME)
		{
			return 1 - (this.currentQueueProgress + 0.5) / (this.numActiveQueueSlots + 0.5);
		}
		return 1;
	}

	public function reset():void
	{
		this.state = STATE_WAITING;
		this.currentQueueProgress = START_OF_QUEUE;
		this.indexExecuted = NO_EXECUTION_ON_FRAME;
		this.indexActivated = false;
		this.selectedStoneIndexToExecute = NO_SELECTED_STONE_PENDING_ACTIVATION;
	}
}