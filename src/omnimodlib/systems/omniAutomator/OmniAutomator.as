package omnimodlib.systems.omniAutomator
{
	import com.playsaurus.model.Model;

	public class OmniAutomator extends Model
	{
		public var startNodes:Array = [];
		public var controlNodes:Array = [];

		public function OmniAutomator()
		{
			super();

			[
				{
					id: 'OMNIMOD_START_NODE',
					target: 2,
					x: 0,
					y: 0,
					approaching: 3,
					progress: 160
				},
				{
					id: 'OMNIMOD_START_NODE',
					target: undefined,
					at: 2,
					elapsed: 16
				},
				{
					id: 'OMNIMOD_BASH',
					x: 0,
					y: 100,
					outputs: {
						'SUCCESS': 3
					}
				},
				{
					id: 'OMNIMOD_SHORT_WAIT',
					x: 0,
					y: 200,
					inputs: {
						'amount': 1
					},
					outputs: {}
				}
			]
		}
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------

import com.playsaurus.model.Model;
import com.playsaurus.utils.TimeFormatter;
import heroclickerlib.CH2;

class Automator extends Model
{

	public static const SINGLE_FRAME_MSEC:int = 16;

	public static var allStones:Object = {};

	public static var allGems:Object = {};

	public var stoneInventory:Array;

	public var gemInventory:Array;

	public var queueSets:Array;

	public var maxQueueSets:Number = 5;

	public var maxQueueSize:Number = 3;

	public var currentQueueIndex:int = 0;

	public var currentQueue:AutomatorQueue;

	public var enabled:Boolean = true;

	public var loaded:Boolean = false;

	public var numSetsUnlocked:int = 1;

	public var totalStonesUnlocked:Number = 0;

	public function Automator()
	{
		this.stoneInventory = new Array();
		this.gemInventory = new Array();
		this.queueSets = new Array();
		super();
		this.registerDynamicObject("gemInventory");
		this.registerDynamicObject("stoneInventory");
		this.registerDynamicCollection("queueSets", AutomatorQueue);
		this.registerDynamicNumber("currentQueueIndex");
		this.registerDynamicBoolean("enabled");
		this.registerDynamicNumber("numSetsUnlocked");
		this.registerDynamicNumber("totalStonesUnlocked");
	}

	public function isQueueLocked(index:int):Boolean
	{
		return index >= this.numSetsUnlocked;
	}

	public function addQueueSet():void
	{
		this.numSetsUnlocked++;
	}

	public function addQueueSlot():void
	{
		this.maxQueueSize++;
	}

	public function setCurrentQueue(queueSetIndex:int):void
	{
		if (queueSetIndex > this.numSetsUnlocked)
		{
			queueSetIndex = 1;
		}
		else if (queueSetIndex < 1)
		{
			queueSetIndex = this.numSetsUnlocked;
		}
		CH2.currentCharacter.inputLogger.recordInput(GameActions.AUTOMATOR_SELECT_QUEUE, queueSetIndex);
		if (this.queueSets[queueSetIndex] == null)
		{
			this.queueSets[queueSetIndex] = new AutomatorQueue();
		}
		this.currentQueueIndex = queueSetIndex;
		this.currentQueue = this.queueSets[queueSetIndex];
		if (this.currentQueue.name == "")
		{
			this.currentQueue.name = "Set " + queueSetIndex;
		}
	}

	public function removeStoneFromSave(id:String, replacement:String = null):int
	{
		var index:int = 0;
		var queue:AutomatorQueue = null;
		var j:int = 0;
		var indiciesToRemove:Array = [];
		for (var i:int = 0; i < this.stoneInventory.length; i++)
		{
			if (this.stoneInventory[i] == id)
			{
				if (replacement)
				{
					this.stoneInventory[i] = replacement;
				}
				else
				{
					indiciesToRemove.push(i);
				}
			}
		}
		var removalCount:int = indiciesToRemove.length;
		for each (index in indiciesToRemove)
		{
			for each (queue in this.queueSets)
			{
				for (j = 0; j < queue.queue.length; j++)
				{
					if (queue.queue[j])
					{
						if (queue.queue[j].stoneIndex == index)
						{
							queue.queue[j].removeStoneInSlot();
						}
						else if (queue.queue[j].stoneIndex > index)
						{
							queue.queue[j].stoneIndex--;
						}
					}
				}
			}
			this.stoneInventory.removeAt(index);
		}
		return removalCount;
	}

	public function removeGemFromSave(id:String, replacement:String = null):int
	{
		var index:int = 0;
		var queue:AutomatorQueue = null;
		var j:int = 0;
		var indiciesToRemove:Array = [];
		for (var i:int = 0; i < this.gemInventory.length; i++)
		{
			if (this.gemInventory[i] == id)
			{
				if (replacement)
				{
					this.gemInventory[i] = replacement;
				}
				else
				{
					indiciesToRemove.push(i);
				}
			}
		}
		var removalCount:int = indiciesToRemove.length;
		for each (index in indiciesToRemove)
		{
			for each (queue in this.queueSets)
			{
				for (j = 0; j < queue.queue.length; j++)
				{
					if (queue.queue[j])
					{
						if (queue.queue[j].gemIndex == index)
						{
							queue.queue[j].removeGemInSlot();
						}
						else if (queue.queue[j].gemIndex > index)
						{
							queue.queue[j].gemIndex--;
						}
					}
				}
			}
			this.gemInventory.removeAt(index);
		}
		return removalCount;
	}

	public function setupInventories():void
	{
		var gemId:String = null;
		var stoneId:String = null;
		for each (gemId in this.gemInventory)
		{
			if (!allGems[gemId])
			{
				this.gemInventory.removeAt(this.gemInventory.indexOf(gemId));
			}
		}
		for each (stoneId in this.stoneInventory)
		{
			if (!allStones[stoneId])
			{
				this.stoneInventory.removeAt(this.stoneInventory.indexOf(stoneId));
			}
		}
	}

	public function setSelectedGemInQueue(queueIndex:int, gemIndex:int):void
	{
		var valueRepresentingGemAndQueueIndex:int = (queueIndex + 1) * 200 + gemIndex;
		CH2.currentCharacter.inputLogger.recordInput(GameActions.AUTOMATOR_SET_GEM_INDEX_FOR_QUEUE_INDEX, valueRepresentingGemAndQueueIndex);
		this.currentQueue.setGemInQueue(queueIndex, gemIndex);
	}

	public function setSelectedStoneInQueue(queueIndex:int, stoneIndex:int):void
	{
		var valueRepresentingStoneAndQueueIndex:int = (queueIndex + 1) * 200 + stoneIndex;
		CH2.currentCharacter.inputLogger.recordInput(GameActions.AUTOMATOR_SET_GEM_INDEX_FOR_QUEUE_INDEX, valueRepresentingStoneAndQueueIndex);
		this.currentQueue.setStoneInQueue(queueIndex, stoneIndex);
	}

	public function executeAutomatorQueue(dtMsec:Number):void
	{
		var queue:AutomatorQueue = null;
		if (!this.loaded)
		{
			for each (queue in this.queueSets)
			{
				queue.loadSavedQueue();
			}
			this.setCurrentQueue(this.currentQueueIndex);
			this.loaded = true;
		}
		if (this.enabled)
		{
			this.currentQueue.update(dtMsec);
		}
	}

	public function getAvailableStone(index:int):AutomatorStone
	{
		return allStones[this.stoneInventory[index]];
	}

	public function getStaticStone(id:String):AutomatorStone
	{
		return allStones[id];
	}

	public function getStoneId(index:int):String
	{
		return this.stoneInventory[index];
	}

	public function addStone(id:String, name:String, label:String, description:String, onActivate:Function, cooldownMs:Number = 16):void
	{
		var stone:AutomatorStone = new AutomatorStone();
		stone.id = id;
		stone.name = name;
		stone.label = label;
		stone.description = description;
		stone.onActivate = onActivate;
		stone.cooldownMs = cooldownMs;
		allStones[id] = stone;
	}

	public function addCooldownStone(id:String, cooldownMs:Number = 0):void
	{
		var longTime:String = TimeFormatter.formatTimeSimple(cooldownMs / 1000, 120, 60, 24, false);
		var shortTime:String = TimeFormatter.formatTimeSimple(cooldownMs / 1000, 120, 60, 24, true);
		var name:String = shortTime + " Cooldown";
		var label:String = shortTime + " CD";
		var description:String = "A stone that activates every " + longTime + ".";
		this.addStone(id, name, label, description, function():Boolean
		{
			return true;
		}, cooldownMs);
	}

	public function unlockStone(stoneId:String):void
	{
		this.stoneInventory.push(stoneId);
		this.totalStonesUnlocked++;
	}

	public function getAvailableGem(index:int):AutomatorGem
	{
		return this.getStaticGem(this.gemInventory[index]);
	}

	public function getStaticGem(id:String):AutomatorGem
	{
		return allGems[id];
	}

	public function getGemId(index:int):String
	{
		return this.gemInventory[index];
	}

	public function addGem(id:String, name:String, iconId:Number, description:String, onActivate:Function, canActivate:Function, cooldownMs:Number = 0):void
	{
		var gem:AutomatorGem = new AutomatorGem();
		gem.id = id;
		gem.assetName = "BitmapHUD_gem" + id.split("_")[1];
		gem.name = name;
		gem.iconId = iconId;
		gem.description = description;
		gem.onActivate = onActivate;
		gem.canActivate = canActivate;
		gem.cooldownMs = cooldownMs;
		allGems[id] = gem;
	}

	public function addSkillGem(id:String, skill:Skill):void
	{
		var tooltip:Object = skill.tooltip;
		this.addGem(id, skill.name, skill.iconId, "Activates \"" + skill.name + "\" on cooldown.", function():Boolean
		{
			var character:Character = CH2.currentCharacter;
			var activeSkill:Skill = character.getActiveSkill(skill.uid);
			if (activeSkill && character.canUseSkill(activeSkill))
			{
				activeSkill.useSkill();
				return true;
			}
			return false;
		}, function():Boolean
		{
			var character:Character = CH2.currentCharacter;
			var activeSkill:Skill = character.getActiveSkill(skill.uid);
			return activeSkill && character.canUseSkill(activeSkill);
		});
	}

	public function unlockGem(gemId:String):void
	{
		this.gemInventory.push(gemId);
	}
}

// --------------------------------------------------------------------------------------------------------------------------------------

class AutomatorQueue extends Model
{

	public var name:String = "";

	public var queue:Array;

	public var stoneQueue:Array;

	public var automatorQueueArrow:AutomatorQueueArrow;

	public function AutomatorQueue()
	{
		this.queue = new Array();
		this.stoneQueue = new Array();
		this.automatorQueueArrow = new AutomatorQueueArrow();
		super();
		this.registerDynamicCollection("queue", AutomatorQueueSlot);
		this.registerDynamicString("name");
		this.registerDynamicChild("automatorQueueArrow", AutomatorQueueArrow);
	}

	public function getLength():int
	{
		return this.queue.length;
	}

	public function removeQueueSlot(queueIndex:int):void
	{
		CH2.currentCharacter.inputLogger.recordInput(GameActions.AUTOMATOR_REMOVE_SLOT_FROM_QUEUE, queueIndex);
		this.queue.splice(queueIndex, 1);
	}

	public function setGemInQueue(queueIndex:int, gemIndex:int):void
	{
		if (queueIndex < 0 || gemIndex < 0)
		{
			return;
		}
		if (!this.isGemInUse(gemIndex))
		{
			if (this.queue[queueIndex] == null)
			{
				this.queue[queueIndex] = new AutomatorQueueSlot();
			}
			this.queue[queueIndex].setGemInSlot(gemIndex);
		}
	}

	public function setStoneInQueue(queueIndex:int, stoneIndex:int):void
	{
		if (queueIndex < 0 || stoneIndex < 0)
		{
			return;
		}
		if (!this.isStoneInUse(stoneIndex))
		{
			if (this.queue[queueIndex] == null)
			{
				this.queue[queueIndex] = new AutomatorQueueSlot();
			}
			this.queue[queueIndex].setStoneInSlot(stoneIndex);
		}
	}

	public function getGem(queueIndex:int):int
	{
		if (this.queue[queueIndex] != null)
		{
			return this.queue[queueIndex].gemIndex;
		}
		return -1;
	}

	public function getStone(queueIndex:int):int
	{
		if (this.queue[queueIndex] != null)
		{
			return this.queue[queueIndex].stoneIndex;
		}
		return -1;
	}

	public function canActivateSlot(queueIndex:int):Boolean
	{
		if (this.queue[queueIndex] != null)
		{
			return this.queue[queueIndex].isExecutable();
		}
		return false;
	}

	public function getPercentageRemainingOnHighestCooldown(queueIndex:int):Number
	{
		var gemCooldownPercentage:Number = NaN;
		var stoneCooldownPercentage:Number = NaN;
		if (this.queue[queueIndex] != null)
		{
			gemCooldownPercentage = 0;
			if (this.isGemOnCooldown(queueIndex))
			{
				gemCooldownPercentage = this.queue[queueIndex].gemCooldownProgressMs / this.queue[queueIndex].gemCooldownMs;
			}
			stoneCooldownPercentage = 0;
			if (this.isStoneOnCooldown(queueIndex))
			{
				stoneCooldownPercentage = this.queue[queueIndex].stoneCooldownProgressMs / this.queue[queueIndex].stoneCooldownMs;
			}
			return Math.max(gemCooldownPercentage, stoneCooldownPercentage);
		}
		return 0;
	}

	public function isGemOnCooldown(queueIndex:int):Boolean
	{
		if (this.queue[queueIndex] != null && this.queue[queueIndex].gem)
		{
			return this.queue[queueIndex].gemOnCooldown();
		}
		return false;
	}

	public function isStoneOnCooldown(queueIndex:int):Boolean
	{
		if (this.queue[queueIndex] != null && this.queue[queueIndex].stone)
		{
			return this.queue[queueIndex].stoneOnCooldown();
		}
		return false;
	}

	public function update(dtMsec:int):void
	{
		var length:int = this.getLength();
		for (var i:int = 0; i < length; i++)
		{
			if (this.queue[i] != null)
			{
				this.queue[i].progressCooldowns(dtMsec);
			}
		}
		this.automatorQueueArrow.update(dtMsec);
	}

	public function isGemInUse(gemIndex:int):Boolean
	{
		var automatorQueueSlot:AutomatorQueueSlot = null;
		for (var i:int = 0; i < this.queue.length; i++)
		{
			if (this.queue[i] != null)
			{
				automatorQueueSlot = this.queue[i] as AutomatorQueueSlot;
				if (automatorQueueSlot && automatorQueueSlot.gemIndex == gemIndex)
				{
					return true;
				}
			}
		}
		return false;
	}

	public function isStoneInUse(stoneIndex:int):Boolean
	{
		var automatorQueueSlot:AutomatorQueueSlot = null;
		for (var i:int = 0; i < this.queue.length; i++)
		{
			if (this.queue[i] != null)
			{
				automatorQueueSlot = this.queue[i] as AutomatorQueueSlot;
				if (automatorQueueSlot && automatorQueueSlot.stoneIndex == stoneIndex)
				{
					return true;
				}
			}
		}
		return false;
	}

	public function loadSavedQueue():void
	{
		var queueSlot:AutomatorQueueSlot = null;
		for each (queueSlot in this.queue)
		{
			queueSlot.loadSavedSlot();
		}
	}
}
