package omnimodlib.systems.omniAutomator
{
	import com.playsaurus.model.Model;

	public class OmniAutomatorControlNode
	{
		public var id:String;
		public var outputs:Array;
		public var executionFunction:Function;
		public var targets:Object = {};

		public function OmniAutomatorControlNode()
		{
			super();
		}
	}
}

import com.doogog.bitmap.BitmapAnimation;
import com.playsaurus.model.Model;
import com.playsaurus.utils.StringFormatter;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.utils.Timer;
import heroclickerlib.CH2;
import heroclickerlib.managers.Trace;
import models.Buff;
import models.Monster;
import models.AttackData;
import models.Character;

class AutomatorStone extends Model
{

	public var id:String;

	public var name:String;

	public var label:String;

	public var description:String;

	public var cooldownMs:Number;

	public var onActivate:Function;

	public var enabled:Boolean = true;

	public function AutomatorStone()
	{
		super();
		this.registerDynamicString(this.id);
		this.cooldownMs = 0;
	}

	public function getTooltip():Object
	{
		var header:String = this.name;
		var body:String = this.description;
		return {"header": header, "body": body};
	}

	public function activate():Boolean
	{
		return this.onActivate && this.onActivate();
	}
}

class AutomatorGem extends Model
{

	public var id:String;

	public var assetName:String;

	public var name:String;

	public var description:String;

	public var iconId:Number;

	public var onActivate:Function;

	public var canActivate:Function;

	public var cooldownMs:Number;

	public var enabled:Boolean = true;

	public function AutomatorGem()
	{
		super();
	}

	public function getTooltip():Object
	{
		var header:String = this.name;
		var body:String = this.description;
		if (this.cooldownMs != 0)
		{
			body = body + ("\n\nCooldown: " + this.cooldownMs / 1000 + " seconds");
		}
		return {"header": header, "body": body};
	}

	public function activate():Boolean
	{
		return this.onActivate && this.onActivate();
	}
}

class Skill extends Model
{
	public static var skillDurationMultiplier:Number = 1;

	public static var staticFields:Array = ["description", "iconId", "consumableOnly", "minimumAscensions", "effectFunction", "tooltipFunction", "ignoresGCD", "maximumRange", "minimumRange", "usesMaxEnergy", "usesMaxMana"];

	public var level:Number = 1;

	public var computedDurationWhenUsed:Number = 0;

	public var isConsumable:Boolean = false;

	public var isActive:Boolean = false;

	public var cooldownRemaining:Number = 0;

	public var slot:Number = -1;

	public var timeOfLastUse:Number = -1;

	public var timeOfUnlock:Number = -1;

	public var cooldown:int;

	public var manaCost:int;

	public var energyCost:int;

	public var castTime:Number;

	public var timesCast:Number = 0;

	public var name:String;

	public var modName:String;

	public var description:String;

	public var iconId:Number;

	public var consumableOnly:Boolean;

	public var minimumAscensions:Number;

	public var effectFunction:Function;

	public var tooltipFunction:Function;

	public var ignoresGCD:Boolean;

	public var maximumRange:Number;

	public var minimumRange:Number;

	public var usesMaxEnergy:Boolean = false;

	public var usesMaxMana:Boolean = false;

	public function Skill()
	{
		super();
		registerDynamicString("name");
		registerDynamicString("modName");
		registerDynamicNumber("cooldown");
		registerDynamicNumber("cooldownRemaining");
		registerDynamicNumber("manaCost");
		registerDynamicNumber("energyCost");
		registerDynamicNumber("castTime");
		registerDynamicNumber("timesCast");
		registerDynamicNumber("timeOfLastUse");
		registerDynamicNumber("level");
		registerDynamicBoolean("isConsumable");
		registerDynamicBoolean("isActive");
		registerDynamicNumber("slot");
	}

	public function get uid():String
	{
		return this.name;
	}

	public function get isOnCooldown():Boolean
	{
		return this.cooldownRemaining > 0;
	}

	public function get canUseSkill():Boolean
	{
		if (CH2.world.getNextMonster() == null)
		{
			return false;
		}
		var nearestMonsterDistance:Number = Math.abs(CH2.world.getNextMonster().y - CH2.currentCharacter.y);
		return !this.isOnCooldown && CH2.currentCharacter.state != Character.STATE_CASTING && CH2.currentCharacter.energy >= this.getCalculatedEnergyCost() && CH2.currentCharacter.mana >= this.getManaCost() && (CH2.currentCharacter.gcdRemaining <= 0 || this.ignoresGCD) && (this.maximumRange == 0 || nearestMonsterDistance <= this.maximumRange) && nearestMonsterDistance >= this.minimumRange;
	}

	public function hasEnoughResources():Boolean
	{
		return CH2.currentCharacter.energy >= this.getCalculatedEnergyCost() && CH2.currentCharacter.mana >= this.getManaCost();
	}

	public function get tooltip():Object
	{
		if (this.tooltipFunction)
		{
			return this.tooltipFunction();
		}
		return {"header": this.name, "body": this.skillCosts()};
	}

	public function initialize():void
	{
		var field:* = null;
		for (field in this.getFields())
		{
			this[field] = CH2.currentCharacter.getStaticSkill(this.uid)[field];
		}
	}

	public function prettyDisplay(num:Number):String
	{
		return Number(num.toFixed(2)).toString();
	}

	public function skillCosts():String
	{
		var effectiveCooldown:* = undefined;
		var skillCosts:String = "";
		if (this.getManaCost() > 0)
		{
			skillCosts = skillCosts + ("Mana: " + this.getManaCost() + "\n");
			skillCosts = StringFormatter.colorize(skillCosts, "#00A6CE");
		}
		if (this.getCalculatedEnergyCost() > 0)
		{
			skillCosts = skillCosts + ("Energy: " + this.getCalculatedEnergyCost() + "\n");
			skillCosts = StringFormatter.colorize(skillCosts, "#ffff00");
		}
		if (this.cooldown > 0)
		{
			effectiveCooldown = this.cooldown / CH2.currentCharacter.hasteRating.numberValue();
			skillCosts = skillCosts + ("Cooldown: " + this.prettyDisplay(Math.ceil(effectiveCooldown / 1000)) + " sec");
		}
		return skillCosts;
	}

	private function consumableEffectivenessMultiplier():Number
	{
		return 1;
	}

	public function getManaCost():Number
	{
		if (!this.usesMaxMana)
		{
			return this.manaCost;
		}
		return CH2.currentCharacter.maxMana.numberValue();
	}

	public function getCalculatedEnergyCost():Number
	{
    return 0;
		/* return CH2.currentCharacter.getCalculatedEnergyCost(this); */
	}

	public function useSkill():void
	{
		var buffedCastTime:Number = NaN;
		this.timeOfLastUse = CH2.user.totalMsecsPlayed;
		if (!this.isConsumable)
		{
			CH2.currentCharacter.addMana(this.getManaCost() * -1);
			CH2.currentCharacter.addEnergy(this.getCalculatedEnergyCost() * -1);
		}
		this.cooldownRemaining = this.cooldown;
		CH2.currentCharacter.triggerGlobalCooldown();
		if (this.castTime)
		{
			buffedCastTime = this.castTime * (1 / CH2.currentCharacter.hasteRating.numberValue());
			if (CH2.currentCharacter.hasteRating.numberValue() < 1)
			{
				Trace("hasteRating < 1");
			}
			Trace("Haste Rating: " + CH2.currentCharacter.hasteRating.numberValue());
			Trace("Buffed Cast Time: " + buffedCastTime);
			CH2.currentCharacter.castTime = buffedCastTime;
			CH2.currentCharacter.castTimeRemaining = buffedCastTime;
			/* CH2.currentCharacter.skillBeingCast = this; */
			CH2.currentCharacter.changeState(Character.STATE_CASTING);
		}
		else
		{
			this.timesCast++;
			this.effectFunction();
		}
		/* CH2.currentCharacter.onUsedSkill(this); */
	}

	private function addEffect(effect:BitmapAnimation, x:Number, y:Number, canvas:Sprite, delay:Number = 0):void
	{
		var effectTimer:Timer = null;
		if (delay > 0)
		{
			effectTimer = new Timer(delay, 1);
			effectTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function():*
			{
				effect.x = x;
				effect.y = y;
				canvas.addChild(effect);
				effect.addEventListener(Event.ENTER_FRAME, function():*
				{
					if (effect.currentFrame == effect.totalFrames)
					{
						removeEffect(effect);
					}
				});
			});
			effectTimer.start();
		}
		else
		{
			effect.x = x;
			effect.y = y;
			canvas.addChild(effect);
			effect.addEventListener(Event.ENTER_FRAME, function():*
			{
				if (effect.currentFrame == effect.totalFrames)
				{
					removeEffect(effect);
				}
			});
		}
	}

	public function removeEffect(effect:DisplayObject):void
	{
		if (effect.parent != null)
		{
			effect.parent.removeChild(effect);
		}
	}

	public function skillTooltip(body:String):Object
	{
		return {"header": this.name, "body": body + "\n\n" + this.skillCosts()};
	}

	public function buffTooltip(buff:Buff, body:String):Object
	{
		return {"header": buff.name, "body": body};
	}
}
