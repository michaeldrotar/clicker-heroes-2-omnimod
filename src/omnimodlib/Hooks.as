package omnimodlib
{

	public class Hooks
	{
		private var _hookOverrides:Array = [];

		public function after(originalFunction:Function, callbackFunction:Function):Function
		{
			return function(... args):*
			{
				var returnValue:* = originalFunction.apply(this, args);
				callbackFunction.apply(this, args);
				return returnValue;
			};
		}

		public function before(originalFunction:Function, callbackFunction:Function):Function
		{
			return function(... args):*
			{
				callbackFunction.apply(this, args);
				return originalFunction.apply(this, args);
			};
		}

		public function hook(originalFunction:Function, callbackFunction:Function):Function
		{
			return function(... args):*
			{
				// AS3 doesn't support .bind() so polyfilling the functionality
				// to .bind(this) to the originalFn
				var _this:* = this;
				var boundOriginalFunction:Function = function(... args):*
				{
					if (this.toString() === '[object global]')
					{
						return originalFunction.apply(_this, args);
					}

					return originalFunction.apply(this, args);
				};
				return callbackFunction.apply(this, args.concat(boundOriginalFunction));
			};
		}

		public function hookOverride(object:*, overrideName:String, callbackFunction:Function):void
		{
			var hookOverride:Array = Util.find(_hookOverrides, function(value:Object, ... _):Boolean
			{
				return value[0] === object && value[1] === overrideName;
			});
			if (!hookOverride)
			{
				hookOverride = [object, overrideName, object[overrideName + 'Handler']];
				_hookOverrides.push(hookOverride);
			}

			var handler:* = hookOverride[2];
			var next:Function = handler && handler[overrideName + 'Override'] ? handler[overrideName + 'Override'] : object[overrideName + 'Default'];
			var overrideObject:Object = {};
			overrideObject[overrideName + 'Override'] = function(... args):*
			{
				return callbackFunction.apply(this, args.concat(next));
			};

			object[overrideName + 'Handler'] = overrideObject;
			hookOverride[2] = overrideObject;
		}
	}
}