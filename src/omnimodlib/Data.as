package omnimodlib
{

	public class Data extends Extension
	{
		private var _backup:Object;

		public function Data()
		{
			super();
		}

		public function hasKey(key:*):Boolean
		{
			return _keys.indexOf(_getNormalizedKey(key)) !== -1;
		}

		public function getArray(key:*):Array
		{
			return _getValue(key) as Array;
		}

		public function setArray(key:*, value:Array):void
		{
			_setValue(key, value);
		}

		public function getBoolean(key:*):Boolean
		{
			return _getValue(key) as Boolean;
		}

		public function setBoolean(key:*, value:Boolean):void
		{
			_setValue(key, value);
		}

		public function getDate(key:*):Date
		{
			var epoc:Number = _getValue(key) as Number;
			return epoc > 0 ? new Date(epoc) : null;
		}

		public function setDate(key:*, date:Date):void
		{
			_setValue(key, date.valueOf());
		}

		public function getNumber(key:*):Number
		{
			return _getValue(key) as Number;
		}

		public function setNumber(key:*, value:Number):void
		{
			_setValue(key, value);
		}

		public function adjustNumber(key:*, value:Number):void
		{
			_setValue(key, _getValue(key) + value);
		}

		public function getObject(key:*):Object
		{
			return _getValue(key) as Object;
		}

		public function setObject(key:*, value:Object):void
		{
			_setValue(key, value);
		}

		public function getString(key:*):String
		{
			return _getValue(key) as String;
		}

		public function setString(key:*, value:String):void
		{
			_setValue(key, value);
		}

		public function removeKey(key:*):void
		{
			_remove(key);
		}

		public function removeMatched(search:RegExp):void
		{
			var keysToRemove:Array = [];
			var key:String;
			for each (key in _keys)
			{
				if (key.match(search))
				{
					keysToRemove.push(key);
				}
			}
			for each (key in keysToRemove)
			{
				removeKey(key);
			}
		}

		public function toString():String
		{
			var key:String;
			var value:*;
			var keys:Array = _keys.sort();
			var lines:Array = ['DATA (' + keys.length + ' keys)'];
			for each (key in keys)
			{
				value = _getValue(key);
				lines.push(key + ' = ' + value);
			}
			return lines.join('\n');
		}

		private function get _keys():Array
		{
			var key:String;
			var keys:Array = [];
			for (key in currentCharacter.traits)
			{
				if (_isOwnKey(key))
				{
					keys.push(key);
				}
			}
			return keys;
		}

		private function _getNormalizedKey(key:*):String
		{
			var normalizedKey:String = Util.toId(key);
			if (normalizedKey.indexOf(Util.toId(modName)) !== 0)
			{
				normalizedKey = Util.toId([modName, key]);
			}
			return normalizedKey;
		}

		private function _getValue(key:*):*
		{
			return currentCharacter.traits[_getNormalizedKey(key)];
		}

		private function _isOwnKey(key:String):Boolean
		{
			return key.indexOf(Util.toId(modName)) === 0;
		}

		private function _setValue(key:*, value:*):void
		{
			currentCharacter.traits[_getNormalizedKey(key)] = value;
		}

		private function _remove(key:*):void
		{
			delete currentCharacter.traits[_getNormalizedKey(key)];
		}

		public function onDataBackup():void
		{
			_backup = {};

			var key:String;
			var traits:Object = currentCharacter.traits;
			for (key in traits)
			{
				if (_isOwnKey(key))
				{
					_backup[key] = traits[key];
				}
			}
		}

		public function onDataRestore():void
		{
			var key:String;
			var traits:Object = currentCharacter.traits;
			for (key in _backup)
			{
				traits[key] = _backup[key];
			}
		}
	}
}