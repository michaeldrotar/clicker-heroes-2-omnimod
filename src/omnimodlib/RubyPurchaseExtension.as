package omnimodlib
{
	import models.Character;
	import models.RubyPurchase;
	import omnimodlib.Extension;

	public class RubyPurchaseExtension extends Extension
	{
		public var name:String;
		public var iconId:Number;
		public var price:Number;
		public var priority:Number;

		public var getDescription:Function;
		public var getSoldOutText:Function;

		public var canAppear:Function;
		public var canPurchase:Function
		public var onPurchase:Function;

		public var maxQuantity:Number;

		private var _purchased:Number;

		public function RubyPurchaseExtension()
		{
			super();

			name = '';
			iconId = 1;
			price = 0;
			priority = 3;

			getSoldOutText = (new Character()).getDefaultSoldOutText;

			canAppear = canAppearAlways;
			canPurchase = canPurchaseOncePerVisit;
		}

		public function get remainingQuantity():Number
		{
			return Math.max(maxQuantity - _purchased, 0);
		}

		public function get remainingQuantityText():String
		{
			if (maxQuantity <= 1)
			{
				return '';
			}
			return remainingQuantity > 0 ? _(' (Available: {remainingQuantity})') : '';
		}

		public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			var newRubyPurchaseOption:RubyPurchase = toRubyPurchase();

			// In order to support overrides, need a way to determine that the base one wasn't found
			// so the override shouldn't be created
			if (!newRubyPurchaseOption.name)
			{
				return;
			}

			// If the name conflicts with an existing option, remove the existing one
			// so the new one can replace it
			var rubyPurchaseOption:RubyPurchase;
			var index:Number;
			for (index = rubyPurchaseOptions.length - 1; index >= 0; index--)
			{
				rubyPurchaseOption = rubyPurchaseOptions[index];
				if (rubyPurchaseOption.name === newRubyPurchaseOption.name)
				{
					rubyPurchaseOptions.removeAt(index);
				}
			}

			// Add the new option
			rubyPurchaseOptions.push(newRubyPurchaseOption);
		}

		public function onRubyShopGenerating():void
		{
			_purchased = 0;
		}

		public function canAppearAlways():Boolean
		{
			return true;
		}

		public function canAppearWithQuantity():Boolean
		{
			return remainingQuantity > 0;
		}

		public function canPurchaseAlways():Boolean
		{
			return canAppear();
		}

		public function canPurchaseOncePerVisit():Boolean
		{
			return _purchased == 0 && canAppear();
		}

		public function canPurchaseWithQuantity():Boolean
		{
			return remainingQuantity > 0 && canAppear();
		}

		public function copyPurchaseOption(purchaseOptionName:String):Boolean
		{
			var purchaseOptions:Array = currentCharacter.rubyPurchaseOptions;
			var purchaseOption:RubyPurchase;
			var index:Number;

			var basePurchaseOption:RubyPurchase;
			for (index = 0; index < purchaseOptions.length; index++)
			{
				purchaseOption = purchaseOptions[index];
				if (purchaseOption.name === purchaseOptionName)
				{
					basePurchaseOption = purchaseOption;
					break;
				}
			}

			if (!basePurchaseOption)
			{
				return false;
			}

			name = basePurchaseOption.name;
			iconId = basePurchaseOption.iconId;
			price = basePurchaseOption.price;
			priority = basePurchaseOption.priority;

			getDescription = basePurchaseOption.getDescription;
			getSoldOutText = basePurchaseOption.getSoldOutText;

			canAppear = basePurchaseOption.canAppear;
			canPurchase = basePurchaseOption.canPurchase;
			onPurchase = basePurchaseOption.onPurchase;

			return true;
		}

		public function toRubyPurchase():RubyPurchase
		{
			var rubyPurchase:RubyPurchase = new RubyPurchase();
			rubyPurchase.name = name;
			rubyPurchase.iconId = iconId;
			rubyPurchase.price = price;
			rubyPurchase.priority = priority;
			rubyPurchase.getDescription = function():String
			{
				return getDescription() + remainingQuantityText;
			};
			rubyPurchase.getSoldOutText = getSoldOutText;
			rubyPurchase.canAppear = canAppear;
			rubyPurchase.canPurchase = canPurchase;
			rubyPurchase.onPurchase = function():void
			{
				onPurchase();
				_purchased++;
			};
			return rubyPurchase;
		}
	}
}