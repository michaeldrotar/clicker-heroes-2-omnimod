package omnimodlib
{

	public class Fn
	{
		public function Fn()
		{
			super();
		}

		public function find(fn:Function, data:Array):*
		{
			var index:uint, length:uint;
			for (index = 0, length = data.length; index < length; index++)
			{
				if (fn(data[index]))
				{
					return data[index];
				}
			}
			return undefined;
		}

		public function every(fn:Function, data:Array):Boolean
		{
			return data.every(_start(fn));
		}

		public function filter(fn:Function, data:Array):Array
		{
			return data.filter(_start(fn));
		}

		public function map(fn:Function, data:Array):Array
		{
			return data.map(_start(fn));
		}

		public function some(fn:Function, data:Array):Boolean
		{
			return data.some(_start(fn));
		}

		public function and(... args):Function
		{
			return function(value:*):Boolean
			{
				var index:uint, length:uint;
				for (index = 0, length = args.length; index < length; index++)
				{
					var result:Boolean = args[index](value);
					if (!result)
					{
						return result;
					}
				}
				return true;
			}
		}

		public function eq(other:*):Function
		{
			return function(value:*):Boolean
			{
				if (_delegateExists(value, 'eq'))
				{
					return _delegate(value, 'eq', other);
				}
				return value === other;
			}
		}

		public function gt(other:*):Function
		{
			return function(value:*, ... args):Boolean
			{
				if (_delegateExists(value, 'gt'))
				{
					return _delegate(value, 'gt', other);
				}
				return value > other;
			}
		}

		public function gte(other:*):Function
		{
			return function(value:*):Boolean
			{
				if (_delegateExists(value, 'gte'))
				{
					return _delegate(value, 'gte', other);
				}
				return value >= other;
			}
		}

		public function lt(other:*):Function
		{
			return function(value:*):Boolean
			{
				if (_delegateExists(value, 'lt'))
				{
					return _delegate(value, 'lt', other);
				}
				return value < other;
			}
		}

		public function lte(other:*):Function
		{
			return function(value:*):Boolean
			{
				if (_delegateExists(value, 'lte'))
				{
					return _delegate(value, 'lte', other);
				}
				return value <= other;
			}
		}

		public function ne(other:*):Function
		{
			return function(value:*):Boolean
			{
				if (_delegateExists(value, 'eq'))
				{
					return !_delegate(value, 'eq', other);
				}
				return value !== other;
			}
		}

		public function or(... args):Function
		{
			return function(value:*):Boolean
			{
				var index:uint, length:uint;
				for (index = 0, length = args.length; index < length; index++)
				{
					var result:Boolean = args[index](value);
					if (result)
					{
						return result;
					}
				}
				return false;
			}
		}

		public function prop(name:String, comparator:Function = null):Function
		{
			return function(object:*):*
			{
				var value:* = object[name];
				if (comparator is Function)
				{
					return comparator(value);
				}
				return value;
			}
		}

		private function _delegate(value:*, methodName:String, other:*):*
		{
			// Used to make two objects the same type, primarily for converting Number to BigNumber
			// Limited use, might not actually be a good idea, wouldn't work to convert a BigNumber to Number
			// and Number doesn't have the same-named eq, gt, gte, etc methods
			var klass:Class = Util.getClass(value);
			return value[methodName](other is klass ? other : new klass(other));
		}

		private function _delegateExists(value:*, methodName:String):Boolean
		{
			return methodName in value;
		}

		private function _start(fn:Function):Function
		{
			return function(value:*, ... args):*
			{
				return fn(value);
			}
		}
	}
}