package omnimodlib.helpfulAdventurer.gildTalents
{
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Character;
	import omnimodlib.GildTalentExtension;

	public class ChargeTalent extends GildTalentExtension
	{
		public const DAMAGE_MULTIPLIER:Number = 2;
		public const DISTANCE_MULTIPLIER:Number = 0.4;

		public function ChargeTalent()
		{
			super();
			name = 'Charge';
			description = _('Dash now strikes the enemy for { DAMAGE_MULTIPLIER | percent } damage and positions you closer to the enemy.');
			tier = 0;
		}

		public function onCharacterCreated():void
		{
			currentCharacter.onTeleportAttackHandler = this;
		}

		public function onTeleportAttackOverride():void
		{
			if (!CH2.world.getNextMonster())
			{
				return;
			}

			var attackData:AttackData = new AttackData();
			attackData.isClickAttack = true;
			attackData.isTeleportAttack = true;
			attackData.damage = currentCharacter.clickDamage;
			if (active)
			{
				attackData.damage = attackData.damage.multiplyN(DAMAGE_MULTIPLIER);
			}
			teleportOverride();
			currentCharacter.attack(attackData);
		}

		public function teleportOverride():void
		{
			if (!CH2.world.getNextMonster())
			{
				return;
			}

			var previousY:Number = currentCharacter.y;
			var monsterY:Number = CH2.world.getNextMonster().y;

			var attackDistance:Number = currentCharacter.attackRange;
			if (active)
			{
				attackDistance = attackDistance * DISTANCE_MULTIPLIER;
			}
			currentCharacter.y = monsterY - attackDistance;

			currentCharacter.changeState(Character.STATE_COMBAT);
			if (IdleHeroMain.IS_RENDERING)
			{
				currentCharacter.characterDisplay.playDash(currentCharacter.y - previousY);
			}
		}
	}
}