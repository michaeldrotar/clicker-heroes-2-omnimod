package omnimodlib.helpfulAdventurer.gildTalents
{
	import models.Buff;
	import omnimodlib.GildTalentExtension;

	public class SleeperCellsTalent extends GildTalentExtension
	{
		public const ENERGY_REGEN_MULTIPLIER:Number = 10;
		public const MANA_REGEN_MULTIPLIER:Number = 20;

		private var _hasBuff:Boolean;

		public function SleeperCellsTalent()
		{
			super();
			name = 'Sleeper Cells';
			description = _('Regenerates { ENERGY_REGEN_MULTIPLIER | percent } energy and { MANA_REGEN_MULTIPLIER | percent } mana when paused. Energy regeneration is based on how much you would generate if constantly auto-attacking.');
			tier = 0;
		}

		public function get enabled():Boolean
		{
			return active && currentCharacter.isPaused;
		}

		public function get energyPerSecond():Number
		{
			return currentCharacter.energyRegeneration / (currentCharacter.attackDelay / 1000);
		}

		public function get manaPerSecond():Number
		{
			return currentCharacter.getManaRegenRate();
		}

		public function addBuff():void
		{
			var buff:Buff = new Buff();
			buff.name = 'Sleeper Cells';
			buff.iconId = 152;
			buff.isUntimedBuff = true;
			buff.tooltipFunction = getBuffTooltip;
			currentCharacter.buffs.addBuff(buff);
			_hasBuff = true;
		}

		public function getBuffTooltip():Object
		{
			var body:String = _('Regenerating { 0 | label:energyPerSecond } and { 1 | label:manaPerSecond }.', energyPerSecond * ENERGY_REGEN_MULTIPLIER, manaPerSecond * MANA_REGEN_MULTIPLIER);
			return {header: name, body: body};
		}

		public function getTooltip():Object
		{
			return {header: name, body: description};
		}

		public function removeBuff():void
		{
			currentCharacter.buffs.removeBuff('Sleeper Cells');
			_hasBuff = false;
		}

		public function onCharacterCreated():void
		{
			currentCharacter.regenerateManaAndEnergyHandler = this;
		}

		public function onCharacterStarted():void
		{
			_hasBuff = false;
		}

		public function onCharacterUpdate(dt:int):void
		{
			if (enabled && !_hasBuff)
			{
				addBuff();
			}
			else if (!enabled && _hasBuff)
			{
				removeBuff();
			}
		}

		public function regenerateManaAndEnergyOverride(dt:Number):void
		{
			var multiplier:Number = dt / 1000;

			if (enabled)
			{
				extensions.energy.addEnergy(ENERGY_REGEN_MULTIPLIER * (energyPerSecond * multiplier), false);
				extensions.mana.addMana(MANA_REGEN_MULTIPLIER * (manaPerSecond * multiplier), false);
			}
			else
			{
				extensions.mana.addMana(manaPerSecond * multiplier, false);
			}
		}
	}
}