package omnimodlib.helpfulAdventurer.gildTalents
{
	import heroclickerlib.CH2;
	import heroclickerlib.GpuMovieClip;
	import heroclickerlib.managers.CH2AssetManager;
	import models.AttackData;
	import models.Monster;
	import omnimodlib.GildTalentExtension;

	public class ShadowStrikesTalent extends GildTalentExtension
	{
		public const SHADOW_STRIKE_CHANCE:Number = 0.10;

		public function ShadowStrikesTalent()
		{
			super();
			name = 'Shadow Strikes';
			description = _('Each auto attack has a { SHADOW_STRIKE_CHANCE | percent } chance to hit an additional enemy and restore additional energy.');
			tier = 0;
		}

		public function shadowStrike():void
		{
			var index:uint;

			var monsters:Vector.<Monster> = CH2.world.monsters.monsters;
			var nextMonster:Monster = CH2.world.getNextMonster();
			var monster:Monster;
			var targetMonster:Monster;

			if (!nextMonster)
			{
				return;
			}

			if (nextMonster.y > currentCharacter.y + currentCharacter.attackRange)
			{
				targetMonster = nextMonster;
			}
			else
			{
				for (index = 0; index < monsters.length; index++)
				{
					monster = monsters[index];
					if (monster.isAlive && monster.y >= currentCharacter.y && monster !== nextMonster)
					{
						targetMonster = monster;
						break;
					}
				}
			}

			if (!targetMonster)
			{
				return;
			}

			var effect:GpuMovieClip = CH2AssetManager.instance.getGpuMovieClip('HelpfulAdventurer_bamplode' + roller.integer(1, 3));
			effect.gotoAndPlay(1);
			effect.isLooping = false;
			CH2.world.addEffect(effect, CH2.world.roomsFront, targetMonster.x, targetMonster.y);

			var attackData:AttackData = new AttackData();
			attackData.isAutoAttack = true;
			attackData.damage = currentCharacter.autoAttackDamage;
			attackData.isCritical = roller.boolean(currentCharacter.criticalChance);
			attackData.monster = targetMonster;
			if (attackData.isCritical)
			{
				attackData.damage = attackData.damage.multiplyN(currentCharacter.criticalDamageMultiplier);
			}
			currentCharacter.buffs.onAttack(attackData);
			attackData.monster.takeDamage(attackData);
			currentCharacter.playRandomHitSound(attackData);

			extensions.energy.addEnergy(currentCharacter.energyRegeneration, false);
		}

		public function shouldShadowStrike():Boolean
		{
			return active && roller.boolean(SHADOW_STRIKE_CHANCE);
		}

		public function onCharacterCreated():void
		{
			currentCharacter.autoAttackHandler = this;
		}

		public function autoAttackOverride():void
		{
			currentCharacter.autoAttackDefault();
			if (shouldShadowStrike())
			{
				shadowStrike();
			}
		}
	}
}