package omnimodlib.helpfulAdventurer.skills
{
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class ManaCritExtension extends Extension
	{
		public static const CRITICAL_HIT_CHANCE:Number = 1;

		public function onCreateSkills():void
		{
			var manaCrit:Skill = extensions.skills.getStaticSkill('Mana Crit');

			manaCrit.tooltipFunction = function():Object
			{
				return createTooltip(_('Mana Crit'), _('Clicks with a { 0 | percent | color:value } chance to score a critical hit.', CRITICAL_HIT_CHANCE) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T7'];
			nodeType['tooltip'] = _('Clicks with a { 0 | percent | color:value } chance to score a critical hit.', CRITICAL_HIT_CHANCE);
		}
	}
}