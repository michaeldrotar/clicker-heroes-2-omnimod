package omnimodlib.helpfulAdventurer.skills
{
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Buff;
	import models.Character;
	import models.Monster;
	import models.Skill;
	import omnimodlib.Extension;

	public class HugeClickExtension extends Extension
	{
		public static const AREA_OF_EFFECT_DISTANCE:Number = 3 * Character.ONE_METER_Y_DISTANCE;
		public static const AREA_OF_EFFECT_DAMAGE_MULTIPLIER:Number = 0.5;
		public static const DAMAGE_MULTIPLIER:Number = 10;

		public function getCurrentAreaOfEffectDamageMultiplier():Number
		{
			return getCurrentDamageMultiplier() * AREA_OF_EFFECT_DAMAGE_MULTIPLIER;
		}

		public function getCurrentDamageMultiplier():Number
		{
			return DAMAGE_MULTIPLIER * Math.pow(1.25, currentCharacter.getTrait('HugeClickDamage'));
		}

		public function onCreateSkills():void
		{
			var hugeClick:Skill = extensions.skills.getStaticSkill('Huge Click');

			hugeClick.effectFunction = after(hugeClick.effectFunction, function():void
			{
				var areaOfEffectDamageMultiplier:Number = getCurrentAreaOfEffectDamageMultiplier();
				var damageMultiplier:Number = getCurrentDamageMultiplier();

				var buff:Buff = extensions.buffs.getCurrentBuff('Huge Click');

				buff.attackFunction = hook(buff.attackFunction, function(attackData:AttackData, originalFunction:Function):void
				{
					// check if huge click will be used
					var isHugeClick:Boolean = attackData.isClickAttack && buff.stacks % 20 == 1;

					// peform default effect
					originalFunction(attackData);

					// abort if not using huge click
					if (!isHugeClick)
					{
						return;
					}

					// get monsters in aoe
					var targetAttack:AttackData = attackData;
					var targetMonster:Monster = targetAttack.monster;
					var monsters:Array = CH2.world.monsters.getMonstersInCenter(targetMonster.x, targetMonster.y, AREA_OF_EFFECT_DISTANCE);

					// calculate base damage
					var areaOfEffectAttack:AttackData = targetAttack.getCopy();
					if (areaOfEffectAttack.isCritical)
					{
						areaOfEffectAttack.damage = areaOfEffectAttack.damage.divideN(currentCharacter.criticalDamageMultiplier);
						areaOfEffectAttack.isCritical = false;
					}
					areaOfEffectAttack.damage = areaOfEffectAttack.damage.divideN(damageMultiplier).multiplyN(areaOfEffectDamageMultiplier);

					// damage monsters
					var index:uint;
					var monster:Monster;
					var monsterAttack:AttackData;
					for (index = 0; index < monsters.length; index++)
					{
						monster = monsters[index];
						if (monster !== targetMonster && monster.isAlive)
						{
							monsterAttack = areaOfEffectAttack.getCopy();
							monsterAttack.monster = monster;
							monsterAttack.isCritical = roller.boolean(currentCharacter.criticalChance);
							if (monsterAttack.isCritical)
							{
								monsterAttack.damage = monsterAttack.damage.multiplyN(currentCharacter.criticalDamageMultiplier);
							}
							monster.takeDamage(monsterAttack);
						}
					}
				});

				buff.tooltipFunction = function():Object
				{
					return createTooltip(_('Huge Click'), _('Your next click will deal { 0 | percent | label:damage } to the target. Monsters within { 1 | distance } of your target will take { 2 | percent | label:damage }.', damageMultiplier, AREA_OF_EFFECT_DISTANCE, areaOfEffectDamageMultiplier));
				};
			});

			hugeClick.tooltipFunction = function():Object
			{
				return createTooltip(_('Huge Click'), _('Causes your next click to deal { 0 | percent | label:damage } to the target. Monsters within { 1 | distance } of your target take { 2 | percent | label:damage }.', getCurrentDamageMultiplier(), AREA_OF_EFFECT_DISTANCE, getCurrentAreaOfEffectDamageMultiplier()) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T5'];
			nodeType['tooltip'] = _('Causes your next click to deal { 0 | percent | label:damage } to the target. Monsters within { 1 | distance } of your target take { 2 | percent | label:damage }.', DAMAGE_MULTIPLIER, AREA_OF_EFFECT_DISTANCE, AREA_OF_EFFECT_DAMAGE_MULTIPLIER);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A05', 'Helpful Adventurer_17', 'Huge Click');
		}
	}
}