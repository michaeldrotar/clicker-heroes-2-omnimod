package omnimodlib.helpfulAdventurer.skills
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Buff;
	import models.Character;
	import models.Monster;
	import models.Skill;
	import omnimodlib.Extension;

	public class GoldenClicksExtension extends Extension
	{
		public static const CLICKS_PER_SECOND:Number = 2.5;
		public static const ENERGY_COST_PER_CLICK:Number = 0.5;
		public static const GOLD_MULTIPLIER:Number = 0.75;
		public static const STACK_DURATION:Number = 60 * 1000;

		public function buffFunction(buff:Buff):void
		{
			buff.iconId = 201;

			buff.attackFunction = function(attackData:AttackData):void
			{
				if (!attackData.isClickAttack || attackData.monster.health.lteN(0))
				{
					return;
				}

				var monster:Monster = attackData.monster;

				var effectiveDamage:BigNumber;
				if (attackData.damage.lt(monster.health))
				{
					effectiveDamage = attackData.damage;
				}
				else
				{
					effectiveDamage = monster.health;
				}

				var percentDamage:BigNumber = effectiveDamage.divide(monster.maxHealth);
				var goldAmount:BigNumber = percentDamage.multiply(monster.goldReward()).multiplyN(GOLD_MULTIPLIER * buff.stacks);
				currentCharacter.addGold(goldAmount);
			}

			buff.tickFunction = after(buff.tickFunction, function():void
			{
				currentCharacter.clickAttack(false);
			});
		}

		public function buffTooltipFunction(buff:Buff, stackedClicksPerSecond:Number, stackedEnergyCostPerSecond:Number):Object
		{
			var stackedGoldMultiplier:Number = GOLD_MULTIPLIER * buff.stacks;
			return createTooltip(_('Golden Clicks'), _('Performing { 0 | label:clicksPerSecond } until you run out of energy. All clicks are awarding bonus gold based on the percent of damage done to the monster\'s health. Total bonus gold is worth { 2 | percent | color:value } of the monster\'s total worth. Consuming { 1 | label:energyPerSecond } while clicking.', stackedClicksPerSecond, stackedEnergyCostPerSecond, stackedGoldMultiplier));
		}

		public function skillTooltipFunction(skill:Skill, currentClicksPerSecond:Number, currentStackDuration:Number):Object
		{
			return createTooltip(_('Golden Clicks'), _('Performs { 0 | label:clicksPerSecond } until you run out of energy. While active, all clicks award bonus gold based on the percent of damage done to the monster\'s health. Total bonus gold is worth { 2 | percent | color:value } of the monster\'s total worth. Speed and total bonus gold increase every { 1 | duration }.', currentClicksPerSecond, currentStackDuration, GOLD_MULTIPLIER));
		}

		public function onCreateSkills():void
		{
			var stormOverride:Object = {};
			stormOverride['name'] = 'GoldenClicks';
			stormOverride['baseTicksPerSecond'] = CLICKS_PER_SECOND;
			stormOverride['baseEnergyCostPerTick'] = ENERGY_COST_PER_CLICK;
			stormOverride['baseStackDuration'] = STACK_DURATION;
			stormOverride['buffFunction'] = buffFunction;
			stormOverride['buffTooltipFunction'] = buffTooltipFunction;
			stormOverride['skillTooltipFunction'] = skillTooltipFunction;
			extensions.storms.overrideStorm(stormOverride);
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['Q41'];
			nodeType['tooltip'] = _('Performs { 0 | label:clicksPerSecond } until you run out of energy. While active, all clicks award bonus gold based on the percent of damage done to the monster\'s health. Total bonus gold is worth { 2 | percent | color:value } of the monster\'s total worth. Speed and total bonus gold increase every { 1 | duration }.', CLICKS_PER_SECOND, STACK_DURATION, GOLD_MULTIPLIER);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A13', 'Helpful Adventurer_58', 'GoldenClicks');
		}
	}
}
