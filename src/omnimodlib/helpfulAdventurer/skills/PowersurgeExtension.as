package omnimodlib.helpfulAdventurer.skills
{
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Buff;
	import models.Character;
	import models.Monster;
	import models.Skill;
	import omnimodlib.Extension;

	public class PowersurgeExtension extends Extension
	{
		public static const AUTO_ATTACK_AREA_OF_EFFECT_DISTANCE:Number = 3 * Character.ONE_METER_Y_DISTANCE;
		public static const AUTO_ATTACK_DAMAGE_MULTIPLIER:Number = 0.5;
		public static const DAMAGE_MULTIPLIER:Number = 2;
		public static const DURATION:Number = 60 * 1000;

		public function getCurrentDamageMultiplier():Number
		{
			var improvedPowersurgeStacks:Number = currentCharacter.getTrait('ImprovedPowersurge');
			return DAMAGE_MULTIPLIER * Math.pow(1.25, improvedPowersurgeStacks);
		}

		public function getCurrentDuration():Number
		{
			var sustainedPowersurgeStacks:Number = currentCharacter.getTrait('SustainedPowersurge');
			return (DURATION * Math.pow(1.2, sustainedPowersurgeStacks)) / currentCharacter.hasteRating.numberValue();
		}

		public function onCreateSkills():void
		{
			var powersurge:Skill = extensions.skills.getStaticSkill('Powersurge');

			powersurge.effectFunction = after(powersurge.effectFunction, function():void
			{
				var damageMultiplier:Number = getCurrentDamageMultiplier();
				var duration:Number = getCurrentDuration();

				var buff:Buff = extensions.buffs.getCurrentBuff('Powersurge');

				buff.attackFunction = after(buff.attackFunction, function(attackData:AttackData):void
				{
					// abort if not an auto-attack
					if (!attackData.isAutoAttack)
					{
						return;
					}

					// get monsters in aoe
					var targetAttack:AttackData = attackData;
					var targetMonster:Monster = targetAttack.monster;
					var monsters:Array = CH2.world.monsters.getMonstersInCenter(currentCharacter.x, currentCharacter.y, AUTO_ATTACK_AREA_OF_EFFECT_DISTANCE);

					// calculate base damage
					var areaOfEffectAttack:AttackData = targetAttack.getCopy();
					areaOfEffectAttack.isCritical = false;
					areaOfEffectAttack.damage = currentCharacter.autoAttackDamage.multiplyN(AUTO_ATTACK_DAMAGE_MULTIPLIER);

					// damage monsters
					var index:uint;
					var monster:Monster;
					var monsterAttack:AttackData;
					for (index = 0; index < monsters.length; index++)
					{
						monster = monsters[index];
						if (monster !== targetMonster)
						{
							monsterAttack = areaOfEffectAttack.getCopy();
							monsterAttack.monster = monster;
							monsterAttack.isCritical = roller.boolean(currentCharacter.criticalChance);
							if (monsterAttack.isCritical)
							{
								monsterAttack.damage = monsterAttack.damage.multiplyN(currentCharacter.criticalDamageMultiplier);
							}
							monster.takeDamage(monsterAttack);
						}
					}
				});

				buff.tooltipFunction = function():Object
				{
					return createTooltip(_('Powersurge'), _('Your clicks deal { 0 | percent | label:damage }. Your auto-attacks deal { 1 | percent | label:damage } to monsters within { 2 | distance }.', damageMultiplier, AUTO_ATTACK_DAMAGE_MULTIPLIER, AUTO_ATTACK_AREA_OF_EFFECT_DISTANCE));
				};
			});

			powersurge.tooltipFunction = function():Object
			{
				return createTooltip(_('Powersurge'), _('Causes your clicks within { 0 | duration } to deal { 1 | percent | label:damage }. Auto-attacks during this time deal { 2 | percent | label:damage } to monsters within { 3 | distance }.', getCurrentDuration(), getCurrentDamageMultiplier(), AUTO_ATTACK_DAMAGE_MULTIPLIER, AUTO_ATTACK_AREA_OF_EFFECT_DISTANCE) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T6'];
			nodeType['tooltip'] = _('Causes your clicks within { 0 | duration } to deal { 1 | percent | label:damage }. Auto-attacks during this time deal { 2 | percent | label:damage } to monsters within { 3 | distance }.', DURATION, DAMAGE_MULTIPLIER, AUTO_ATTACK_DAMAGE_MULTIPLIER, AUTO_ATTACK_AREA_OF_EFFECT_DISTANCE);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A06', 'Helpful Adventurer_19', 'Powersurge');
		}
	}
}