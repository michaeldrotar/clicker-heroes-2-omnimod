package omnimodlib.helpfulAdventurer.skills
{
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class EnergizeExtension extends Extension
	{
		public static const DURATION:Number = 60 * 1000;
		public static const ENERGY_RESTORED_PER_SECOND:Number = 2;

		public function getCurrentDuration():Number
		{
			var stacks:Number = currentCharacter.getTrait('ImprovedEnergize');
			return (DURATION + (DURATION * (stacks * 0.2))) / currentCharacter.hasteRating.numberValue();
		}

		public function getCurrentEnergyRestoredPerSecond():Number
		{
			return ENERGY_RESTORED_PER_SECOND * currentCharacter.hasteRating.numberValue();
		}

		public function onCreateSkills():void
		{
			var energize:Skill = extensions.skills.getStaticSkill('Energize');

			energize.effectFunction = after(energize.effectFunction, function():void
			{
				var energyRestoredPerSecond:Number = getCurrentEnergyRestoredPerSecond();

				var buff:Buff = extensions.buffs.getCurrentBuff('Energize');
				buff.tooltipFunction = function():Object
				{
					return createTooltip(_('Energize'), _('Restoring { 0 | label:energyPerSecond }.', energyRestoredPerSecond));
				};
			});

			energize.tooltipFunction = function():Object
			{
				return createTooltip(_('Energize'), _('Restores { 0 | label:energyPerSecond } for { 1 | duration }.', getCurrentEnergyRestoredPerSecond(), getCurrentDuration()) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T2'];
			nodeType['tooltip'] = _('Restores { 0 | label:energyPerSecond } for { 1 | duration }.', ENERGY_RESTORED_PER_SECOND, DURATION);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A02', 'Helpful Adventurer_18', 'Energize');
		}
	}
}