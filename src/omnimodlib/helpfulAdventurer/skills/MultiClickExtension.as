package omnimodlib.helpfulAdventurer.skills
{
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class MultiClickExtension extends Extension
	{
		public static const CLICKS:Number = 5;
		public static const LOSS_MULTIPLIER:Number = 0.2;

		public function getCurrentClicks():Number
		{
			var extraClicks:Number = currentCharacter.getTrait('ExtraMulticlicks');
			var flurryMultiplier:Number = currentCharacter.getTrait('Flurry') ? currentCharacter.hasteRating.numberValue() : 1;
			return Math.ceil((CLICKS + extraClicks) * flurryMultiplier);
		}

		public function onCreateSkills():void
		{
			var multiClick:Skill = extensions.skills.getStaticSkill('MultiClick');

			multiClick.tooltipFunction = function():Object
			{
				return createTooltip(_('MultiClick'), _('Performs { 0 | label:clicks }. Dashing consumes { 1 | percent | color:value } of remaining clicks.', getCurrentClicks(), LOSS_MULTIPLIER) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T3'];
			nodeType['tooltip'] = _('Performs { 0 | label:clicks }. Dashing consumes { 1 | percent | color:value } of remaining clicks.', CLICKS, LOSS_MULTIPLIER);
		}
	}
}