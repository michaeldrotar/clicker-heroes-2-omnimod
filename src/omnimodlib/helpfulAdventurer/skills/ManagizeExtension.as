package omnimodlib.helpfulAdventurer.skills
{
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class ManagizeExtension extends Extension
	{
		public static const MANA_RESTORED_PERCENT:Number = 0.25;

		public function getCurrentManaRestoredPercent():Number
		{
			var stacks:Number = currentCharacter.getTrait('ImprovedEnergize');
			return MANA_RESTORED_PERCENT + (MANA_RESTORED_PERCENT * stacks * 0.2);
		}

		public function onCreateSkills():void
		{
			var managize:Skill = extensions.skills.getStaticSkill('Managize');

			managize.tooltipFunction = function():Object
			{
				return createTooltip(_('Managize'), _('Restores { 0 | percent | color:mana } of your maximum mana.', getCurrentManaRestoredPercent()) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['Q30'];
			nodeType['tooltip'] = _('Restores { 0 | percent | color:mana } of your maximum mana.', MANA_RESTORED_PERCENT);
		}
	}
}