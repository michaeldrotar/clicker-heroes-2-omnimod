package omnimodlib.helpfulAdventurer.skills
{
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class AutoAttackstormExtension extends Extension
	{
		public static const AUTO_ATTACKS_PER_SECOND:Number = 2.5;
		public static const MANA_COST_PER_AUTO_ATTACK:Number = 0.5;
		public static const STACK_DURATION:Number = 60 * 1000;

		public function buffFunction(buff:Buff):void
		{
			buff.iconId = 201;

			buff.tickFunction = after(buff.tickFunction, function():void
			{
				// Perform an auto-attack but count it against the click timer instead so that it
				// doesn't interrupt auto-attacks for Synchrony
				var attackTimer:Number = currentCharacter.timeSinceLastAutoAttack;
				currentCharacter.autoAttack();
				currentCharacter.timeSinceLastClickAttack = currentCharacter.timeSinceLastAutoAttack;
				currentCharacter.timeSinceLastAutoAttack = attackTimer;
			});
		}

		public function buffTooltipFunction(buff:Buff, stackedAutoAttacksPerSecond:Number, stackedManaCostPerSecond:Number):Object
		{
			return createTooltip(_('Auto-attackstorm'), _('Performing { 0 | label:autoAttacksPerSecond } until you run out of energy. Consuming { 1 | label:manaPerSecond } while auto-attacking.', stackedAutoAttacksPerSecond, stackedManaCostPerSecond));
		}

		public function skillTooltipFunction(skill:Skill, currentAutoAttacksPerSecond:Number, currentStackDuration:Number):Object
		{
			return createTooltip(_('Auto-attackstorm'), _('Performs { 0 | label:autoAttacksPerSecond } until you run out of mana. Speed increases every { 1 | duration }.', currentAutoAttacksPerSecond, currentStackDuration));
		}

		public function onCreateSkills():void
		{
			var stormOverride:Object = {};
			stormOverride['name'] = 'Autoattackstorm';
			stormOverride['baseTicksPerSecond'] = AUTO_ATTACKS_PER_SECOND;
			stormOverride['baseManaCostPerTick'] = MANA_COST_PER_AUTO_ATTACK;
			stormOverride['baseStackDuration'] = STACK_DURATION;
			stormOverride['buffFunction'] = buffFunction;
			stormOverride['buffTooltipFunction'] = buffTooltipFunction;
			stormOverride['skillTooltipFunction'] = skillTooltipFunction;
			extensions.storms.overrideStorm(stormOverride);
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['Q29'];
			nodeType['tooltip'] = _('Performs { 0 | label:autoAttacksPerSecond } until you run out of mana. Speed increases every { 1 | duration }.', AUTO_ATTACKS_PER_SECOND, STACK_DURATION);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A11', 'Helpful Adventurer_56', 'Autoattackstorm');
		}
	}
}