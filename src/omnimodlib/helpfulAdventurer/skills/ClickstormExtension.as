package omnimodlib.helpfulAdventurer.skills
{
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class ClickstormExtension extends Extension
	{
		public static const CLICKS_PER_SECOND:Number = 5;
		public static const ENERGY_COST_PER_CLICK:Number = 0.5;
		public static const STACK_DURATION:Number = 60 * 1000;

		public function buffFunction(buff:Buff):void
		{
			buff.iconId = 200;

			buff.tickFunction = after(buff.tickFunction, function():void
			{
				currentCharacter.clickAttack(false);
			});
		}

		public function buffTooltipFunction(buff:Buff, stackedClicksPerSecond:Number, stackedEnergyCostPerSecond:Number):Object
		{
			return createTooltip(_('Clickstorm'), _('Performing { 0 | label:clicksPerSecond } until you run out of energy. Consuming { 1 | label:energyPerSecond } while clicking.', stackedClicksPerSecond, stackedEnergyCostPerSecond));
		}

		public function skillTooltipFunction(skill:Skill, currentClicksPerSecond:Number, currentStackDuration:Number):Object
		{
			return createTooltip(_('Clickstorm'), _('Performs { 0 | label:clicksPerSecond } until you run out of energy. Speed increases every { 1 | duration }.', currentClicksPerSecond, currentStackDuration));
		}

		public function onCreateSkills():void
		{
			var stormOverride:Object = {};
			stormOverride['name'] = 'Clickstorm';
			stormOverride['baseTicksPerSecond'] = CLICKS_PER_SECOND;
			stormOverride['baseEnergyCostPerTick'] = ENERGY_COST_PER_CLICK;
			stormOverride['baseStackDuration'] = STACK_DURATION;
			stormOverride['buffFunction'] = buffFunction;
			stormOverride['buffTooltipFunction'] = buffTooltipFunction;
			stormOverride['skillTooltipFunction'] = skillTooltipFunction;
			extensions.storms.overrideStorm(stormOverride);
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T4'];
			nodeType['tooltip'] = _('Performs { 0 | label:clicksPerSecond } until you run out of energy. Speed increases every { 1 | duration }.', CLICKS_PER_SECOND, STACK_DURATION);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A04', 'Helpful Adventurer_51', 'Clickstorm');
		}
	}
}