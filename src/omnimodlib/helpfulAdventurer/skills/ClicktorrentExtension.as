package omnimodlib.helpfulAdventurer.skills
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Buff;
	import models.Character;
	import models.Monster;
	import models.Skill;
	import omnimodlib.Extension;

	public class ClicktorrentExtension extends Extension
	{
		public static const CLICKS_PER_SECOND:Number = 50;
		public static const ENERGY_COST_PER_CLICK:Number = 1;
		public static const PUSH_DISTANCE:Number = 1 * Character.ONE_METER_Y_DISTANCE;
		public static const ZONE_DAMAGE_PERCENT_MINIMUM:Number = 0;
		public static const ZONE_DAMAGE_PERCENT_MAXIMUM:Number = 1;
		public static const DURATION:Number = 5 * 1000;

		private var _damageCounter:BigNumber = new BigNumber(0);
		private var _isClicktorrentAttack:Boolean = false;

		public function buffFunction(buff:Buff):void
		{
			_damageCounter = new BigNumber(0);
			_isClicktorrentAttack = false;

			buff.iconId = 202;

			buff.attackFunction = function(attackData:AttackData):void
			{
				if (!_isClicktorrentAttack)
				{
					return;
				}

				_damageCounter = _damageCounter.add(attackData.damage);
			};

			buff.tickFunction = after(buff.tickFunction, function():void
			{
				_isClicktorrentAttack = true;
				currentCharacter.clickAttack(false);
				_isClicktorrentAttack = false;
			});

			buff.finishFunction = function():void
			{
				var attackData:AttackData = new AttackData();
				attackData.isClickAttack = true;
				attackData.isCritical = false;

				var index:uint;
				var monstersInZone:Vector.<Monster> = extensions.monsterSpawner.monsters;
				var monster:Monster;
				var percent:Number;
				var maxY:Number = (monstersInZone[monstersInZone.length - 1] as Monster).y;
				for (index = 0; index < monstersInZone.length; index++)
				{
					monster = monstersInZone[index];
					attackData.monster = monster;

					percent = roller.randFloat();

					if (monster.isAlive)
					{
						monster.y = Math.min(monster.y + PUSH_DISTANCE, maxY);
						attackData.damage = _damageCounter.multiplyN(ZONE_DAMAGE_PERCENT_MINIMUM + (percent * (ZONE_DAMAGE_PERCENT_MAXIMUM - ZONE_DAMAGE_PERCENT_MINIMUM)));
						attackData.monster.takeDamage(attackData);

						currentCharacter.buffs.onAttack(attackData);
					}
				}

				// make it sound like a crit
				attackData.isCritical = true;
				currentCharacter.playRandomHitSound(attackData);

				// allow character to move up to re-engage pushed monsters
				monster = CH2.world.getNextMonster();
				if (monster && monster.y > currentCharacter.y + currentCharacter.attackRange)
				{
					currentCharacter.state = Character.STATE_ENDING_COMBAT;
				}
			};
		}

		public function buffTooltipFunction(buff:Buff, stackedClicksPerSecond:Number, stackedEnergyCostPerSecond:Number):Object
		{
			var totalTicks:Number = CLICKS_PER_SECOND * (DURATION / 1000);

			return createTooltip(_('Clicktorrent'), _('Performing up to { 0 | label:clicks } to unleash a Clicktorrent.', totalTicks));
		}

		public function skillTooltipFunction(skill:Skill, currentClicksPerSecond:Number, currentStackDuration:Number):Object
		{
			return createTooltip(_('Clicktorrent'), _('Performs { 0 | label:clicksPerSecond } for { 1 | duration } or until you run out of energy. Once finished, all monsters in the zone are pushed back { 2 | distance } and damaged a random amount up to { 3 | percent | color:value } of the damage Clicktorrent did.\n\n|c:label:Cannot push bosses or push monsters past bosses.|r', currentClicksPerSecond, currentStackDuration, PUSH_DISTANCE, ZONE_DAMAGE_PERCENT_MAXIMUM));
		}

		public function onCreateSkills():void
		{
			var stormOverride:Object = {};
			stormOverride['name'] = 'Clicktorrent';
			stormOverride['baseTicksPerSecond'] = CLICKS_PER_SECOND;
			stormOverride['baseEnergyCostPerTick'] = ENERGY_COST_PER_CLICK;
			stormOverride['baseStackDuration'] = DURATION;
			stormOverride['buffFunction'] = buffFunction;
			stormOverride['buffTooltipFunction'] = buffTooltipFunction;
			stormOverride['skillTooltipFunction'] = skillTooltipFunction;
			extensions.storms.overrideStorm(stormOverride);
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['Q88'];
			nodeType['tooltip'] = _('Performs { 0 | label:clicksPerSecond } for { 1 | duration } or until you run out of energy. Once finished, all monsters in the zone are pushed back { 2 | distance } and damaged a random amount up to { 3 | percent | color:value } of the damage Clicktorrent did.', CLICKS_PER_SECOND, DURATION, PUSH_DISTANCE, ZONE_DAMAGE_PERCENT_MAXIMUM);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A12', 'Helpful Adventurer_57', 'Clicktorrent');
		}
	}
}
