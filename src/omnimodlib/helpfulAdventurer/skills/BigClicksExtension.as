package omnimodlib.helpfulAdventurer.skills
{
	import models.AutomatorGem;
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class BigClicksExtension extends Extension
	{
		public static const DAMAGE_MULTIPLIER:Number = 3;
		public static const STACKS:Number = 6;

		public function getCurrentDamageMultiplier():Number
		{
			var buffAmount:Number = 1.25;
			var buffStacks:Number = currentCharacter.getTrait('BigClicksDamage');

			var multiplier:Number = DAMAGE_MULTIPLIER * Math.pow(buffAmount, buffStacks);
			if (currentCharacter.getTrait('DistributedBigClicks'))
			{
				multiplier = (multiplier - 1) * 0.5 + 1;
			}
			return multiplier;
		}

		public function getCurrentStacks():Number
		{
			var extraStacks:Number = currentCharacter.getTrait('BigClickStacks');
			return STACKS + extraStacks;
		}

		public function onCreateSkills():void
		{
			var bigClicks:Skill = extensions.skills.getStaticSkill('Big Clicks');

			bigClicks.effectFunction = after(bigClicks.effectFunction, function():void
			{
				var damageMultiplier:Number = getCurrentDamageMultiplier();

				var buff:Buff = extensions.buffs.getCurrentBuff('Big Clicks');
				buff.tooltipFunction = function():Object
				{
					return createTooltip(_('Big Clicks'), _('Your next { 0 | label:clicks } will deal { 1 | percent | label:damage }', buff.stacks, damageMultiplier));
				};
			});

			bigClicks.tooltipFunction = function():Object
			{
				return createTooltip(_('Big Clicks'), _('Causes your next { 0 | label:clicks } to deal { 1 | percent | label:damage }.', getCurrentStacks(), getCurrentDamageMultiplier()) + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T1'];
			nodeType['tooltip'] = _('Causes your next { 0 | label:clicks } to deal { 1 | percent | label:damage }.', STACKS, DAMAGE_MULTIPLIER);

			var gemNodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['A03'];
			gemNodeType['setupFunction'] = after(gemNodeType['setupFunction'], function():void
			{
				var gem:AutomatorGem = extensions.automator.getStaticGem('Helpful Adventurer_16');
				gem.description = _('Activates Big Clicks on cooldown. Won\'t activate Big Clicks if you already have the Big Clicks buff unless you also have Limitless Big Clicks.');
				extensions.automator.setGemCanActivateFunction(gem, hook(gem.canActivate, function(originalFunction:Function):Boolean
				{
					return originalFunction() && (currentCharacter.getTrait('UnlimitedBigClicks') > 0 || !extensions.buffs.getCurrentBuff('Big Clicks'));
				}));
			});
		}
	}
}