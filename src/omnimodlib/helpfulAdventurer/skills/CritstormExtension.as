package omnimodlib.helpfulAdventurer.skills
{
	import heroclickerlib.CH2;
	import models.Buff;
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class CritstormExtension extends Extension
	{
		public static const CLICKS_PER_SECOND:Number = 5;
		public static const ENERGY_COST_PER_CLICK:Number = 0.5;
		public static const CRITICAL_DAMAGE_MULTIPLIER:Number = 0.5;
		public static const STACK_DURATION:Number = 60 * 1000;

		public function buffFunction(buff:Buff):void
		{
			buff.iconId = 201;
			buff.buffStat(CH2.STAT_CRIT_DAMAGE, 1 + CRITICAL_DAMAGE_MULTIPLIER);

			buff.finishFunction = after(buff.finishFunction, function():void
			{
				buff.buffStat(CH2.STAT_CRIT_DAMAGE, 1 + (CRITICAL_DAMAGE_MULTIPLIER * buff.stacks));
			});

			buff.tickFunction = after(buff.tickFunction, function():void
			{
				currentCharacter.clickAttack(false);
			});
		}

		public function buffTooltipFunction(buff:Buff, stackedClicksPerSecond:Number, stackedEnergyCostPerSecond:Number):Object
		{
			var stackedCriticalDamageMultiplier:Number = CRITICAL_DAMAGE_MULTIPLIER * buff.stacks;
			return createTooltip(_('Critstorm'), _('Performing { 0 | label:clicksPerSecond } until you run out of energy. Crit damage increased by { 2 | percent | color:value }. Consuming { 1 | label:energyPerSecond } while clicking.', stackedClicksPerSecond, stackedEnergyCostPerSecond, stackedCriticalDamageMultiplier));
		}

		public function skillTooltipFunction(skill:Skill, currentClicksPerSecond:Number, currentStackDuration:Number):Object
		{
			return createTooltip(_('Critstorm'), _('Performs { 0 | label:clicksPerSecond } until you run out of energy. Increases crit damage by { 2 | percent | color:value }. Speed and crit damage increase every { 1 | duration }.', currentClicksPerSecond, currentStackDuration, CRITICAL_DAMAGE_MULTIPLIER));
		}

		public function onCreateSkills():void
		{
			var stormOverride:Object = {};
			stormOverride['name'] = 'Critstorm';
			stormOverride['baseTicksPerSecond'] = CLICKS_PER_SECOND;
			stormOverride['baseEnergyCostPerTick'] = ENERGY_COST_PER_CLICK;
			stormOverride['baseStackDuration'] = STACK_DURATION;
			stormOverride['buffFunction'] = buffFunction;
			stormOverride['buffTooltipFunction'] = buffTooltipFunction;
			stormOverride['skillTooltipFunction'] = skillTooltipFunction;
			extensions.storms.overrideStorm(stormOverride);
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var nodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['Q65'];
			nodeType['tooltip'] = _('Performs { 0 | label:clicksPerSecond } until you run out of energy. Increases crit damage by { 2 | percent | color:value }. Speed and crit damage increase every { 1 | duration }.', CLICKS_PER_SECOND, STACK_DURATION, CRITICAL_DAMAGE_MULTIPLIER);

			extensions.automator.enhanceBuffGem(helpfulAdventurerTemplate, 'A14', 'Helpful Adventurer_59', 'Critstorm');
		}
	}
}