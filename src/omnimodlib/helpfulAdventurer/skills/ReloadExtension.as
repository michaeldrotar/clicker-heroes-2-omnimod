package omnimodlib.helpfulAdventurer.skills
{
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;

	public class ReloadExtension extends Extension
	{
		public static const RELOAD_PERCENT:Number = 0.4;

		public function getCurrentReloadPercent():Number
		{
			var improvedReloads:Number = currentCharacter.getTrait('ImprovedReload');
			var hasSmallReloads:Number = currentCharacter.getTrait('SmallReloads');

			return (RELOAD_PERCENT + (improvedReloads * 0.2)) * (hasSmallReloads ? 0.2 : 1)
		}

		public function onCreateSkills():void
		{
			var reload:Skill = extensions.skills.getStaticSkill('Reload');

			reload.tooltipFunction = function():Object
			{
				var header:String = _('Reload');
				var body:String = _('Restores energy and mana and reduces the remaining cooldowns of all skills by { 0 | percent | color:value }.', getCurrentReloadPercent());
				return createTooltip(header, body + extensions.skills.getStandardSkillTooltipSuffix(this));
			};
		}

		public function onHelpfulAdventurerTemplateCreated(helpfulAdventurerTemplate:Character):void
		{
			var reloadNodeType:Object = helpfulAdventurerTemplate.levelGraphNodeTypes['T8'];
			reloadNodeType['tooltip'] = _('Restores energy and mana and reduces the remaining cooldowns of all skills by { 0 | percent | color:value }.', RELOAD_PERCENT);
		}
	}
}