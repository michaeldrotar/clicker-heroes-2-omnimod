package omnimodlib.helpfulAdventurer
{
	import heroclickerlib.CH2;
	import models.Buff;
	import models.Character;
	import models.Monster;
	import models.Skill;
	import omnimodlib.Extension;

	public class StormsExtension extends Extension
	{
		public function overrideStorm(stormOverride:Object):void
		{
			var skill:Skill = extensions.skills.getStaticSkill(stormOverride['name']);
			skill.effectFunction = function():void
			{
				var buffTicksPerSecond:Number = _getCurrentTicksPerSecond(stormOverride);
				var buffEnergyCostPerTick:Number = _getCurrentEnergyCostPerTick(stormOverride);
				var buffManaCostPerTick:Number = _getCurrentManaCostPerTick(stormOverride);
				var buffStackDuration:Number = _getCurrentStackDuration(stormOverride);

				var buff:Buff = extensions.buffs.getCurrentBuff(stormOverride['name']);
				if (!buff)
				{
					buff = new Buff();
					buff.name = stormOverride['name'];
					buff.unhastened = true;
					currentCharacter.buffs.addBuff(buff);
				}
				else
				{
					buff.refreshTimer();
				}
				buff.stacks = 1;
				buff.duration = buffStackDuration;
				buff.tickRate = 1000 / buffTicksPerSecond;

				buff.tickFunction = function():void
				{
					var isFinished:Boolean = false;

					if (currentCharacter.isPaused)
					{
						return;
					}

					if (stormOverride['baseManaCostPerTick'])
					{
						if (!currentCharacter.isNextMonsterInRange)
						{
							return;
						}

						extensions.mana.addMana(-1 * buffManaCostPerTick, false);
						isFinished = currentCharacter.mana <= 0;
					}

					if (stormOverride['baseEnergyCostPerTick'])
					{
						var closestMonster:Monster;
						var canClick:Boolean = false;

						if (currentCharacter.state === Character.STATE_COMBAT)
						{
							canClick = true;
						}
						else if (currentCharacter.isInTeleportClickAttackState)
						{
							closestMonster = CH2.world.getNextMonster();
							if (!currentCharacter.isNextMonsterInRange && closestMonster && (!closestMonster.isBoss || CH2.world.bossEncounter.isWithinAttackRange))
							{
								canClick = true;
							}
						}

						if (!canClick)
						{
							return;
						}

						extensions.energy.addEnergy(-1 * buffEnergyCostPerTick, false);
						isFinished = currentCharacter.energy <= 0;
					}

					if (isFinished)
					{
						buff.isFinished = true;
						buff.onFinish();
					}
				};

				buff.finishFunction = function():void
				{
					if ((stormOverride['baseEnergyCostPerTick'] && currentCharacter.energy <= 0) || (stormOverride['baseManaCostPerTick'] && currentCharacter.mana <= 0))
					{
						// Allow to finish
						return;
					}

					// Increment to next stack
					buff.stacks += 1;
					buff.tickRate = (1000 / buffTicksPerSecond) / buff.stacks;
					buff.timeSinceActivated = 0;
					buff.isFinished = false;
				};

				if (stormOverride['buffFunction'])
				{
					stormOverride['buffFunction'](buff);
				}

				if (stormOverride['buffTooltipFunction'])
				{
					buff.tooltipFunction = function():Object
					{
						var stackedTicksPerSecond:Number = buffTicksPerSecond * buff.stacks;
						var stackedCostPerSecond:Number = stackedTicksPerSecond * (stormOverride['baseEnergyCostPerTick'] ? buffEnergyCostPerTick : buffManaCostPerTick);

						return stormOverride['buffTooltipFunction'](buff, stackedTicksPerSecond, stackedCostPerSecond);
					}
				}
			};

			if (stormOverride['skillTooltipFunction'])
			{
				skill.tooltipFunction = function():Object
				{
					var buffTicksPerSecond:Number = _getCurrentTicksPerSecond(stormOverride);
					var buffStackDuration:Number = _getCurrentStackDuration(stormOverride);

					var buffEnergyCostPerTick:Number = _getCurrentEnergyCostPerTick(stormOverride);
					var buffManaCostPerTick:Number = _getCurrentManaCostPerTick(stormOverride);
					var currentCooldown:Number = extensions.skills.getCurrentCooldown(this);

					var tooltip:Object = stormOverride['skillTooltipFunction'](skill, buffTicksPerSecond, buffStackDuration);

					tooltip['body'] += '\n\n';
					if (stormOverride['baseEnergyCostPerTick'] > 0)
					{
						tooltip['body'] += extensions.skills.getStandardResourceDescription(buffEnergyCostPerTick, 'energyPerClick') + '\n';
					}
					else if (stormOverride['baseManaCostPerTick'] > 0)
					{
						tooltip['body'] += extensions.skills.getStandardResourceDescription(buffManaCostPerTick, 'manaPerAutoAttack') + '\n';
					}
					tooltip['body'] += extensions.skills.getStandardCooldownDescription(currentCooldown);

					return tooltip;
				};
			}
		}

		private function _getCurrentTicksPerSecond(stormOverride:Object):Number
		{
			return stormOverride['baseTicksPerSecond'] * currentCharacter.hasteRating.numberValue();
		}

		private function _getCurrentEnergyCostPerTick(stormOverride:Object):Number
		{
			return (stormOverride['baseEnergyCostPerTick'] || 0) * (1 - currentCharacter.energyCostReduction);
		}

		private function _getCurrentManaCostPerTick(stormOverride:Object):Number
		{
			return (stormOverride['baseManaCostPerTick'] || 0);
		}

		private function _getCurrentStackDuration(stormOverride:Object):Number
		{
			return stormOverride['baseStackDuration'] / currentCharacter.hasteRating.numberValue();
		}
	}
}
