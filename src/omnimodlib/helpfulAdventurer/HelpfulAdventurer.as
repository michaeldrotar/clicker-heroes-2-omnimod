package omnimodlib.helpfulAdventurer
{
	import omnimodlib.CoreCharacterTemplateExtension;

	public class HelpfulAdventurer extends CoreCharacterTemplateExtension
	{
		public function HelpfulAdventurer()
		{
			super();

			templateName = 'Helpful Adventurer';
		}
	}
}