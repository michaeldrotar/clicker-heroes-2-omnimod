package omnimodlib
{
	import omnimodlib.Extension;

	public class Migrations extends Extension
	{
		public function Migrations()
		{
			super();
		}

		public function onCharacterCreated():void
		{
			var characterVersion:Number = data.getNumber('CHARACTER_VERSION') || modVersion;

			runAlwaysChecks();

			if (characterVersion <= 1)
			{
				// Placeholder for changes for version 2...
			}

			data.setNumber('CHARACTER_VERSION', modVersion);
		}

		public function runAlwaysChecks():void
		{
			data.removeKey('OMNIMOD_RUBY_SHOP_COOLDOWN');
			data.removeKey('OMNIMOD_RUBY_SHOP_DURATION');

			data.removeKey('OMNIMOD_AUTOMATOR_POINT_PURCHASE_TIME_SINCE_LAST_PURCHASE');
			data.removeKey('OMNIMOD_SKILL_POINT_PURCHASE_TIME_SINCE_LAST_PURCHASE');

			if (data.getBoolean('OMNIMOD_GILDING_GILDING_ENABLED'))
			{
				currentCharacter.highestWorldCompleted += 1;
			}
			data.removeKey('OMNIMOD_GILDING_GILDING_ENABLED');
		}
	}
}