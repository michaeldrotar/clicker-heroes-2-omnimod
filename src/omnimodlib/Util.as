package omnimodlib
{
	import com.playsaurus.model.Model;
	import com.playsaurus.numbers.BigNumber;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	import heroclickerlib.CH2;
	import heroclickerlib.managers.Trace;
	import models.Character;
	import models.Monster;
	import omnimodlib.managers.OmniSaveManager;
	import omnimodlib.managers.OmniTemplateManager;

	// Wish I had a better way to do this, import Trace doesn't work great.. might as well add other misc things for now.
	public class Util
	{
		public static const MELEE_RANGE:Number = 90;
		public static const MONSTER_POSITION_DISTANCE:Number = 176;
		public static const YARDS_DISTANCE:Number = 30;

		public static function assign(... objects):Object
		{
			var newObject:Object = {};
			forEach(objects, function(object:Object, ... _):void
			{
				forEach(keys(object), function(key:String, ... _):void
				{
					newObject[key] = object[key];
				});
			});
			return newObject;
		}

		public static function bind(originalFunction:Function, thisObject:*, ... boundArgs):Function
		{
			var boundFunction:Function = function(... args):*
			{
				var fullArgs:Array = boundArgs.concat(args);
				return originalFunction.apply(thisObject, fullArgs);
			};
			return boundFunction;
		}

		public static function defaults(... objects):Object
		{
			var swapped:Array = reverse(objects);
			return assign.apply(Util, swapped);
		}

		public static function falseFn(... args):Boolean
		{
			return false;
		}

		public static function find(array:Array, callback:Function, thisObject:* = null):*
		{
			for (var index:uint = 0; index < array.length; index++)
			{
				var value:* = array[index];
				var result:* = callback.call(thisObject, value, index, array);
				if (result)
				{
					return value;
				}
			}
			return undefined;
		}

		public static function filter(array:Array, callback:Function, thisObject:* = null):Array
		{
			var newArray:Array = [];
			array.forEach(function(value:*, index:uint, object:*):void
			{
				var result:* = callback.call(thisObject, value, index, object);
				if (result)
				{
					newArray.push(value);
				}
			});
			return newArray;
		}

		public static function forEach(array:Array, callback:Function):void
		{
			array.forEach(callback);
		}

		public static function getClass(value:*):Class
		{
			return Class(getDefinitionByName(getQualifiedClassName(value)));
		}

		public static function getClassFromName(name:String):Class
		{
			return Class(getDefinitionByName(name));
		}

		public static function getClassName(value:*):String
		{
			return getQualifiedClassName(value);
		}

		public static function getSuperClass(value:*):Class
		{
			return Class(getDefinitionByName(getQualifiedSuperclassName(value)));
		}

		public static function getSuperClassName(value:*):String
		{
			return getQualifiedSuperclassName(value);
		}

		public static function map(array:Array, callback:Function, thisObject:* = null):Array
		{
			var newArray:Array = [];
			array.forEach(function(value:*, index:uint, object:*):void
			{
				var newValue:* = callback.call(thisObject, value, index, object);
				newArray.push(newValue);
			});
			return newArray;
		}

		public static function mergeObjects(... args):*
		{
			var target:Object = {};

			var key:String;
			var source:Object;

			var index:uint = 0;
			var length:uint = args.length;
			for (; index < length; index++)
			{
				source = args[index];

				for (key in source)
				{
					target[key] = source[key];
				}
			}

			return target;
		}

		public static function reverse(array:Array):Array
		{
			var newArray:Array = [];
			forEach(array, function(value:*, ... _):void
			{
				newArray.unshift(value);
			});
			return newArray;
		}

		public static function trueFn(... args):Boolean
		{
			return true;
		}

		public static function keys(object:Object):Array
		{
			var keys:Array = [];
			var key:String;
			for (key in object)
			{
				keys.push(key);
			}
			return keys;
		}

		public static function monstersInRange(character:Character, minRange:Number, maxRange:Number):Array
		{
			var monster:Monster = null;
			var monsters:Array = [];
			for (var i:int = 0; i < CH2.world.monsters.monsters.length; i++)
			{
				monster = CH2.world.monsters.monsters[i];
				if (monster.isAlive && monster.y >= character.y + minRange && monster.y <= character.y + maxRange)
				{
					monsters.push(monster);
				}
				else if (monsters.length > 0)
				{
					break;
				}
			}
			return monsters;
		}

		public static function noop(... args):*
		{

		}

		public static function returnFirstArg(first:*, ... args):*
		{
			return first;
		}

		public static function toId(value:*):String
		{
			var text:String = '';
			if (value is Array)
			{
				text = (value as Array).join('_');
			}
			else
			{
				text = value.toString();
			}
			return underscore(text).toUpperCase();
		}

		public static function toArray(value:*):Array
		{
			if (value is Array)
			{
				return value;
			}

			var array:Array = [];
			if ('forEach' in value)
			{
				value.forEach(function(item:*, ... args):void
				{
					array.push(item);
				});
				return array;
			}

			throw 'Unable to convert ' + typeof(value) + ' to array';
		}

		public static function trace(... args):void
		{
			var format:Function;
			var formatChildren:Function;

			format = function(value:*):String
			{
				if (value is Array)
				{
					return '[object Array length:' + (value as Array).length + ']';
				}
				if (value is String)
				{
					return '"' + (value as String) + '"';
				}
				if (value is BigNumber)
				{
					const base:String = (value as BigNumber).base.toFixed(2).replace('.00', '');
					const power:String = (value as BigNumber).power.toFixed(2).replace('.00', '');
					return base + (power !== '0' ? 'e' + power : '');
				}
				if (value is Object)
				{
					return value.toString();
				}
				return String(value);
			}

			formatChildren = function(value:*, indentSize:uint = 2, depth:int = 3):String
			{
				if (depth <= 0)
				{
					return '';
				}

				var key:String;
				var indent:String = (new Array(indentSize)).join(' ');
				var output:String = '';
				if (value is Array)
				{
					for each (var item:* in value)
					{
						output += '\n' + indent + format(item);
						output += formatChildren(item, indentSize + 2, depth - 1);
					}
				}
				else if (value is Model)
				{
					var modelDefinition:Object = (value as Model).getModelDefinition();
					var dynamicFields:Array = Util.keys(modelDefinition['dynamicFields']);
					var staticFields:Array = Util.keys(modelDefinition['staticFields']);
					var keys:Array = Util.uniq([].concat(dynamicFields, staticFields)).sort();
					for each (key in keys)
					{
						output += '\n' + indent + format(key) + ': ' + format(value[key]);
						output += formatChildren(value[key], indentSize + 2, depth - 1);
					}
				}
				else if (value is Object)
				{
					var klass:Class = getClass(value);
					var templateRegistration:Object = OmniTemplateManager.instance.getClassRegistration(klass);
					var saveRegistration:Object = OmniSaveManager.instance.getClassRegistration(klass);
					if (templateRegistration || saveRegistration)
					{
						var properties:Array = [];
						if (templateRegistration)
						{
							properties = properties.concat(templateRegistration['properties'], templateRegistration['idPropertyName']);
						}
						if (saveRegistration)
						{
							properties = properties.concat(saveRegistration['properties']);
						}
						properties = Util.uniq(properties).sort();
						for each (key in properties)
						{
							output += '\n' + indent + format(key) + ': ' + format(value[key]);
							output += formatChildren(value[key], indentSize + 2, depth - 1);
						}

					}
					else
					{
						for (key in value)
						{
							output += '\n' + indent + format(key) + ': ' + format(value[key]);
							output += formatChildren(value[key], indentSize + 2, depth - 1);
						}
					}
				}
				return output;
			};

			var arg:*;
			for (var i:int = 0; i < args.length; i++)
			{
				arg = args[i];
				args[i] = format(arg);
				args[i] += formatChildren(arg);
			}
			Trace.apply(null, args);
		}

		public static function traceLine():void
		{
			Trace.call(null, '--------------------------------------------------------');
		}

		public static function underscore(value:String):String
		{
			return value.replace(/([a-z0-9])([A-Z])/g, '$1_$2').replace(/[^a-z0-9]+/gi, '_').replace(/(^_+|_+$)/g, '').toLowerCase();
		}

		public static function uniq(arr:Array):Array
		{
			var uniqArray:Array = [];
			arr.forEach(function(item:*, ... args):void
			{
				if (uniqArray.indexOf(item) === -1)
				{
					uniqArray.push(item);
				}
			});
			return uniqArray;
		}

		public static function weightedChoice(choices:Object):String
		{
			var keys:Array = [];
			var key:String = null;
			var sum:uint = 0;
			for (key in choices)
			{
				sum = sum + choices[key];
				keys.push(key);
			}
			keys.sort(Array.CASEINSENSITIVE);

			var choice:uint = CH2.roller.modRoller.integer(0, sum - 1);
			sum = 0;
			for each (key in keys)
			{
				sum = sum + choices[key];
				if (choice < sum)
				{
					return key;
				}
			}

			return null;
		}
	}
}