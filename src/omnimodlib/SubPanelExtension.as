package omnimodlib
{
	import flash.display.MovieClip;
	import ui.CH2UI;
	import ui.elements.SubPanel;

	public class SubPanelExtension extends Extension
	{
		public var label:String;
		public var subPanel:SubPanel;
		public var iconId:int;
		public var isVisible:Function;
		public var isGlowing:Function;
		public var onClick:Function;

		public function SubPanelExtension()
		{
			super();
			isVisible = function():Boolean
			{
				return true;
			};
			isGlowing = function():Boolean
			{
				return false;
			};
			onClick = Util.noop;
		}

		public function onUICreated():void
		{
			subPanel.setDisplay(new MovieClip());

			var position:int = CH2UI.instance.mainUI.mainPanel.tabs.length;
			CH2UI.instance.mainUI.mainPanel.registerTab(position, label, subPanel, iconId, isVisible, isGlowing, onClick);
		}
	}
}
