package omnimodlib.ui
{
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import omnimodlib.ui.subPanels.OmniAutomatorSubPanel;
	import ui.CH2UI;

	public class OmniAutomatorSubPanelExtension extends Extension
	{
		public var subPanel:OmniAutomatorSubPanel;

		public function OmniAutomatorSubPanelExtension()
		{
			super();
		}

		public function registerTab(options:Object = null):void
		{
			options = Util.defaults(options, {label: 'Automator', iconId: 5, position: 3, isVisible: Util.trueFn, isGlowing: Util.falseFn, onClick: Util.noop});

			subPanel = new OmniAutomatorSubPanel();
			CH2UI.instance.mainUI.mainPanel.registerTab(options.position, options.label, subPanel, options.iconId, options.isVisible, options.isGlowing, options.onClick);
		}
	}
}