package omnimodlib.ui
{
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import omnimodlib.ui.subPanels.OmniSkillsSubPanel;
	import ui.CH2UI;

	public class OmniSkillsSubPanelExtension extends Extension
	{
		public var subPanel:OmniSkillsSubPanel

		public function registerTab(options:Object = null):void
		{
			options = Util.defaults(options, {label: 'Skills', iconId: 3, position: 2, isVisible: Util.trueFn, isGlowing: Util.falseFn, onClick: Util.noop});

			subPanel = new OmniSkillsSubPanel();
			CH2UI.instance.mainUI.mainPanel.registerTab(options.position, options.label, subPanel, options.iconId, options.isVisible, options.isGlowing, options.onClick);
		}
	}
}
