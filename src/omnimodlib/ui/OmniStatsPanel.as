package omnimodlib.ui
{
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import omnimodlib.ui.OmniStatsPanel;
	import ui.CH2UI;

	public class OmniStatsPanel extends Extension
	{
		public var statsPanel:StatsPanel;

		public var statLevels:Array;

		private var _defaults:Object;
		private var _getStatsFunction:Function;
		private var _getStatPointChoicesFunction:Function;

		public function OmniStatsPanel()
		{
			super();

			this._defaults = {};
			this._defaults['label'] = _('Stat Levels');
			this._defaults['iconId'] = 2;
			this._defaults['shouldShowTab'] = this.shouldShowTab;
			this._defaults['shouldShowGlow'] = this.shouldShowGlow;
			this._defaults['onClick'] = this.onClick;
		}

		public function clearStatPanelChoices():void
		{
			this.data.removeKey('STAT_PANEL_CHOICES');
		}

		public function getStatPanelChoices():Array
		{
			var statIdChoices:Array = this.data.getArray('STAT_PANEL_CHOICES') || [];
			if (statIdChoices.length)
			{
				return statIdChoices;
			}

			var index:uint;
			var length:uint;
			var choice:Object;
			var statId:String;

			var weightedChoices:Object = {};
			for (index = 0, length = this.statLevels.length; index < length; index++)
			{
				choice = this.statLevels[index];
				if (choice.weight)
				{
					weightedChoices[choice['id'].toString()] = choice['weight'];
				}
			}

			var choiceCounts:Object = this.data.getObject('STAT_PANEL_CHOICE_COUNTS') || {};
			var timesOffered:uint = this.extensions.omniStatPoints.spentStatPoints;

			var reweightedChoices:Object = {};
			for (statId in weightedChoices)
			{
				choiceCounts[statId] = choiceCounts[statId] || 0;
				reweightedChoices[statId] = weightedChoices[statId] * ((timesOffered * 3) + 1) / ((choiceCounts[statId] * 3) + 1);
			}

			for (index = 0; index < 3; index++)
			{
				statId = Util.weightedChoice(reweightedChoices);
				choiceCounts[statId] = choiceCounts[statId] + 1;
				delete reweightedChoices[statId];
				statIdChoices.push(parseInt(statId));
			}

			this.data.setArray('STAT_PANEL_CHOICES', statIdChoices);
			this.data.setObject('STAT_PANEL_CHOICE_COUNTS', choiceCounts);
			return statIdChoices;
		}

		public function levelUpStatChoice(statId:uint):void
		{
			if (this.extensions.omniStatPoints.availableStatPoints > 0)
			{
				currentCharacter.levelUpStat(statId);
				clearStatPanelChoices();
				this.extensions.omniStatPoints.spentStatPoints = this.extensions.omniStatPoints.spentStatPoints + 1;
			}
		}

		public function register(options:Object):void
		{
			options = Util.mergeObjects(this._defaults, options);

			this.statsPanel = new StatsPanel(this);
			this._getStatsFunction = options['getStatsFunction'];
			this._getStatPointChoicesFunction = options['getStatPointChoicesFunction'];
			this.statLevels = options['statLevels'];
			CH2UI.instance.mainUI.mainPanel.unregisterTab(options['position']);
			CH2UI.instance.mainUI.mainPanel.registerTab(options['position'], options['label'], this.statsPanel, options['iconId'], options['shouldShowTab'], options['shouldShowGlow'], options['onClick']);
		}

		public function onClick():void
		{

		}

		public function shouldShowTab():Boolean
		{
			return this.currentCharacter.level > 1;
		}

		public function shouldShowGlow():Boolean
		{
			return this.extensions.omniStatPoints.availableStatPoints > 0;
		}

		public function unregister():void
		{
			this.statsPanel = null;
		}

		public function onUICreated():void
		{
			unregister();
		}
	}
}

import flash.display.MovieClip;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import heroclickerlib.CH2;
import heroclickerlib.scrollers.SimpleScroller;
import lib.managers.TextManager;
import models.Character;
import omnimodlib.OmniStringUtil;
import omnimodlib.Util;
import omnimodlib.core.ui.components.OmniComponent;
import omnimodlib.ui.OmniStatsPanel;
import omnimodlib.ui.OmniSubPanel;
import swc.HUD.SkillsPanelDisplay2;

class StatsPanel extends OmniSubPanel
{
	private const _LINE_DISTANCE:uint = 50;

	private var _statPointHeading:StatHeading;
	private var _statPointTextField:TextField;
	private var _statPointButtons:Vector.<StatPointButton>;
	private var _statEntryHeading:StatHeading;
	private var _statEntries:Vector.<StatEntry>;
	private var _statEntryCounter:uint;
	private var _omniStatsPanel:OmniStatsPanel;
	private var _panelOverlay:SkillsPanelDisplay2;
	private var _scrollbar:SimpleScroller;

	public function StatsPanel(omniStatsPanel:OmniStatsPanel)
	{
		super();

		this._statEntries = new Vector.<StatEntry>();
		this._omniStatsPanel = omniStatsPanel;

		this.setDisplay(new MovieClip());

		this._panelOverlay = new SkillsPanelDisplay2();
		this._panelOverlay.mouseChildren = false;
		this._panelOverlay.mouseEnabled = false;
		this.display.addChild(this._panelOverlay);

		this._scrollbar = new SimpleScroller(CH2.game.stage);
		this._scrollbar.offsetScrollY = 0;
		this._scrollbar.initWithOwnSetup(this._panelOverlay.entryHolder, this._panelOverlay.panelMask, this._panelOverlay.scrollbar);
		this._scrollbar.mouseDownSpeed = 10;
		this._scrollbar.registerAreaBGClip(this.display);
		this._panelOverlay.entryHolder.mask = this._panelOverlay.panelMask;

		this._statPointHeading = new StatHeading();
		this._statPointHeading.headerText = _('Stat Points');
		this._statPointHeading.x = 10;
		this._statPointHeading.y = 0;
		this._panelOverlay.entryHolder.addChild(this._statPointHeading);
		this._statPointHeading.redraw();

		this._statPointTextField = new TextField();
		this._statPointTextField.y = 0;
		this._statPointTextField.textColor = this.colors.yellow;
		this._statPointTextField.autoSize = TextFieldAutoSize.RIGHT;
		this._panelOverlay.entryHolder.addChild(this._statPointTextField);

		this._statPointButtons = new Vector.<StatPointButton>;
		var statPointButton:StatPointButton;
		var statPointButtonIndex:uint = 0;
		var omniStatsPanel:OmniStatsPanel = this._omniStatsPanel
		for (; statPointButtonIndex < 3; statPointButtonIndex++)
		{
			statPointButton = new StatPointButton();
			statPointButton.x = 20 + (165 * statPointButtonIndex);
			statPointButton.y = 50;
			statPointButton.onClick = function(event:Event):void
			{
				Util.traceLine();
				Util.trace(event.target);
				Util.trace((event.target as StatPointButton).statId);
				omniStatsPanel.levelUpStatChoice((event.target as StatPointButton).statId);
			}

			this._statPointButtons.push(statPointButton);
			this._panelOverlay.entryHolder.addChild(statPointButton);
		}

		this._statEntryHeading = new StatHeading();
		this._statEntryHeading.headerText = _('Stat Levels');
		this._statEntryHeading.x = 10;
		this._statEntryHeading.y = 160;
		this._panelOverlay.entryHolder.addChild(this._statEntryHeading);
		this._statEntryHeading.redraw();
	}

	override public function activate():void
	{
		super.activate();
	}

	override public function dispose():void
	{
		super.dispose();
		this._scrollbar.dispose();
	}

	override protected function setup():void
	{
		super.setup();
	}

	override public function update(dt:Number):void
	{
		super.update(dt);
		//if (this._omniStatsPanel._getStatsFunction) {
		//this._omniStatsPanel._getStatsFunction(this.addHeader, this.addStat);
		//}

		TextManager.setText(this._statPointTextField, OmniStringUtil.instance.formatNumber(this._omniStatsPanel.extensions.omniStatPoints.availableStatPoints), 20);
		this._statPointTextField.x = 510 - this._statPointTextField.width;

		var statPointButton:StatPointButton;
		var statPointButtonIndex:uint = 0;
		var statChoices:Array = this._omniStatsPanel.getStatPanelChoices();
		for (; statPointButtonIndex < this._statPointButtons.length; statPointButtonIndex++)
		{
			statPointButton = this._statPointButtons[statPointButtonIndex];
			statPointButton.label = CH2.STATS[statChoices[statPointButtonIndex]]['displayName'];
			statPointButton.statId = statChoices[statPointButtonIndex];
			statPointButton.enabled = this._omniStatsPanel.extensions.omniStatPoints.availableStatPoints > 0;
			statPointButton.redraw();
		}

		this._resetStatLineCounter();

		var statLevels:Array = this._omniStatsPanel.statLevels;
		var statId:uint;
		var statLevelIndex:uint;
		var statLevelLength:uint = statLevels.length

		var maxStatLevel:Number = 0;
		for (statLevelIndex = 0; statLevelIndex < statLevelLength; statLevelIndex++)
		{
			statId = statLevels[statLevelIndex]['id'];
			maxStatLevel = Math.max(this._omniStatsPanel.currentCharacter.statLevels[statId] || 0, maxStatLevel);
		}
		for (statLevelIndex = 0; statLevelIndex < statLevelLength; statLevelIndex++)
		{
			statId = statLevels[statLevelIndex]['id'];
			addStatEntry(statId, maxStatLevel);
		}

		this._scrollbar.updateScrollableRange();
	}

	public function addHeader(text:String):void
	{
		var statLine:StatEntry = _getNextStatEntry();
		statLine.headerText = text;
		statLine.redraw();
	}

	public function addStatEntry(statId:uint, maxStatLevel:Number = 0):void
	{
		var currentCharacter:Character = this._omniStatsPanel.currentCharacter;
		var statLevel:Number = currentCharacter.statLevels[statId] || 0;

		var formatterFunction:Function;
		var statDetails:Object = CH2.STATS[statId];
		if (statDetails['formattingFunction'] === CH2.percentFormat)
		{
			formatterFunction = OmniStringUtil.instance.formatPercent;
		}
		else
		{
			formatterFunction = OmniStringUtil.instance.formatNumber;
		}

		var statLine:StatEntry = _getNextStatEntry();
		statLine.statNameText = _(statDetails['displayName']);
		statLine.statValueText = formatterFunction(this.getStatBonus(statId));
		statLine.statLevelText = OmniStringUtil.instance.formatNumber(statLevel);
		statLine.statPercent = maxStatLevel ? statLevel / maxStatLevel : 0;
		statLine.redraw();
	}

	public function getStatBonus(statId:uint):Number
	{
		var classStat:Number = this._omniStatsPanel.currentCharacter.getClassStat(statId).numberValue();
		var baseStat:Number = this._omniStatsPanel.currentCharacter.statBaseValues[statId];

		return classStat - baseStat; // works for add and multiply stats
	}

	private function _resetStatLineCounter():void
	{
		this._statEntryCounter = 0;
	}

	private function _getNextStatEntry():StatEntry
	{
		var statLine:StatEntry;
		if (this._statEntryCounter < this._statEntries.length)
		{
			statLine = this._statEntries[this._statEntryCounter];
		}
		else
		{
			var previousLine:StatEntry;
			if (this._statEntryCounter > 0)
			{
				previousLine = this._statEntries[this._statEntryCounter - 1];
			}

			statLine = new StatEntry();
			statLine.x = 10;
			statLine.y = previousLine ? previousLine.y + previousLine.height + 4 : 200;

			this._statEntries.push(statLine);
			this._panelOverlay.entryHolder.addChild(statLine);
		}
		this._statEntryCounter = this._statEntryCounter + 1;
		return statLine;
	}
}

class StatHeading extends OmniComponent
{
	public var headerText:String;

	private var _headerTextField:TextField;

	public function StatHeading()
	{
		super();

		this._headerTextField = new TextField();
		this._headerTextField.autoSize = TextFieldAutoSize.LEFT;
		this._headerTextField.textColor = this.colors.yellow;
		this._headerTextField.x = 0;
		this._headerTextField.y = 0;
		this.addChild(this._headerTextField);
	}

	override public function redraw():void
	{
		super.redraw();

		this.graphics.clear();

		TextManager.setText(this._headerTextField, this.headerText.toUpperCase(), 20);
		this.graphics.beginFill(this._headerTextField.textColor);
		this.graphics.drawRect(0, this.height - 1, 510, 1);
	}
}

class StatPointButton extends OmniComponent
{
	public var label:String;
	public var statId:uint;

	private var _labelTextField:TextField;

	public function StatPointButton()
	{
		super();

		this._labelTextField = new TextField();
		this._labelTextField.autoSize = TextFieldAutoSize.CENTER;
		this._labelTextField.wordWrap = true;
		this.addChild(this._labelTextField);

		enableMouse(true);
	}

	override public function redraw():void
	{
		super.redraw();

		this.graphics.clear();

		TextManager.setText(_labelTextField, label.toUpperCase(), 14);
		_labelTextField.textColor = enabled ? this.colors.white : this.colors.disabledText;

		this.graphics.beginFill(enabled ? (_mouseOver ? this.colors.blue : this.colors.yellow) : this.colors.disabledBorder, 0.8);
		this.graphics.drawRoundRect(0, 0, 155, 50, 15, 15);

		this.graphics.beginFill(enabled ? (_mouseDown ? this.colors.light : this.colors.dark) : this.colors.disabledBackground, 0.5);
		this.graphics.drawRoundRect(3, 3, this.width - 6, this.height - 6, 10, 10);

		this._labelTextField.x = (this.width - this._labelTextField.width) / 2;
		this._labelTextField.y = (this.height - this._labelTextField.height) / 2;
	}
}

class StatEntry extends OmniComponent
{
	public var statsPanel:StatsPanel;

	public var headerText:String;
	public var statNameText:String;
	public var statValueText:String;
	public var statLevelText:String;
	public var statPercent:Number;

	private var _headerTextField:TextField;
	private var _statNameTextField:TextField;
	private var _statValueTextField:TextField;
	private var _statLevelTextField:TextField;

	public function StatEntry()
	{
		super();

		this._statLevelTextField = new TextField();
		this._statLevelTextField.autoSize = TextFieldAutoSize.RIGHT;
		this._statLevelTextField.textColor = this.colors.yellow;
		this._statLevelTextField.y = 2;
		this.addChild(this._statLevelTextField);

		this._statNameTextField = new TextField();
		this._statNameTextField.autoSize = TextFieldAutoSize.LEFT;
		this._statNameTextField.textColor = this.colors.lightest;
		this._statNameTextField.x = 50;
		this._statNameTextField.y = 2;
		this.addChild(this._statNameTextField);

		this._statValueTextField = new TextField();
		this._statValueTextField.autoSize = TextFieldAutoSize.RIGHT;
		this._statValueTextField.textColor = this.colors.yellow;
		this._statValueTextField.y = 2;
		this.addChild(this._statValueTextField);
	}

	override public function redraw():void
	{
		super.redraw();

		this.graphics.clear();

		this.graphics.beginFill(this.colors.darkest, 0.5);
		this.graphics.drawRect(0, 0, 510, this._statNameTextField.height + 4);

		this.graphics.beginFill(this.colors.darkest, 1);
		this.graphics.drawRect(0, 0, 510 * this.statPercent, this._statNameTextField.height + 4);

		TextManager.setText(this._statLevelTextField, this.statLevelText, 14);
		TextManager.setText(this._statNameTextField, this.statNameText, 14);
		TextManager.setText(this._statValueTextField, this.statValueText, 14);

		this._statLevelTextField.x = 40 - this._statLevelTextField.width;
		this._statValueTextField.x = 500 - this._statValueTextField.width;
	}
}