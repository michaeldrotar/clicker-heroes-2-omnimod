package omnimodlib.ui
{
	import OmnimodMain;
	import flash.display.MovieClip;
	import omnimodlib.OmniColorPalette;
	import omnimodlib.Util;
	import ui.elements.SubPanel;

	public class OmniSubPanel extends SubPanel
	{
		public function OmniSubPanel()
		{
			super();
			setDisplay(new MovieClip());
		}

		protected function get colors():OmniColorPalette
		{
			return OmniColorPalette.instance;
		}

		public function _(text:*, ... args):String
		{
			if (args.length === 0)
			{
				// If no args given, use its own properties by their names
				args = [this];
			}
			return OmnimodMain.instance._.apply(OmnimodMain.instance, [].concat(text, args));
		}

		public function trace(... args):void
		{
			Util.trace.apply(Util, args);
		}

	}
}