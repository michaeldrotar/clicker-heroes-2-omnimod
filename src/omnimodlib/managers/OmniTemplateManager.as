package omnimodlib.managers
{
	import omnimodlib.Util;

	/**
	 * Handles registering classes which have templates and their template models.
	 * Provides methods for using the template models.
	 */
	public class OmniTemplateManager
	{
		private static var _instance:OmniTemplateManager

		private var _registeredClasses:Object = {};
		private var _registeredModels:Object = {};

		public function OmniTemplateManager()
		{
			super();
		}

		public static function get instance():OmniTemplateManager
		{
			_instance ||= new OmniTemplateManager();
			return _instance;
		}

		public function applyModel(parentClass:Class, id:String, model:*):*
		{
			var key:String = _getClassKey(parentClass);
			var registeredClass:Object = _registeredClasses[key];
			if (!registeredClass)
			{
				throw new Error('Must call OmniTemplateManager#registerClass on `' + String(parentClass) + '` before attempting to use a model of that class.');
			}

			var uuid:String = _getModelKey(parentClass, id);
			if (!_registeredModels[uuid])
			{
				throw new Error('Must call OmniTemplateManager#registerModel on a `' + String(parentClass) + '` model with id `' + id + '` before attempting to use that model.');
			}

			var template:Object = _registeredModels[uuid];
			model[registeredClass['idPropertyName']] = id;
			registeredClass['properties'].forEach(function(property:String, ... args):void
			{
				// TODO: Create shallow copies of arrays and objects
				model[property] = template[property];
			});
			return model;
		}

		public function createModel(parentClass:Class, id:String):*
		{
			var model:* = new parentClass();
			return applyModel(parentClass, id, model);
		}

		public function getClassRegistration(parentClass:Class):Object
		{
			var key:String = _getClassKey(parentClass);
			var registration:Object = _registeredClasses[key];
			if (!registration)
			{
				return undefined;
			}
			return {'properties': [].concat(registration['properties']), 'idPropertyName': registration['idPropertyName']}
		}

		public function registerClass(parentClass:Class, properties:Array, idPropertyName:String = 'id'):void
		{
			var key:String = _getClassKey(parentClass);
			if (_registeredClasses[key])
			{
				Util.trace('WARNING: OmniTemplateManager#registerClass called with an already registered class `' + String(parentClass) + '`');
			}

			var registeredClass:Object = {};
			registeredClass['parentClass'] = parentClass;
			registeredClass['properties'] = properties;
			registeredClass['idPropertyName'] = idPropertyName;
			_registeredClasses[key] = registeredClass;
		}

		public function registerModel(model:*):void
		{
			var parentClass:Class = Util.getClass(model);
			var registeredClass:Object = _registeredClasses[_getClassKey(parentClass)];
			if (!registeredClass)
			{
				throw new Error('Must call OmniTemplateManager#registerClass on `' + String(parentClass) + '` before attempting to register a model of that class.');
			}

			var id:String = model[registeredClass['idPropertyName']];
			if (!id)
			{
				throw new Error('OmniTemplateManager#registerModel called for a model of class `' + String(parentClass) + '` without an id');
			}

			var uuid:String = _getModelKey(parentClass, id);
			if (_registeredModels[uuid])
			{
				Util.trace('WARNING: OmniTemplateManager#registerModel called with an already registered `' + String(parentClass) + '` model with id `' + id + '`');
			}

			_registeredModels[uuid] = model;
		}

		private function _getClassKey(parentClass:Class):String
		{
			return Util.getClassName(parentClass);
		}

		private function _getModelKey(parentClass:Class, id:String):String
		{
			return Util.getClassName(parentClass) + '::' + id;
		}
	}
}