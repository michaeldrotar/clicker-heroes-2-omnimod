package omnimodlib.managers
{
	import com.adobe.serialization.json.JSONDecoder;
	import com.adobe.serialization.json.JSONEncoder;
	import omnimodlib.Util;

	/**
	 * This class is responsible for serializing and deserializing data for saves.
	 */
	public class OmniSaveManager
	{
		private static var _instance:OmniSaveManager

		private var _registeredClasses:Object = {};

		public function OmniSaveManager()
		{
			super();
		}

		public static function get instance():OmniSaveManager
		{
			_instance ||= new OmniSaveManager();
			return _instance;
		}

		/**
		 * Deserializes a json string into a hash of its root objects.
		 * @param	json
		 * @return
		 */
		public function fromJsonString(json:String):Object
		{
			return fromJsonObject((new JSONDecoder(json, true)).getValue());
		}

		/**
		 * Deserializes a json object into a hash of its root objects.
		 * @param	jsonObject
		 * @return
		 */
		public function fromJsonObject(jsonObject:Object):Object
		{
			var serializedRoot:Object = jsonObject['root'];
			var serializedData:Array = jsonObject['data'];

			// An initial pass over the serialized data creates all the object references
			// and copies primitives to the `data` collection so that the `data` collection
			// now has everything, regardless of whether its assigning by value or by reference.
			var data:Array = [];
			serializedData.forEach(function(item:*, ... args):void
			{
				var type:String = typeof item;
				if (type === 'object' && item !== null)
				{
					var className:String = Util.getClassName(item);
					if (className === 'Array')
					{
						data.push([]);
					}
					else if (item['__type'])
					{
						var klass:Class = Util.getClassFromName(item['__type']);
						var model:*;
						// Apply the template if the model has one
						var templateRegistration:Object = OmniTemplateManager.instance.getClassRegistration(klass);
						if (templateRegistration)
						{
							var idPropertyName:String = templateRegistration['idPropertyName'];
							if (idPropertyName in item)
							{
								var idProperty:String = serializedData[item[idPropertyName]];
								if (idProperty)
								{
									model = OmniTemplateManager.instance.createModel(klass, idProperty);
								}
							}
						}
						if (!model)
						{
							model = new klass();
						}
						data.push(model);
					}
					else
					{
						data.push({});
					}
				}
				else
				{
					data.push(item);
				}
			});

			// A second pass now populates all of the empty objects.
			// Since the objects already exist, it doesn't matter if a reference
			// is assigned before or after all of its properties are populated,
			// which also means it doesn't matter how deeply nested things might be.
			data.forEach(function(item:*, index:int, ... args):void
			{
				var serializedItem:* = serializedData[index];
				var type:String = typeof item;
				if (type === 'object' && item !== null)
				{
					var className:String = Util.getClassName(item);
					var newArray:Array = item as Array;
					if (className === 'Array')
					{
						serializedItem.forEach(function(position:int, ... args):void
						{
							newArray.push(data[position]);
						});
					}
					else if (className === 'Object')
					{
						Util.keys(serializedItem).forEach(function(itemKey:String, ... args):void
						{
							var position:int = serializedItem[itemKey];
							item[itemKey] = data[position];
						});
					}
					else
					{
						var properties:Array = _registeredClasses[className];
						if (!properties)
						{
							Util.trace('WARNING: Attempted to load an instance of `' + className + '` but it has not been registered');
							properties = [];
						}
						properties.forEach(function(property:String, ... args):void
						{
							if (property in serializedItem)
							{
								var position:int = serializedItem[property];
								item[property] = data[position];
							}
						});
					}
				}
			});

			// A final pass through root assigns everything that will be returned
			var root:Object = {};
			Util.keys(serializedRoot).forEach(function(key:String, ... args):void
			{
				var position:int = serializedRoot[key];
				root[key] = data[position];
			});

			return root;
		}

		/**
		 * Provides the details of a class registration, primary for introspection purposes.
		 * @param	klass
		 * @return
		 */
		public function getClassRegistration(klass:Class):Object
		{
			var key:String = Util.getClassName(klass);
			var properties:Array = _registeredClasses[key];
			if (!properties)
			{
				return undefined;
			}
			return {'properties': [].concat(properties)};
		}

		/**
		 * Registers a model's class so that it may be persisted.
		 * Unlike arrays and objects, which get everything serialized, models
		 * must specify which properties should be serialized since they're generally
		 * more complex and generally don't need everything persisted.
		 * @param	klass        The model class
		 * @param	properties   An array of property names to serialize
		 */
		public function registerClass(klass:Class, properties:Array):void
		{
			var key:String = Util.getClassName(klass);
			_registeredClasses[key] = [].concat(properties);
		}

		/**
		 * Serializes an object into a json string which may be saved.
		 * @param	root    A hash of all data to be serialized
		 * @return          A string of JSON which may be persisted with `registerDynamicString`
		 */
		public function toJsonString(root:Object):String
		{
			return new JSONEncoder(toJsonObject(root)).getString();
		}

		/**
		 * Serializes a root object into a json object.
		 * To maintain references, every value in the root hash, arrays, objects, and models is replaced
		 * with an index to where the value lives in the serialized data.
		 * This also allows items to contain references to themselves that can be deserialized later.
		 * @param	root    A hash of all data to be serialized
		 * @return          An object of the serialized representation of the data
		 */
		public function toJsonObject(root:Object):Object
		{
			var queue:Array = [];
			var pointer:uint = 0;

			var serializedRoot:Object = {};
			var serializedData:Array = [];

			// Returns the item's position in the queue, adds the item if it doesn't already exist
			// For example, if the number 42 appears multiple times, it all gets queued to the same position
			// so that every value is only processed once whether an object or not
			function fromQueue(item:*):int
			{
				var position:int = queue.indexOf(item);
				if (position === -1)
				{
					queue.push(item);
					return queue.length - 1;
				}
				return position;
			}

			// Add the root keys first.. even root keys may be duplicate object references
			Util.keys(root).forEach(function(key:String, ... args):void
			{
				serializedRoot[key] = fromQueue(root[key]);
			});

			// Iterate the queue until its empty.. fromQueue will keep adding each new value it finds
			// Each item is pushed to serializedData in order so that the index position of the raw
			// item in the queue matches its serialized version in serializedData
			while (pointer < queue.length)
			{
				var item:* = queue[pointer];
				pointer = pointer + 1;

				var type:String = typeof item;
				if (item === undefined || item === null)
				{
					// Treat undefined as null
					// Alternatively it could be skipped, but then it won't overwrite values when deserialized
					// Or it could have a custom representation but null seems like a more reasonable compromise
					serializedData.push(null);
				}
				else if (type !== 'object')
				{
					// Anything else that's not an object is simply pushed: numbers, strings, booleans, NaN
					serializedData.push(item);
				}
				else
				{
					var className:String = Util.getClassName(item);
					if (className === 'Array')
					{
						// Map each item in the array to its position in the queue
						serializedData.push((item as Array).map(function(arrayItem:*, ... args):int
						{
							return fromQueue(arrayItem);
						}));
					}
					else if (className === 'Object')
					{
						// Map all values in an object to their positions in the queue
						var newItem:Object = {};
						Util.keys(item).forEach(function(itemKey:String, ... args):void
						{
							newItem[itemKey] = fromQueue(item[itemKey]);
						});
						serializedData.push(newItem);
					}
					else
					{
						// Map all registered properties of a model to their positions in the queue
						var properties:Array = _registeredClasses[className];
						if (!properties)
						{
							Util.trace('WARNING: Attempted to save an instance of `' + className + '` but it has not been registered');
							properties = [];
						}
						var newModel:Object = {__type: className};
						properties.forEach(function(property:String, ... args):void
						{
							newModel[property] = fromQueue(item[property]);
						});
						serializedData.push(newModel);
					}
				}
			}

			// Return the final serialized structure
			return {root: serializedRoot, data: serializedData};
		}
	}
}