package omnimodlib
{
	import models.Character;
	import models.Characters;

	public class CoreCharacterTemplateExtension extends Extension
	{
		public var templateName:String = '';

		public function CoreCharacterTemplateExtension()
		{
			super();
		}

		public function get templateId():String
		{
			return templateName.replace(/\s+/, '');
		}
	}
}