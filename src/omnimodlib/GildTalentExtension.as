package omnimodlib
{

	public class GildTalentExtension extends Extension
	{
		public var characterName:String;
		public var name:String;
		public var description:String;
		public var tier:uint;

		public function GildTalentExtension()
		{
			super();
		}

		public function get active():Boolean
		{
			return data.getBoolean([uuid, 'active']);
		}

		public function set active(value:Boolean):void
		{
			if (value === active)
			{
				return;
			}

			if (value)
			{
				data.setBoolean([uuid, 'active'], true);
			}
			else
			{
				data.removeKey([uuid, 'active']);
			}
		}

		public function get available():Boolean
		{
			return !hasErrors;
		}

		public function get hasErrors():Boolean
		{
			if (requiredLevel > extensions.gilding.gilds)
			{
				return true;
			}

			var activeTalentInTier:GildTalentExtension = extensions.gilding.getActiveTalentForTier(tier);
			if (activeTalentInTier && activeTalentInTier !== this)
			{
				return true;
			}

			return false;
		}

		public function get errors():Array
		{
			var errors:Array = [];

			if (requiredLevel > extensions.gilding.gilds)
			{
				errors.push(_({one: 'Requires {count} gild level.', other: 'Requires {count} gild levels.'}, {count: requiredLevel}));
			}

			var activeTalentInTier:GildTalentExtension = extensions.gilding.getActiveTalentForTier(tier);
			if (activeTalentInTier && activeTalentInTier !== this)
			{
				errors.push(_('Another talent has already been chosen in this tier. This talent will be available again after your next gild.'));
			}

			return errors;
		}

		public function get requiredLevel():uint
		{
			return extensions.gilding.getRequiredLevelForTier(tier);
		}

		public function get tooltip():Object
		{
			var header:String = name;
			var body:String = description;
			var status:String;

			var index:uint;
			var errors:Array = this.errors;
			if (errors.length === 0)
			{
				body += '\n\n';
				if (active)
				{
					status = _('This talent is currently active.');
				}
				else
				{
					status = _('Click to choose this talent, other talents in this tier will become unavailable until your next gild.');
				}
				body += _('{ 0 | color:yellow }', status);
			}
			else
			{
				body += '\n';
				for (index = 0; index < errors.length; index++)
				{
					body += _('\n{ 0 | color:warning }', errors[index]);
				}
			}

			return {header: header, body: body};
		}

		public function onPopulateGildTalents(gildTalents:Array):void
		{
			if (!characterName || currentCharacter.name === characterName)
			{
				gildTalents.push(this);
			}
		}
	}
}