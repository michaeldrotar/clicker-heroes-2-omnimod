package omnimodlib
{
	import models.Character;

	public class CharacterPropertyTracker extends Extension
	{
		private var _propertyTracker:Character;
		private var _trackedProperties:Array;

		public function CharacterPropertyTracker()
		{
			super();

			_trackedProperties = [];
		}

		public function onCharacterStarted():void
		{
			var property:String;
			for each (property in _trackedProperties)
			{
				_propertyTracker[property] = currentCharacter[property];
			}
		}

		public function onCharacterUpdate(dt:int):void
		{
			var property:String;
			for each (property in _trackedProperties)
			{
				if (_propertyTracker[property] != currentCharacter[property])
				{
					var oldValue:* = _propertyTracker[property];
					var newValue:* = currentCharacter[property];
					_propertyTracker[property] = newValue;
					trigger('onCharacterPropertyChanged', {property: property, oldValue: oldValue, newValue: newValue});
				}
			}
		}
	}
}