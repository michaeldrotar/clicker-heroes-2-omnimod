package omnimodlib.core
{
	import models.Automator;
	import models.AutomatorGem;
	import models.Character;
	import omnimodlib.Extension;

	public class AutomatorExtension extends Extension
	{
		public function enhanceBuffGem(characterTemplate:Character, nodeTypeId:String, gemId:String, buffName:String):void
		{
			var nodeType:Object = characterTemplate.levelGraphNodeTypes[nodeTypeId];
			nodeType['setupFunction'] = after(nodeType['setupFunction'], function():void
			{
				var gem:AutomatorGem = getStaticGem(gemId);
				gem.description = _('Activates { 0 } on cooldown unless { 0 } buff is already active.', _(buffName));
				extensions.automator.setGemCanActivateFunction(gem, hook(gem.canActivate, function(originalFunction:Function):Boolean
				{
					return originalFunction() && !extensions.buffs.getCurrentBuff(buffName);
				}));
			});
		}

		public function getStaticGem(id:String):AutomatorGem
		{
			return Automator.allGems[id];
		}

		public function setGemCanActivateFunction(gem:AutomatorGem, canActivateFunction:Function = null):void
		{
			if (canActivateFunction !== null)
			{
				gem.canActivate = canActivateFunction;
			}

			gem.onActivate = hook(gem.onActivate, function(originalFunction:Function):Boolean
			{
				if (!gem.canActivate())
				{
					return false;
				}

				return originalFunction();
			});
		}
	}
}