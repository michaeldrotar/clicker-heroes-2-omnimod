package omnimodlib.core
{
	import heroclickerlib.CH2;
	import models.Character;
	import models.RubyPurchase;
	import omnimodlib.Extension;
	import omnimodlib.Util;

	public class RubyShop extends Extension
	{
		public static const STANDARD_COOLDOWN:uint = Character.RUBY_SHOP_APPEARANCE_COOLDOWN;
		public static const STANDARD_DURATION:uint = Character.RUBY_SHOP_APPEARANCE_DURATION;

		public var maxItems:Number;
		public var isActive:Boolean;

		public function RubyShop()
		{
			super();
			maxItems = 3;
			isActive = false;
		}

		public function get cooldown():Number
		{
			return STANDARD_COOLDOWN;
		}

		public function get duration():Number
		{
			return STANDARD_DURATION;
		}

		public function onCharacterCreated():void
		{
			currentCharacter.generateRubyShopHandler = this;
			currentCharacter.populateRubyPurchaseOptionsHandler = this;
			currentCharacter.shouldRubyShopDeactivateHandler = this;
			currentCharacter.shouldRubyShopActivateHandler = this;
			currentCharacter.updateRubyShopFieldsHandler = this;
		}

		public function onCharacterStarted():void
		{
			isActive = false;
		}

		public function generateRubyShopOverride():void
		{
			currentCharacter.currentRubyShop = [];
			trigger('onRubyShopGenerating');

			var index:uint;
			var rubyPurchaseOptions:Array = currentCharacter.rubyPurchaseOptions;
			var choices:Object = {};
			var references:Object = {};
			var rubyPurchaseOption:RubyPurchase;
			for (index = 0; index < rubyPurchaseOptions.length; index++)
			{
				rubyPurchaseOption = rubyPurchaseOptions[index];
				if (rubyPurchaseOption.canAppear())
				{
					references[rubyPurchaseOption.name] = rubyPurchaseOption;
					choices[rubyPurchaseOption.name] = rubyPurchaseOption.priority;
				}
			}

			var choiceName:String;
			for (index = 0; index < maxItems; index++)
			{
				choiceName = Util.weightedChoice(choices);
				if (!choiceName)
				{
					break;
				}

				delete choices[choiceName];
				currentCharacter.currentRubyShop.push(references[choiceName]);
			}

			trigger('onRubyShopGenerated');
		}

		public function populateRubyPurchaseOptionsOverride():void
		{
			currentCharacter.populateRubyPurchaseOptionsDefault();
			trigger('onPopulateRubyPurchaseOptions', currentCharacter.rubyPurchaseOptions);
		}

		public function shouldRubyShopActivateOverride():Boolean
		{
			var shouldActivate:Boolean = currentCharacter.timeSinceLastRubyShopAppearance > cooldown && !currentCharacter.didFinishWorld && currentCharacter.rubies >= 50 && !CH2.world.massiveOrangeFish.isActive;
			if (shouldActivate && !isActive)
			{
				isActive = true;
			}
			return isActive;
		}

		public function shouldRubyShopDeactivateOverride():Boolean
		{
			var shouldDeactivate:Boolean = currentCharacter.timeSinceLastRubyShopAppearance > duration || currentCharacter.didFinishWorld;
			if (shouldDeactivate && isActive)
			{
				isActive = false;
			}
			return !isActive;
		}

		public function updateRubyShopFieldsOverride(dt:int):void
		{
			currentCharacter.updateRubyShopFieldsDefault(dt);
		}
	}
}