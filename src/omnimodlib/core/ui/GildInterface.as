package omnimodlib.core.ui
{
	import omnimodlib.SubPanelExtension;
	import omnimodlib.core.ui.components.GildSubPanel;

	public class GildInterface extends SubPanelExtension
	{
		public var gildSubPanel:GildSubPanel;

		public function GildInterface()
		{
			super();

			gildSubPanel = new GildSubPanel();

			label = 'Gilding';
			subPanel = gildSubPanel;
			isVisible = function():Boolean
			{
				return currentCharacter.gilds > 0;
			};
			isGlowing = function():Boolean
			{
				return extensions.gilding.talentsAvailable;
			};
		}

		public function onExtensionsCreated():void
		{
			gildSubPanel.gilding = extensions.gilding;
		}
	}
}
