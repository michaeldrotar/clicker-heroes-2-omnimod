package omnimodlib.core.ui.components
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import lib.managers.TextManager;

	public class GildButton extends OmniComponent
	{
		public var label:String;

		private var _textField:TextField;

		public function GildButton()
		{
			super();

			_textField = new TextField();
			_textField.autoSize = TextFieldAutoSize.LEFT;
			addChild(_textField);

			enableMouse(true);
		}

		override public function redraw():void
		{
			super.redraw();

			graphics.clear();

			TextManager.setText(_textField, label, 16);
			_textField.textColor = enabled ? 0xffffff : 0xafafaf;

			graphics.beginFill(enabled ? (_mouseOver ? 0xf1d961 : 0xefd031) : 0xdddddd);
			graphics.drawRoundRect(0, 0, width + 30, height + 15, 15, 15);

			graphics.beginFill(enabled ? (_mouseDown ? 0x56c3f9 : 0x79cff9) : 0xeeeeee);
			graphics.drawRoundRect(3, 3, width - 6, height - 6, 10, 10);

			_textField.x = (width - _textField.width) / 2;
			_textField.y = (height - _textField.height) / 2;
		}
	}
}