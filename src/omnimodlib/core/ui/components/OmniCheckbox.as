package omnimodlib.core.ui.components
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import lib.managers.TextManager;

	public class OmniCheckbox extends OmniComponent
	{
		public var checked:Boolean = false;
		public var label:String;

		private var _textField:TextField;

		public function OmniCheckbox()
		{
			super();

			_textField = new TextField();
			_textField.autoSize = TextFieldAutoSize.LEFT;
			addChild(_textField);

			enableMouse(true);
		}

		override public function redraw():void
		{
			super.redraw();

			graphics.clear();

			TextManager.setText(_textField, label, 16);
			_textField.textColor = enabled ? colors.enabledText : colors.disabledText;

			graphics.beginFill(enabled ? colors.enabledBorder : colors.disabledBorder);
			graphics.drawRoundRect(0, 4, 16, 16, 2, 2);

			graphics.beginFill(enabled ? colors.enabledBackground : colors.disabledBackground);
			graphics.drawRoundRect(2, 6, 12, 12, 2, 2);

			if (checked)
			{
				graphics.beginFill(enabled ? colors.enabledText : colors.disabledText);
				graphics.drawRoundRect(4, 8, 8, 8, 2, 2);
			}

			_textField.x = 20;
			_textField.y = 0;
		}
	}
}
