package omnimodlib.core.ui.components
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import omnimodlib.Util;
	import ui.elements.SubPanel;

	public class OmniSubPanel extends SubPanel
	{
		public function OmniSubPanel()
		{
			super();
			setDisplay(new MovieClip());
		}

		public function _(text:*, ... args):String
		{
			if (args.length === 0)
			{
				// If no args given, use its own properties by their names
				args = [this];
			}
			return OmnimodMain.instance._.apply(OmnimodMain.instance, [].concat(text, args));
		}

		public function dispatch(methodName:String, ... args):void
		{
			var index:int;
			var child:DisplayObject;
			for (index = 0; index < display.numChildren; index++)
			{
				child = display.getChildAt(index);
				if (methodName in child)
				{
					child[methodName].apply(child, args);
				}
			}
		}

		public function trace(... args):void
		{
			Util.trace.apply(Util, args);
		}
	}
}