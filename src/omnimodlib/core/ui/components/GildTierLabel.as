package omnimodlib.core.ui.components
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import lib.managers.TextManager;

	public class GildTierLabel extends OmniComponent
	{
		public var requiredGilds:int;
		public var available:Boolean;
		public var highlighted:Boolean;

		private var _labelField:TextField
		private var _state:String

		public function GildTierLabel()
		{
			super();

			_labelField = new TextField();
			_labelField.autoSize = TextFieldAutoSize.LEFT;
			addChild(_labelField);
		}

		override public function redraw():void
		{
			graphics.clear();

			var color:Number = available ? (highlighted ? colors.buttonBorder : colors.white) : colors.disabledText

			_labelField.textColor = color;
			TextManager.setText(_labelField, requiredGilds.toString(), 24);
			_labelField.x = (50 - _labelField.width) / 2;
			_labelField.y = (50 - _labelField.height) / 2;

			graphics.beginFill(color);
			graphics.moveTo(0, 5);
			graphics.lineTo(0, 0);
			graphics.lineTo(5, 0);
			graphics.moveTo(45, 0);
			graphics.lineTo(50, 0);
			graphics.lineTo(50, 5);
			graphics.moveTo(50, 45);
			graphics.lineTo(50, 50);
			graphics.lineTo(45, 50);
			graphics.moveTo(5, 50);
			graphics.lineTo(0, 50);
			graphics.lineTo(0, 45);
			graphics.endFill();
		}
	}
}