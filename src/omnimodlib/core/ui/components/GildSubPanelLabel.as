package omnimodlib.core.ui.components
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import lib.managers.TextManager;

	public class GildSubPanelLabel extends OmniComponent
	{
		public var label:String;
		public var value:Number;
		public var textAlignment:String;

		private var _labelField:TextField;
		private var _valueField:TextField;

		public function GildSubPanelLabel()
		{
			super();

			_labelField = new TextField();
			_labelField.autoSize = TextFieldAutoSize.LEFT;
			_labelField.textColor = 0xffffff;
			_labelField.x = 5;
			_labelField.y = 0;
			addChild(_labelField);

			_valueField = new TextField();
			_valueField.autoSize = TextFieldAutoSize.LEFT;
			_valueField.textColor = 0xffffff;
			_valueField.x = 5;
			_valueField.y = 15;
			addChild(_valueField);
		}

		override public function redraw():void
		{
			graphics.clear();

			TextManager.setText(_labelField, label.toUpperCase(), 11);
			TextManager.setText(_valueField, value.toString(), 16);

			graphics.beginFill(0x000000, 0.3);
			graphics.drawRoundRect(0, 0, 145, 40, 15, 15);

			switch (textAlignment)
			{
			case TextFormatAlign.CENTER:
				_labelField.x = (width - _labelField.width) / 2;
				_valueField.x = (width - _valueField.width) / 2;
				break;

			case TextFormatAlign.RIGHT:
				_labelField.x = width - _labelField.width - 5;
				_valueField.x = width - _valueField.width - 5;
				break;

			default:
				_labelField.x = 5;
				_valueField.x = 5;
			}

		}
	}
}