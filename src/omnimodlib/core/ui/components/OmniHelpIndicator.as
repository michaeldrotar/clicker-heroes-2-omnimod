package omnimodlib.core.ui.components
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import lib.managers.TextManager;

	public class OmniHelpIndicator extends OmniComponent
	{
		private var _helpIcon:TextField;

		public function OmniHelpIndicator()
		{
			super();

			_helpIcon = new TextField();
			_helpIcon.autoSize = TextFieldAutoSize.LEFT;
			TextManager.setText(_helpIcon, "?", 32);
			addChild(_helpIcon);

			enableMouse();
		}

		override public function redraw():void
		{
			super.redraw();

			graphics.clear();

			_helpIcon.textColor = _mouseOver ? 0xffcc00 : 0xafafaf;
		}
	}
}