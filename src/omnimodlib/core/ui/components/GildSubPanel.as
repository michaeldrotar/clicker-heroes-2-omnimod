package omnimodlib.core.ui.components
{
	import flash.events.Event;
	import flash.text.TextFormatAlign;
	import omnimodlib.GildTalentExtension;
	import omnimodlib.core.character.Gilding;
	import omnimodlib.core.ui.InterfaceMeasurements;
	import omnimodlib.core.ui.components.GildButton;

	public class GildSubPanel extends OmniSubPanel
	{
		public var gilding:Gilding;

		public var helpIndicator:OmniHelpIndicator;

		public var tierLabels:Array;
		public var talentButtons:Array;

		public var gildLevelLabel:GildSubPanelLabel;

		public function GildSubPanel()
		{
			super();
		}

		override public function activate():void
		{
			super.activate();

			var help:String = '';
			help += 'Press the Gild Now button to start the next gild, resetting all skill points and gild talents, and unlocking the next {0} worlds.\n';
			help += 'Once you gild, you\'ll immediately receive new skill points to spend and may select new talents based on your gild level.'

			helpIndicator = new OmniHelpIndicator();
			helpIndicator.redraw();
			helpIndicator.x = InterfaceMeasurements.LEFT_PANEL_INNER_WIDTH - InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH - helpIndicator.width;
			helpIndicator.y = InterfaceMeasurements.LEFT_PANEL_INNER_Y + InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH - 5;
			helpIndicator.tooltip = {'body': _(help, gilding.currentCharacter.worldsPerGild)}
			display.addChild(helpIndicator);

			tierLabels = [];
			talentButtons = [];

			var index:int;
			var talent:GildTalentExtension;
			var tierLabel:GildTierLabel;
			var talentButton:GildTalentButton;
			var tierNumber:Number = 0;
			var talents:Array = gilding.getTalentsForTier(tierNumber);
			while (talents.length > 0 || tierNumber < Gilding.TIER_GILDS_REQUIRED.length)
			{
				tierLabel = new GildTierLabel();
				tierLabel.requiredGilds = gilding.getRequiredLevelForTier(tierNumber);
				tierLabel.available = gilding.gilds < tierLabel.requiredGilds;
				tierLabel.x = InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH;
				tierLabel.y = 150 + (tierNumber * 70);
				tierLabel.redraw();
				tierLabels.push(tierLabel);
				display.addChild(tierLabel);

				for (index = 0; index < talents.length; index++)
				{
					talent = talents[index];

					talentButton = new GildTalentButton();
					talentButton.talent = talent;
					talentButton.enabled = talent.errors.length === 0;
					talentButton.icon = 'X';
					talentButton.tooltip = talent.tooltip;
					talentButton.redraw();
					talentButton.x = (tierLabel.x + tierLabel.width + InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH) + ((talentButton.width + InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH) * index);
					talentButton.y = tierLabel.y;
					talentButtons.push(talentButton);
					talentButton.onClick = onTalentButtonClick;
					display.addChild(talentButton);
				}

				tierNumber++;
				talents = gilding.getTalentsForTier(tierNumber);
			}

			gildLevelLabel = new GildSubPanelLabel();
			gildLevelLabel.label = 'Gild Level';
			gildLevelLabel.value = gilding.gilds;
			gildLevelLabel.textAlignment = TextFormatAlign.LEFT;
			gildLevelLabel.redraw();
			gildLevelLabel.x = InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH;
			gildLevelLabel.y = InterfaceMeasurements.LEFT_PANEL_INNER_Y + InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH;
			display.addChild(gildLevelLabel);

			dispatch('activate');
		}

		override public function deactivate(refresh:Boolean = false):void
		{
			super.deactivate(refresh);

			dispatch('deactivate', refresh);

			helpIndicator = null;

			while (tierLabels.length > 0)
			{
				tierLabels.pop();
			}
			tierLabels = null;

			while (talentButtons.length > 0)
			{
				talentButtons.pop();
			}
			talentButtons = null;

			gildLevelLabel = null;
			display.removeChildren();
		}

		override public function update(dt:Number):void
		{
			super.update(dt);

			var index:uint;

			if (gildLevelLabel.value !== gilding.gilds)
			{
				gildLevelLabel.value = gilding.gilds;
				gildLevelLabel.redraw();
			}

			var tierLabel:GildTierLabel;
			var tierLabelAvailable:Boolean;
			var tierLabelHighlighted:Boolean;
			for (index = 0; index < tierLabels.length; index++)
			{
				tierLabel = tierLabels[index];
				tierLabelAvailable = gilding.gilds >= tierLabel.requiredGilds;
				tierLabelHighlighted = tierLabelAvailable && !gilding.getActiveTalentForTier(index);

				if (tierLabel.available !== tierLabelAvailable || tierLabel.highlighted !== tierLabelHighlighted)
				{
					tierLabel.available = tierLabelAvailable;
					tierLabel.highlighted = tierLabelHighlighted;
					tierLabel.redraw();
				}
			}

			var talentButton:GildTalentButton;
			var talentButtonEnabled:Boolean;
			var talentButtonActive:Boolean;
			for (index = 0; index < talentButtons.length; index++)
			{
				talentButton = talentButtons[index];
				talentButtonEnabled = talentButton.talent.available;
				talentButtonActive = talentButtonEnabled && talentButton.talent.active;

				if (talentButton.enabled !== talentButtonEnabled || talentButton.active !== talentButtonActive)
				{
					talentButton.enabled = talentButtonEnabled;
					talentButton.active = talentButtonActive;
					talentButton.tooltip = talentButton.talent.tooltip;
					talentButton.redraw();
				}
			}

			dispatch('update', dt);
		}

		public function onTalentButtonClick(event:Event):void
		{
			var talentButton:GildTalentButton = event.currentTarget as GildTalentButton;
			talentButton.talent.active = true;
		}
	}
}