package omnimodlib.core.ui.components
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import lib.managers.TextManager;
	import omnimodlib.GildTalentExtension;
	import omnimodlib.Util;

	public class GildTalentButton extends OmniComponent
	{
		public var active:Boolean;
		public var talent:GildTalentExtension;
		public var icon:String;

		private var _labelField:TextField;

		public function GildTalentButton()
		{
			super();

			_labelField = new TextField();
			_labelField.autoSize = TextFieldAutoSize.LEFT;
			_labelField.wordWrap = true;
			addChild(_labelField);

			enableMouse(true);
		}

		override public function redraw():void
		{
			super.redraw();

			graphics.clear();

			TextManager.setText(_labelField, talent.name, 16);
			_labelField.textColor = enabled ? colors.white : colors.disabledText;

			graphics.beginFill(enabled ? (active ? colors.buttonBorder : (_mouseOver ? colors.buttonBorder : colors.disabledText)) : colors.disabledBorder);
			graphics.drawRoundRect(0, 0, 145, 50, 15, 15);

			graphics.beginFill(enabled ? (active ? 0x79cff9 : (_mouseDown ? 0x56c3f9 : 0x79cff9)) : colors.disabledBackground);
			graphics.drawRoundRect(3, 3, width - 6, height - 6, 10, 10);

			_labelField.x = (width - _labelField.width) / 2;
			_labelField.y = (height - _labelField.height) / 2;
		}
	}
}