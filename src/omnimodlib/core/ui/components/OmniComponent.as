package omnimodlib.core.ui.components
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import heroclickerlib.managers.MouseEnabler;
	import heroclickerlib.managers.TooltipManager;
	import omnimodlib.OmniColorPalette;
	import omnimodlib.Util;

	public class OmniComponent extends Sprite
	{
		public var enabled:Boolean;
		public var onClick:Function;
		public var tooltip:Object;

		protected var _mouseDown:Boolean;
		protected var _mouseOver:Boolean;

		public function OmniComponent()
		{
			super();
			enabled = true;
		}

		protected function get colors():OmniColorPalette
		{
			return OmniColorPalette.instance;
		}

		public function _(text:*, ... args):String
		{
			if (args.length === 0)
			{
				// If no args given, use its own properties by their names
				args = [this];
			}
			return OmnimodMain.instance._.apply(OmnimodMain.instance, [].concat(text, args));
		}

		public function enableMouse(clickable:Boolean = false):void
		{
			MouseEnabler.instance.enable(this);

			if (clickable)
			{
				buttonMode = true;
				useHandCursor = true;
				addEventListener(MouseEvent.CLICK, _onClick, false, 0, true);
			}

			addEventListener(MouseEvent.MOUSE_DOWN, _onMouseDown, false, 0, true);
			addEventListener(MouseEvent.MOUSE_UP, _onMouseUp, false, 0, true);
			addEventListener(MouseEvent.MOUSE_OVER, _onMouseOver, false, 0, true);
			addEventListener(MouseEvent.MOUSE_OUT, _onMouseOut, false, 0, true);
		}

		public function redraw():void
		{
			useHandCursor = enabled ? true : false;
		}

		public function trace(... args):void
		{
			Util.trace.apply(Util, args);
		}

		protected function _onClick(event:Event):void
		{
			Util.traceLine();
			Util.trace('_onClick');
			if (enabled && typeof(onClick) === 'function')
			{
				Util.trace('onClick');
				onClick(event);
			}
			redraw();
		}

		protected function _onMouseDown(event:Event):void
		{
			_mouseDown = true;
			redraw();
		}

		protected function _onMouseUp(event:Event):void
		{
			_mouseDown = false;
			redraw();
		}

		protected function _onMouseOver(event:Event):void
		{
			_mouseOver = true;
			redraw();

			if (tooltip)
			{
				TooltipManager.instance.showTooltip(tooltip['header'] || '', tooltip['body'] || '', tooltip['anchor'] || this);
			}
		}

		public function _onMouseOut(event:Event):void
		{
			_mouseOver = false;
			_mouseDown = false;
			redraw();

			TooltipManager.instance.hide();
		}
	}
}