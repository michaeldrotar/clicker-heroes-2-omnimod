package omnimodlib.core.ui
{

	public class InterfaceMeasurements
	{
		public static const SCREEN_WIDTH:Number = 1280;
		public static const SCREEN_HEIGHT:Number = 720;

		public static const LEFT_PANEL_X:Number = 0;
		public static const LEFT_PANEL_Y:Number = 0;
		public static const LEFT_PANEL_WIDTH:Number = 572;
		public static const LEFT_PANEL_HEIGHT:Number = SCREEN_HEIGHT;

		public static const LEFT_PANEL_INNER_X:Number = 0;
		public static const LEFT_PANEL_INNER_Y:Number = 67;
		public static const LEFT_PANEL_INNER_WIDTH:Number = 558;
		public static const LEFT_PANEL_INNER_HEIGHT:Number = SCREEN_HEIGHT - LEFT_PANEL_INNER_Y;

		public static const LEFT_PANEL_EDGE_WIDTH:Number = 14;

		public function InterfaceMeasurements()
		{
		}
	}
}