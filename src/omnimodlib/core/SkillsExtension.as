package omnimodlib.core
{
	import models.Character;
	import models.Skill;
	import omnimodlib.Extension;
	import omnimodlib.Util;

	public class SkillsExtension extends Extension
	{
		private var _refreshers:Array;

		public function SkillsExtension()
		{
			super();

			_refreshers = [];
		}

		public function createSkill(id:String):Skill
		{
			var skill:Skill = new Skill();
			skill.modName = modName;
			skill.name = id; // this is also the uid

			// these properties should normally be set as needed
			skill.iconId = 0;
			skill.tooltipFunction = null;
			skill.effectFunction = null;

			skill.cooldown = 0;
			skill.ignoresGCD = false;
			skill.minimumRange = 0;
			skill.maximumRange = 9000;

			skill.energyCost = 0;
			skill.manaCost = 0;
			skill.usesMaxEnergy = false;
			skill.usesMaxMana = false;

			// these properties are more obscure
			skill.isActive = false; // if true, the current character has learned/unlocked it
			skill.isConsumable = false; // if true, skill won't consume mana or energy on use
			skill.castTime = 0; // seems partially implemented: pauses the character and creates a progress bar but never resumes
			skill.level = 1; // items can have skills, and those skills have their levels visible in the item tooltip...?  may be unused

			skill.description = ''; // unused
			skill.consumableOnly = false; // unused
			skill.minimumAscensions = 0; // unused

			return skill;
		}

		public function createStandardTooltip(id:String, name:String = null, description:String = null):Object
		{
			var skill:Skill = getCurrentSkill(id);
			var costDescriptions:Array = getStandardCostDescriptions(skill);

			if (costDescriptions.length)
			{
				description = costDescriptions.join('\n') + '\n\n' + description;
			}

			return createTooltip(name, description);
		}

		public function register(skill:Skill):void
		{
			Character.staticSkillInstances[skill.uid] = skill;
		}

		public function registerRefresh(id:String, refresh:Function = null):void
		{
			_refreshers.push([id, refresh]);
		}

		public function getStaticSkill(id:String):Skill
		{
			return Character.staticSkillInstances[id];
		}

		public function getCurrentSkill(id:String):Skill
		{
			return currentCharacter.skills[id];
		}

		public function getCurrentCooldown(skill:Skill):Number
		{
			return skill.cooldown / currentCharacter.hasteRating.numberValue();
		}

		public function getCurrentEnergyCost(skill:Skill):Number
		{
			return currentCharacter.getCalculatedEnergyCost(skill);
		}

		public function getCurrentManaCost(skill:Skill):Number
		{
			return skill.getManaCost();
		}

		public function getStandardCooldownDescription(cooldown:Number):String
		{
			return _('|c:label:Cooldown:|r { 0 | duration }', cooldown);
		}

		public function getStandardResourceDescription(cost:Number, resource:String):String
		{
			var label:String = cost < 0 ? 'Generate' : 'Cost';
			return _('|c:label:' + label + ':|r { 0 | label:' + resource + ' }', Math.abs(cost));
		}

		public function getStandardCostDescriptions(skill:Skill):Array
		{
			var energyCost:Number = getCurrentEnergyCost(skill);
			var manaCost:Number = getCurrentManaCost(skill);
			var cooldown:Number = getCurrentCooldown(skill);

			var descriptions:Array = [];

			// Generate
			if (energyCost < 0)
			{
				descriptions.push(getStandardResourceDescription(energyCost, 'energy'));
			}
			if (manaCost < 0)
			{
				descriptions.push(getStandardResourceDescription(manaCost, 'mana'));
			}

			// Cost
			if (energyCost > 0)
			{
				descriptions.push(getStandardResourceDescription(energyCost, 'energy'));
			}
			if (manaCost > 0)
			{
				descriptions.push(getStandardResourceDescription(manaCost, 'mana'));
			}

			// Cooldown
			if (cooldown > 0)
			{
				descriptions.push(getStandardCooldownDescription(cooldown));
			}

			return descriptions;
		}

		public function getStandardSkillTooltipSuffix(skill:Skill):String
		{
			var descriptions:Array = getStandardCostDescriptions(skill);
			return descriptions.length > 0 ? '\n\n' + descriptions.join('\n') : '';
		}

		public function onCharacterStarted():void
		{
			Util.forEach(_refreshers, function(refresher:Array, ... args):void
			{
				var id:String = refresher[0];
				var refreshFn:Function = refresher[1];

				var staticSkill:Skill = getStaticSkill(id);
				var currentSkill:Skill = getCurrentSkill(id);

				Util.forEach(['castTime', 'cooldown', 'manaCost', 'energyCost'], function(field:String, ... args):void
				{
					currentSkill[field] = staticSkill[field];
				});

				if (refreshFn is Function)
				{
					refreshFn(currentSkill);
				}
			});
		}
	}
}