package omnimodlib.core.gilding
{
	import flash.events.Event;
	import models.AutomatorWorldEndOption;
	import omnimodlib.Extension;
	import omnimodlib.core.ui.InterfaceMeasurements;
	import omnimodlib.core.ui.components.OmniCheckbox;
	import swc.HUD.AutomatorPanelDisplay;
	import swc.hud.WorldEndAutomationDisplay;
	import ui.CH2UI;
	import ui.panels.automator.AutomatorPanel;

	public class AutomaticallyGildExtension extends Extension
	{
		public const WORLD_END_OPTIONS_TO_HOOK:Array = ['Attempt Next World', 'Attempt Highest World'];

		public var automaticallyGildCheckbox:OmniCheckbox;

		public function AutomaticallyGildExtension()
		{
			super();
		}

		public function get automatorPanel():AutomatorPanel
		{
			return CH2UI.instance.mainUI.mainPanel.automatorPanel;
		}

		public function get automatorPanelDisplay():AutomatorPanelDisplay
		{
			return automatorPanel.display as AutomatorPanelDisplay;
		}

		public function get automatorPanelDisplayHasCheckbox():Boolean
		{
			if (!automaticallyGildCheckbox)
			{
				return false;
			}

			try
			{
				return automatorPanelDisplay && automatorPanelDisplay.getChildIndex(automaticallyGildCheckbox) !== -1;
			}
			catch (error:Error)
			{
				// Seems to error if the child isn't there
				if (error is ArgumentError)
				{
					return false;
				}
				throw error;
			}

			return false; // never hit, as3 is insisting there's no return value without this
		}

		public function get automaticallyGild():Boolean
		{
			return data.getBoolean([uuid, 'automaticallyContinue']);
		}

		public function set automaticallyGild(value:Boolean):void
		{
			data.setBoolean([uuid, 'automaticallyContinue'], value);
		}

		public function get isOnAutomatorPanel():Boolean
		{
			return CH2UI.instance.mainUI.mainPanel.isOnAutomatorPanel;
		}

		public function get isOnRelevantWorldEndOption():Boolean
		{
			var option:AutomatorWorldEndOption = currentCharacter.worldEndAutomationOptions[currentCharacter.currentWorldEndAutomationOption];
			return option && WORLD_END_OPTIONS_TO_HOOK.indexOf(option.name) !== -1;
		}

		public function isGildWorld(worldId:Number):Boolean
		{
			var expectedNumGilds:int = Math.floor((worldId - 1) / currentCharacter.worldsPerGild);
			return expectedNumGilds > currentCharacter.gilds;
		}

		public function activate():void
		{
			var worldEndAutomationDisplay:WorldEndAutomationDisplay = automatorPanelDisplay.worldEndAutomationDisplay
			worldEndAutomationDisplay.y -= 20;
			worldEndAutomationDisplay.header.y += 10;

			automaticallyGildCheckbox = new OmniCheckbox();
			automaticallyGildCheckbox.checked = automaticallyGild;
			automaticallyGildCheckbox.label = _('Continue to gilded worlds');
			automaticallyGildCheckbox.redraw();
			automaticallyGildCheckbox.x = (InterfaceMeasurements.LEFT_PANEL_INNER_X + InterfaceMeasurements.LEFT_PANEL_INNER_WIDTH) - ((worldEndAutomationDisplay.width + automaticallyGildCheckbox.width) / 2);
			automaticallyGildCheckbox.y = InterfaceMeasurements.LEFT_PANEL_INNER_Y + InterfaceMeasurements.LEFT_PANEL_INNER_HEIGHT - automaticallyGildCheckbox.height - InterfaceMeasurements.LEFT_PANEL_EDGE_WIDTH;
			automaticallyGildCheckbox.onClick = onToggle;
			automatorPanelDisplay.addChild(automaticallyGildCheckbox);
		}

		public function deactivate():void
		{
			if (automatorPanelDisplayHasCheckbox)
			{
				automatorPanelDisplay.removeChild(automaticallyGildCheckbox);
			}
			automaticallyGildCheckbox = null;
		}

		public function update(dt:int):void
		{
			if (automaticallyGildCheckbox.checked !== automaticallyGild)
			{
				automaticallyGildCheckbox.checked = automaticallyGild;
				automaticallyGildCheckbox.redraw();
			}
			automaticallyGildCheckbox.visible = isOnRelevantWorldEndOption;
		}

		public function onToggle(event:Event):void
		{
			automaticallyGild = !automaticallyGild;
		}

		public function onCharacterCreated():void
		{
			currentCharacter.populateWorldEndAutomationOptionsHandler = this;
		}

		public function onCharacterUpdate(dt:int):void
		{
			if (isOnAutomatorPanel)
			{
				if (extensions.gilding.gilds > 0 || extensions.gilding.gildingAvailable)
				{
					if (!automatorPanelDisplayHasCheckbox)
					{
						activate();
					}
					update(dt);
				}
			}
			else if (automaticallyGildCheckbox)
			{
				deactivate();
			}
		}

		public function populateWorldEndAutomationOptionsOverride():void
		{
			currentCharacter.populateWorldEndAutomationOptionsDefault();

			currentCharacter.worldEndAutomationOptions.forEach(function(option:AutomatorWorldEndOption, index:int, options:Array):void
			{
				if (WORLD_END_OPTIONS_TO_HOOK.indexOf(option.name) !== -1)
				{
					if (option.name === 'Attempt Next World')
					{
						option.onWorldEndFunction = function():void
						{
							var newWorldId:Number = currentCharacter.currentWorldId + 1;
							if (!automaticallyGild && isGildWorld(newWorldId))
							{
								newWorldId = newWorldId - 1;
								currentCharacter.hasSeenWorldsPanel = false;
							}

							currentCharacter.ascend(newWorldId);
						};
					}
					else if (option.name === 'Attempt Highest World')
					{
						option.onWorldEndFunction = function():void
						{
							var newWorldId:Number = currentCharacter.highestWorldCompleted + 1;
							if (!automaticallyGild && isGildWorld(newWorldId))
							{
								newWorldId = newWorldId - 1;
								currentCharacter.hasSeenWorldsPanel = false;
							}

							currentCharacter.ascend(newWorldId);
						};

					}
				}
			});
		}
	}
}