package omnimodlib.core.monsters
{
	import heroclickerlib.CH2;
	import models.Character;
	import models.Environment;
	import models.Monster;
	import omnimodlib.Extension;

	public class MonsterSpawner extends Extension
	{
		// Special
		public static var TREASURE_CHEST_ID:int = 1;

		// Plains
		public static var BLOOP_ID:int = 2;
		public static var GERBEELPILLAR_ID:int = 3;
		public static var CHEEKMUNK_ID:int = 4;
		public static var STUMP_ID:int = 5;
		public static var TREE_IMP_ID:int = 6;
		public static var BILLY_GOAT_ID:int = 7;
		public static var SUNDOUR_ID:int = 8;
		public static var ZOMBUG_ID:int = 9;

		public var formations:Object = {};
		public var groups:Array = [];

		public function MonsterSpawner()
		{
			formations['line-2'] = [{x: 40}, {x: -40}];
			formations['line-3'] = [{x: 80}, {}, {x: -80}];
			formations['circle-5'] = [{x: 60, y: -60}, {x: -60, y: -60}, {}, {x: 60, y: 60}, {x: -60, y: 60}];

			//groups.push(applyFormationByName('line-2', [BLOOP_ID]));
			//groups.push(applyFormationByName('line-3', [BLOOP_ID]));
			//groups.push(applyFormationByName('circle-5', [BLOOP_ID]));

			//groups.push(applyFormationByName('line-2', [BILLY_GOAT_ID]));
			//groups.push(applyFormationByName('line-3', [BILLY_GOAT_ID]));
		}

		public function get monsters():Vector.<Monster>
		{
			return CH2.world.monsters.monsters;
		}

		public function get lastMonster():Monster
		{
			return monsters.length > 0 ? monsters[monsters.length - 1] : null;
		}

		public function get loadedMonsterIds():Array
		{
			return Environment.loadedMonsterIds;
		}

		public function get numMonstersAlive():int
		{
			return CH2.world.monsters.numLivingMonsters();
		}

		public function get numMonstersKilled():int
		{
			return currentCharacter.monstersKilledOnCurrentZone;
		}

		public function get numMonstersSpawned():int
		{
			return numMonstersAlive + numMonstersKilled;
		}

		public function get numMonstersRemaining():Number
		{
			return currentCharacter.monstersPerZone - numMonstersSpawned;
		}

		public function applyFormation(formation:Array, monsterIds:Array):Array
		{
			var index:uint;
			var group:Array = [];
			for (index = 0; index < formation.length; index++)
			{
				group.push({id: monsterIds[index] || monsterIds[monsterIds.length - 1], x: formation[index].x || 0, y: formation[index].y || 0});
			}
			return group;
		}

		public function applyFormationByName(formationName:String, monsterIds:Array):Array
		{
			return applyFormation(formations[formationName], monsterIds);
		}

		public function spawnMonster(monsterData:Object = null):void
		{
			monsterData ||= {};
			monsterData.x ||= currentCharacter.x;
			monsterData.y ||= CH2.world.newMonsterWorldY();
			monsterData.id ||= CH2.world.monsters.getNextMonsterId();

			var monster:Monster = new Monster();
			if (CH2.roller.monsterRoller.boolean(currentCharacter.treasureChestChance.numberValue()))
			{
				monster.setup(Monster.TREASURE_CHEST_ID);
				monster.isTreasureChest = true;
			}
			else
			{
				monster.setup(monsterData.id);
			}

			monster.x = monsterData.x;
			monster.y = monsterData.y;
			CH2.world.addMonsterToWorld(monster);

			if (monster.display && monster.display.monsterAnimation)
			{
				monster.display.monsterAnimation.mSecPerFrame *= roller.integer(90, 110) / 100;
				monster.display.monsterAnimation.currentFrame += roller.integer(0, monster.display.monsterAnimation.totalFrames - 1);
			}
		}

		public function spawnMonsterGroup(group:Array):void
		{
			var index:uint;
			var y:Number = CH2.world.newMonsterWorldY()
			if (group.length > 1)
			{
				// Extra space for groups
				y = y + (2 * Character.ONE_METER_Y_DISTANCE);
			}
			for (index = 0; index < group.length; index++)
			{
				spawnMonster({id: group[index].id, x: currentCharacter.x + (group[index].x || 0), y: y + (group[index].y || 0)});
			}
		}

		public function spawnGroupMonsters():void
		{
			var index:uint;
			var index2:uint;
			var available:Boolean;
			var group:Array;
			var availableGroups:Array = [];

			for (index = 0; index < groups.length; index++)
			{
				group = groups[index];
				if (group.length < numMonstersRemaining)
				{
					available = true;
					for (index2 = 0; index2 < group.length; index2++)
					{
						if (loadedMonsterIds.indexOf(group[index2].id) === -1)
						{
							available = false;
							break;
						}
					}
					if (available)
					{
						availableGroups.push(group);
					}
				}
			}

			if (availableGroups.length === 0)
			{
				spawnRandomMonsters();
				return;
			}

			spawnMonsterGroup(availableGroups[roller.integer(0, availableGroups.length - 1)]);
		}

		public function spawnRandomMonsters():void
		{
			var index:uint;
			var formationName:String;
			var availableFormations:Array = [];
			for (formationName in formations)
			{
				if (formations[formationName].length < numMonstersRemaining)
				{
					availableFormations.push(formations[formationName]);
				}
			}

			if (availableFormations.length === 0)
			{
				spawnMonster();
				return;
			}

			var monsterIds:Array = [];
			var formation:Array = availableFormations[roller.integer(0, availableFormations.length - 1)];
			for (index = 0; index < formation.length; index++)
			{
				monsterIds.push(loadedMonsterIds[roller.integer(0, loadedMonsterIds.length - 1)]);
			}
			spawnMonsterGroup(applyFormation(formation, monsterIds));
		}

		public function onCharacterUpdate(dt:int):void
		{
			// Adapted from World#updateMonsters
			while ((!CH2.world.monsters.isNextMonsterBoss() || !CH2.user.isProgressionModeActive) && (CH2.world.monsters.calculateZoneIndexOfNextSpawnedMonster() != 0 || monsters.length == 0 || !monsters[monsters.length - 1].isAlive))
			{
				spawnGroupMonsters();
			}

			var index:uint;
			var monster:Monster;
			for (index = 0; index < monsters.length; index++)
			{
				monster = monsters[index];
				if (monster && monster.isAlive)
				{
					//monster.display.monsterAnimation.rotation += (1 / dt); // rotates on 2d canvas like a clock
				}
			}
		}
	}
}