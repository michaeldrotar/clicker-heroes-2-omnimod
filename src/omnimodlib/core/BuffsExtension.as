package omnimodlib.core
{
	import models.Buff;
	import omnimodlib.Extension;

	public class BuffsExtension extends Extension
	{
		public function BuffsExtension()
		{
			super();
		}

		public function createBuff(id:String):Buff
		{
			var buff:Buff = new Buff();
			buff.name = id;

			// these properties should normally be set as needed
			buff.iconId = 0;
			buff.tooltipFunction = null;

			buff.duration = 0;
			buff.isUntimedBuff = true;

			buff.tickRate = 100;
			buff.unhastened = false;
			buff.tickFunction = null;

			// these properties are largely for metadata
			buff.stacks = 1;
			buff.stateValues = {};

			// these properties are less common functions
			buff.startFunction = null; // when the buff is added
			buff.clickFunction = null; // when the buff is clicked, not like a click attack
			buff.finishFunction = null; // when the buff finishes or expires
			buff.attackFunction = null;
			buff.critFunction = null;
			buff.skillUseFunction = null;
			buff.monsterSpawnFunction = null;
			buff.monsterHitFunction = null;
			buff.killFunction = null;
			buff.onBossBattleEndedFunction = null;
			buff.onGoldGainedFunction = null;

			// these properties are more obscure
			buff.id = NaN; // essentially unused
			buff.maximumStacks = 1; // unused
			buff.castFunction = null; // unused

			return buff;
		}

		public function getCurrentBuff(name:String):Buff
		{
			return currentCharacter.buffs.getBuff(name);
		}
	}
}