package omnimodlib.core
{
	import omnimodlib.Extension;

	public class OmniSkillPoints extends Extension
	{
		public function OmniSkillPoints()
		{
			super();
		}

		public function get availableSkillPoints():Number
		{
			return this.totalSkillPoints - this.spentSkillPoints;
		}

		public function get spentSkillPoints():Number
		{
			return this.data.getNumber('SPENT_SKILL_POINTS') || 0;
		}

		public function set spentSkillPoints(value:Number):void
		{
			this.data.setNumber('SPENT_SKILL_POINTS', value);
		}

		public function get totalSkillPoints():Number
		{
			return this.data.getNumber('TOTAL_SKILL_POINTS') || 0;
		}

		public function set totalSkillPoints(value:Number):void
		{
			this.data.setNumber('TOTAL_SKILL_POINTS', value);
		}
	}
}
