package omnimodlib.core
{
	import heroclickerlib.CH2;
	import models.Monster;
	import omnimodlib.Extension;
	import omnimodlib.Util;

	public class MonstersExtension extends Extension
	{
		public function MonstersExtension()
		{
			super();
		}

		public function getMonsters():Array
		{
			var monsters:Array = Util.toArray(CH2.world.monsters.monsters);
			return monsters;
		}

		public function getNextMonster():Monster
		{
			return CH2.world.getNextMonster();
		}
	}
}