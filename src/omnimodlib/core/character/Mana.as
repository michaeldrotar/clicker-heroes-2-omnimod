package omnimodlib.core.character
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import models.Buff;
	import models.Character;
	import omnimodlib.Extension;
	import ui.CH2UI;

	public class Mana extends Extension
	{
		private var _remainder:Number = 0;

		public function Mana()
		{
			super();
		}

		public function onCharacterCreated():void
		{
			currentCharacter.addManaHandler = this;
		}

		public function addManaOverride(amount:Number, showFloatingText:Boolean = true):void
		{
			addMana(amount, showFloatingText);
		}

		public function addMana(amount:Number, showFloatingText:Boolean = true, allowOverflow:Boolean = false):void
		{
			_helpfulAdventurerOverride(amount, showFloatingText);

			var total:Number = amount + _remainder;
			amount = Math.floor(total);
			_remainder = total - amount;

			var startingMana:Number = currentCharacter.mana;
			var maxMana:Number = currentCharacter.maxMana.numberValue();

			// Maintain bug from core game since it lets clickables overflow until they can be modded
			if (startingMana <= maxMana)
			{
				allowOverflow = true;
			}

			if (amount > 0 && !allowOverflow)
			{
				amount = Math.min(amount, Math.max(maxMana - startingMana, 0));
			}

			currentCharacter.mana = Math.max(startingMana + amount, 0);

			if (amount < 0)
			{
				currentCharacter.logManaUsed(new BigNumber(amount * -1));
			}

			if (currentCharacter.mana !== startingMana && showFloatingText)
			{
				if (currentCharacter.mana > startingMana)
				{
					CH2UI.instance.mainUI.hud.playManaGainedEffect();
				}
				CH2UI.instance.mainUI.hud.showManaUsed(currentCharacter.mana - startingMana);
			}
		}

		public function _helpfulAdventurerOverride(amount:Number, showFloatingText:Boolean = true):void
		{
			var character:Character = CH2.currentCharacter;
			if (amount < 0)
			{
				if (character.getTrait("SpendManaHaste"))
				{
					if (character.buffs.hasBuffByName("SpendManaHaste"))
					{
						var existingBuff:Buff = character.buffs.getBuff("SpendManaHaste");
						var remainingValue:Number = (existingBuff.getStatValue(CH2.STAT_HASTE) - 1) * (existingBuff.duration - existingBuff.timeSinceActivated) / 5000;
						existingBuff.buffStat(CH2.STAT_HASTE, 1 + (-amount / 100) + remainingValue);
						existingBuff.timeSinceActivated = 0;
					}
					else
					{
						var buff:Buff = new Buff();
						buff.name = "SpendManaHaste";
						buff.iconId = 23;
						buff.isUntimedBuff = false;
						buff.duration = 5000;
						buff.unhastened = true;
						buff.tooltipFunction = function():Object
						{
							return {"header": "Gift of Chronos", "body": "Increases haste by " + (buff.getStatValue(CH2.STAT_HASTE) * 100).toFixed(2) + "%."};
						}
						buff.buffStat(CH2.STAT_HASTE, 1 + (-amount / 100));
						character.buffs.addBuff(buff);
					}
				}
			}
		}
	}
}