package omnimodlib.core.character
{
	import com.playsaurus.numbers.BigNumber;
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Character;
	import models.Monster;
	import omnimodlib.Extension;
	import ui.CH2UI;

	public class Energy extends Extension
	{
		private var _remainder:Number = 0;

		public function Energy()
		{
			super();
		}

		public function onCharacterCreated():void
		{
			currentCharacter.addEnergyHandler = this;
		}

		public function addEnergyOverride(amount:Number, showFloatingText:Boolean = true):void
		{
			addEnergy(amount, showFloatingText);
		}

		public function addEnergy(amount:Number, showFloatingText:Boolean = true, allowOverflow:Boolean = false):void
		{
			_helpfulAdventurerOverride(amount, showFloatingText);

			var total:Number = amount + _remainder;
			amount = Math.floor(total);
			_remainder = total - amount;

			var startingEnergy:Number = currentCharacter.energy;
			var maxEnergy:Number = currentCharacter.maxEnergy.numberValue();

			// Maintain bug from core game since it lets clickables overflow until they can be modded
			if (startingEnergy <= maxEnergy)
			{
				allowOverflow = true;
			}

			if (amount > 0 && !allowOverflow)
			{
				amount = Math.min(amount, Math.max(maxEnergy - startingEnergy, 0));
			}

			currentCharacter.energy = Math.max(startingEnergy + amount, 0);

			if (amount < 0)
			{
				currentCharacter.logEnergyUsed(new BigNumber(amount * -1));
			}

			if (currentCharacter.energy === 0)
			{
				currentCharacter.timeOfLastOutOfEnergy = CH2.user.totalMsecsPlayed;
			}

			if (currentCharacter.energy !== startingEnergy && showFloatingText)
			{
				if (currentCharacter.energy > startingEnergy)
				{
					CH2UI.instance.mainUI.hud.playEnergyGainedEffect();
				}
				CH2UI.instance.mainUI.hud.showEnergyUsed(currentCharacter.energy - startingEnergy);
			}
		}

		private function _helpfulAdventurerOverride(amount:Number, showFloatingText:Boolean = true):void
		{
			var character:Character = CH2.currentCharacter;
			if (amount < 0)
			{
				if (character.getTrait("Discharge"))
				{
					var target:Monster = CH2.world.getNextMonster();
					if (target && (Math.abs(target.y - CH2.currentCharacter.y) < 200))
					{
						var attackData:AttackData = new AttackData();
						attackData.damage = character.clickDamage.multiplyN(Math.abs(amount));
						attackData.isCritical = false;
						attackData.monster = target;
						target.takeDamage(attackData);
						attackData.isClickAttack = true;
						character.buffs.onAttack(attackData);
					}
				}
			}
		}
	}
}