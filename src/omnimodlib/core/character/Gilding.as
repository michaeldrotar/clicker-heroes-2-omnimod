package omnimodlib.core.character
{
	import heroclickerlib.LevelGraphNode;
	import omnimodlib.Extension;
	import omnimodlib.GildTalentExtension;
	import ui.CH2UI;

	public class Gilding extends Extension
	{
		public static const TIER_GILDS_REQUIRED:Array = [1, 3, 5, 10, 15, 20, 30];

		public var gildTalents:Array;

		private var _gildNoticeShown:Boolean = false;
		private var _gildNoticeConfirmed:Boolean = false;
		private var _timeSinceGildNoticeShown:int = 0;

		public function Gilding()
		{
			super();

			gildTalents = [];
		}

		public function get gildingAvailable():Boolean
		{
			return currentCharacter.highestWorldCompleted === highestWorldInCurrentGild;
		}

		public function get gilds():Number
		{
			return currentCharacter.gilds;
		}

		public function get highestWorldInCurrentGild():Number
		{
			return (gilds + 1) * currentCharacter.worldsPerGild;
		}

		public function get talentsAvailable():Boolean
		{
			var tier:uint;
			var talents:Array;
			var talent:GildTalentExtension;
			for (tier = 0; tier < TIER_GILDS_REQUIRED.length; tier++)
			{
				talents = getTalentsForTier(tier);
				if (talents.length > 0)
				{
					talent = getActiveTalentForTier(tier);
					if (!talent)
					{
						return true;
					}
				}
			}
			return false;
		}

		public function getActiveTalentForTier(tier:uint):GildTalentExtension
		{
			var index:uint;
			var talent:GildTalentExtension;
			var talents:Array = getTalentsForTier(tier);
			for (index = 0; index < talents.length; index++)
			{
				talent = talents[index];
				if (talent.active)
				{
					return talent;
				}
			}
			return null;
		}

		public function getRequiredLevelForTier(tier:uint):uint
		{
			if (tier >= TIER_GILDS_REQUIRED.length)
			{
				return TIER_GILDS_REQUIRED[TIER_GILDS_REQUIRED.length - 1];
			}
			return TIER_GILDS_REQUIRED[tier];
		}

		public function getTalentsForTier(tier:uint):Array
		{
			var talents:Array = [];

			var index:uint;
			var talent:GildTalentExtension;
			for (index = 0; index < gildTalents.length; index++)
			{
				talent = gildTalents[index];
				if (talent.tier === tier)
				{
					talents.push(talent);
				}
			}

			return talents;
		}

		public function learnStarterSkills():void
		{
			var index:uint;
			var node:LevelGraphNode;
			if (currentCharacter.name === 'Helpful Adventurer')
			{
				for (index = 0; index < 5; index++)
				{
					node = extensions.levelGraph.getNodesByProperty('canBePurchased', true)[0];
					currentCharacter.levelGraph.purchaseNode(node.id);
				}
			}
		}

		public function resetTalents():void
		{
			var index:uint;
			var talent:GildTalentExtension;
			for (index = 0; index < gildTalents.length; index++)
			{
				talent = gildTalents[index];
				talent.active = false;
			}
		}

		public function setActiveTalentForTier(tier:uint, talentToActivate:GildTalentExtension):void
		{
			var index:uint;
			var talent:GildTalentExtension;
			var talents:Array = getTalentsForTier(tier);
			for (index = 0; index < talents.length; index++)
			{
				talent = talents[index];
				talent.active = talent === talentToActivate
			}
		}

		private function _addGildOption(gildOption:Object):void
		{
			gildOption['isCharacterSpecific'] ||= false;
			gildTalents.push(gildOption);
		}

		private function _initOptions():void
		{
			gildTalents = [];
			trigger('onPopulateGildTalents', gildTalents);
		}

		public function onCharacterCreated():void
		{
			currentCharacter.addGildHandler = this;
			currentCharacter.onWorldStartedHandler = this;
		}

		public function onCharacterStarted():void
		{
			_initOptions();
			_gildNoticeShown = false;
			_gildNoticeConfirmed = false;
		}

		public function onCharacterUpdate(dt:int):void
		{
			if (_gildNoticeShown && !_gildNoticeConfirmed)
			{
				_timeSinceGildNoticeShown += dt;
				if (_timeSinceGildNoticeShown > 2000)
				{
					_gildNoticeShown = false;
				}
			}

			if (gilds > 0 || currentCharacter.currentWorldId !== highestWorldInCurrentGild || !gildingAvailable || _gildNoticeShown)
			{
				return;
			}

			var header:String = 'Congratulations!';
			var body:String = '';
			body += 'You\'ve unlocked your first gild!\n';
			body += 'Gilding brings new challenges and new game features.\n';
			body += 'Visit the World Panel to learn more.';
			CH2UI.instance.showSimpleGamePopup(header, body, function():void
			{
				_gildNoticeConfirmed = true;
			}, 'OK');
			_gildNoticeConfirmed = false;
			_gildNoticeShown = true;
			_timeSinceGildNoticeShown = 0;
		}

		public function addGildOverride(worldId:Number):void
		{
			trigger('onCharacterGilding');

			trigger('onDataBackup');
			currentCharacter.addGildDefault(worldId);
			trigger('onDataRestore');

			// resetTalents();
			// learnStarterSkills();
			trigger('onCharacterGilded');
		}

		public function onWorldStartedOverride(worldNumber:Number):void
		{
			_gildNoticeConfirmed = false; // re-confirm each time last world is defeated
			currentCharacter.onWorldStartedDefault(worldNumber);
		}
	}
}
