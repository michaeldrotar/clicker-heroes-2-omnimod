package omnimodlib.core.character
{
	import heroclickerlib.LevelGraphNode;
	import omnimodlib.Extension;
	import ui.CH2UI;

	public class LevelGraphExtension extends Extension
	{
		public function LevelGraphExtension()
		{
			super();
		}

		public function get nodes():Array
		{
			return currentCharacter.levelGraph.nodes;
		}

		public function filterNodesByProperty(nodes:Array, propertyName:String, value:*):Array
		{
			var filteredNodes:Array = [];

			var nodesPurchased:Object = currentCharacter.nodesPurchased;
			var levelGraphNodeTypes:Object = currentCharacter.levelGraphNodeTypes;

			var index:uint;
			var node:LevelGraphNode;
			var nodeType:Object;

			for (index = 0; index < nodes.length; index++)
			{
				node = nodes[index];

				if (!node)
				{
					continue;
				}

				if (propertyName === 'purchased')
				{
					if (nodesPurchased[node.id] == value)
					{
						filteredNodes.push(node);
					}
				}
				else if (propertyName === 'canBePurchased')
				{
					if (node.canBePurchased() === value)
					{
						filteredNodes.push(node);
					}
				}
				else if (node.hasOwnProperty(propertyName))
				{
					if (node[propertyName] === value)
					{
						filteredNodes.push(node);
					}
				}
				else
				{
					nodeType = levelGraphNodeTypes[node.type];
					if (nodeType && nodeType[propertyName] === value)
					{
						filteredNodes.push(node);
					}
				}
			}

			return filteredNodes;
		}

		public function filterNodesByProperties(nodes:Array, properties:Object):Array
		{
			var propertyName:String;
			for (propertyName in properties)
			{
				nodes = filterNodesByProperty(nodes, propertyName, properties[propertyName]);
			}

			return nodes;
		}

		public function getNodesByProperty(propertyName:String, value:*):Array
		{
			return filterNodesByProperty(nodes, propertyName, value);
		}

		public function getNodesByProperties(properties:Object):Array
		{
			return filterNodesByProperties(nodes, properties);
		}

		public function refreshDisplay():void
		{
			// Copied from Character#addExperience, must always be called cause some places don't properly refresh it currently
			if (CH2UI.instance.mainUI)
			{
				CH2UI.instance.mainUI.mainPanel.graphPanel.redrawGraph();
				if (CH2UI.instance.mainUI.mainPanel.isOnGraphPanel)
				{
					CH2UI.instance.mainUI.mainPanel.graphPanel.updateInteractiveLayer();
				}
			}
		}
	}
}