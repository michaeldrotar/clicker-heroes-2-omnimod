package omnimodlib.core.character
{
	import com.playsaurus.numbers.BigNumber;
	import omnimodlib.Extension;

	public class SkillPoints extends Extension
	{
		public function SkillPoints()
		{
			super();
		}

		public function get availableSkillPoints():BigNumber
		{
			return currentCharacter.availableStatPoints
		}

		public function get hasAvailableSkillPoints():Boolean
		{
			return currentCharacter.hasAvailableStatPoints;
		}

		public function get spentSkillPoints():BigNumber
		{
			return currentCharacter.spentStatPoints;
		}

		public function get totalSkillPoints():BigNumber
		{
			return currentCharacter.totalStatPoints;
		}

		public function set totalSkillPoints(value:BigNumber):void
		{
			_triggerStatPointConversion();

			currentCharacter.totalStatPointsV2 = value.numberValue();

			// used by tutorial for characters under level 8, doesn't seem important but maintaining it
			currentCharacter.hasNewSkillTreePointsAvailable = hasAvailableSkillPoints;

			extensions.levelGraph.refreshDisplay();
		}

		private function _triggerStatPointConversion():void
		{
			currentCharacter.totalStatPoints; // getter method triggers it
		}
	}
}