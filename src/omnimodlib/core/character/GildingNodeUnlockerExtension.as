package omnimodlib.core.character
{
	import heroclickerlib.LevelGraphNode;
	import omnimodlib.Extension;

	public class GildingNodeUnlockerExtension extends Extension
	{
		public function GildingNodeUnlockerExtension()
		{
			super();
		}

		public function get unlockedNodeIds():Array
		{
			if (!data.hasKey([uuid, 'unlockedNodeIds']))
			{
				data.setArray([uuid, 'unlockedNodeIds'], []);
			}
			return data.getArray([uuid, 'unlockedNodeIds']);
		}

		public function set unlockedNodeIds(value:Array):void
		{
			data.setArray([uuid, 'unlockedNodeIds'], value);
		}

		public function saveUnlockedNodes():void
		{
			var deluxeNodes:Array = extensions.levelGraph.getNodesByProperties({purchased: true, deluxe: true});

			var index:uint;
			var node:LevelGraphNode;
			for (index = 0; index < deluxeNodes.length; index++)
			{
				node = deluxeNodes[index];
				if (unlockedNodeIds.indexOf(node.id) === -1)
				{
					unlockedNodeIds.push(node.id);
				}
			}
		}

		public function unlockNodes():void
		{
			var deluxeNodes:Array = extensions.levelGraph.getNodesByProperties({deluxe: true});

			var index:uint;
			var node:LevelGraphNode;
			for (index = 0; index < deluxeNodes.length; index++)
			{
				node = deluxeNodes[index];
				node.alwaysAvailable = (unlockedNodeIds.indexOf(node.id) !== -1);
			}

			extensions.levelGraph.refreshDisplay();
		}

		public function onCharacterStarted():void
		{
			unlockNodes();
		}

		public function onCharacterGilding():void
		{
			saveUnlockedNodes();
		}

		public function onCharacterGilded():void
		{
			unlockNodes();
		}
	}
}