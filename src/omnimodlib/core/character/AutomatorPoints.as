package omnimodlib.core.character
{
	import omnimodlib.Extension;

	public class AutomatorPoints extends Extension
	{
		public function AutomatorPoints()
		{
			super();
		}

		public function get availableAutomatorPoints():int
		{
			return currentCharacter.automatorPoints;
		}

		public function set availableAutomatorPoints(value:int):void
		{
			currentCharacter.automatorPoints = value;

			extensions.levelGraph.refreshDisplay();
		}

		public function get hasAvailableAutomatorPoints():Boolean
		{
			return availableAutomatorPoints > 0;
		}

		public function get spentAutomatorPoints():int
		{
			return extensions.levelGraph.getNodesByProperties({purchased: true, costsAutomatorPoint: true}).length;
		}

		public function get totalAutomatorPoints():int
		{
			return spentAutomatorPoints + availableAutomatorPoints;
		}
	}
}