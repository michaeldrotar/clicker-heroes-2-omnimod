package omnimodlib.core
{
	import omnimodlib.Extension;

	public class OmniStatPoints extends Extension
	{
		public function OmniStatPoints()
		{
			super();
		}

		public function get availableStatPoints():Number
		{
			return this.totalStatPoints - this.spentStatPoints;
		}

		public function get spentStatPoints():Number
		{
			return this.data.getNumber('SPENT_STAT_POINTS') || 0;
		}

		public function set spentStatPoints(value:Number):void
		{
			this.data.setNumber('SPENT_STAT_POINTS', value);
		}

		public function get totalStatPoints():Number
		{
			return this.data.getNumber('TOTAL_STAT_POINTS') || 0;
		}

		public function set totalStatPoints(value:Number):void
		{
			this.data.setNumber('TOTAL_STAT_POINTS', value);
		}
	}
}