package omnimodlib.core
{
	import heroclickerlib.LevelGraphNode;
	import omnimodlib.Extension;
	import ui.CH2UI;

	public class LevelGraphConnectionsOverrideExtension extends Extension
	{
		public var connections:Object;

		public function LevelGraphConnectionsOverrideExtension()
		{
			super();
		}

		public function tryAddConnections():void
		{
			if (!connections || currentCharacter.gilds < 1)
			{
				return;
			}

			var nodes:Array;
			var startNode:LevelGraphNode;
			var startName:String;
			var endNode:LevelGraphNode;
			var endName:String;
			var endNames:Array;
			var index:uint;

			var edges:Array = currentCharacter.levelGraph.edges;

			for (startName in connections)
			{
				nodes = extensions.levelGraph.getNodesByProperty('name', startName);
				startNode = nodes[0];
				if (!startNode)
				{
					continue;
				}

				endNames = connections[startName];
				for (index = 0; index < endNames.length; index++)
				{
					endName = endNames[index];
					nodes = extensions.levelGraph.getNodesByProperty('name', endName);
					endNode = nodes[0];
					if (!endNode)
					{
						continue;
					}

					if (startNode.connections.indexOf(endNode.id) !== -1)
					{
						continue;
					}

					startNode.connections.push(endNode.id);
					endNode.connections.push(startNode.id);
					edges.push([startNode.x, startNode.y, endNode.x, endNode.y, startNode.id, endNode.id]);
				}
			}

			extensions.levelGraph.refreshDisplay();
		}

		public function onCharacterStarted():void
		{
			connections = null;

			if (currentCharacter.name === 'Helpful Adventurer')
			{
				connections = {};

				connections['Gem: Energize.'] = ['Gem: AutoAttack Storm', 'Gem: Critstorm'];
				connections['Gem: AutoAttack Storm'] = ['Stone: Big Clicks is not active.'];
				connections['Gem: Critstorm'] = ['Stone: Energy more than 90%'];

				connections['Gem: Huge Click'] = ['Gem: Golden Clicks', 'Gem: ClickTorrent'];
				connections['Gem: Golden Clicks'] = ['Gem: Upgrade Cheapest Item'];
				connections['Gem: Upgrade Cheapest Item'] = ['Stone: Huge Click is not active.'];
			}

			tryAddConnections();
		}

		public function onCharacterGilded():void
		{
			tryAddConnections();
		}
	}
}