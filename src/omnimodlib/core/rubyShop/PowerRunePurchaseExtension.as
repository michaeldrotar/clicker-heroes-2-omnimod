package omnimodlib.core.rubyShop
{
	import models.Character;
	import omnimodlib.RubyPurchaseExtension;

	public class PowerRunePurchaseExtension extends RubyPurchaseExtension
	{
		public function PowerRunePurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Power Rune');
			priority = 20;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}