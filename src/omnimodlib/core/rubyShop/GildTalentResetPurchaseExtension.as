package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class GildTalentResetPurchaseExtension extends RubyPurchaseExtension
	{
		public function GildTalentResetPurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Automator Point');
			name = 'Gild Talent Reset';
			priority = 2;
			price = 100;
			canAppear = function():Boolean
			{
				return extensions.gilding.gilds > 0;
			};
			canPurchase = canPurchaseOncePerVisit;

			getDescription = function():String
			{
				return _('Reset gild talents.');
			};

			onPurchase = function():void
			{
				extensions.gilding.resetTalents();
			};

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}
