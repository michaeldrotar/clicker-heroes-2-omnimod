package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class SkillPointPurchase extends RubyPurchaseExtension
	{
		public function SkillPointPurchase()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Automator Point');
			name = 'Skill Point';
			iconId = 2;
			priority = 4;
			canAppear = canAppearAlways;
			canPurchase = canPurchaseOncePerVisit;

			getDescription = function():String
			{
				return _('Gain 1 skill point.');
			};

			onPurchase = function():void
			{
				extensions.skillPoints.totalSkillPoints = extensions.skillPoints.totalSkillPoints.addN(1);
			};

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}
