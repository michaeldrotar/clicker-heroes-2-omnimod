package omnimodlib.core.rubyShop
{
	import models.Character;
	import omnimodlib.RubyPurchaseExtension;

	public class EnergyDrinkPurchase extends RubyPurchaseExtension
	{
		public function EnergyDrinkPurchase()
		{
			super();
		}

		public function get energyDrinkAmount():Number
		{
			return Character.MAGICAL_BREW_MANA_AMOUNT;
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Magical Brew');
			name = 'Energy Drink';
			priority = 10;
			canAppear = canAppearWithQuantity;
			canPurchase = canPurchaseWithQuantity;

			getDescription = function():String
			{
				return _('Gain {energyDrinkAmount} energy, may overflow energy pool.');
			};

			onPurchase = function():void
			{
				extensions.energy.addEnergy(energyDrinkAmount, true, true);
			};

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}

		override public function onRubyShopGenerating():void
		{
			super.onRubyShopGenerating();

			maxQuantity = Math.ceil(currentCharacter.maxEnergy.numberValue() / energyDrinkAmount);
		}
	}
}