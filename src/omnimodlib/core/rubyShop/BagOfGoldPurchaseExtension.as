package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class BagOfGoldPurchaseExtension extends RubyPurchaseExtension
	{
		public function BagOfGoldPurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Bag of Gold');
			priority = 10;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}