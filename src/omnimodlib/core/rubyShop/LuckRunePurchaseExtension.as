package omnimodlib.core.rubyShop
{
	import models.Character;
	import omnimodlib.RubyPurchaseExtension;

	public class LuckRunePurchaseExtension extends RubyPurchaseExtension
	{
		public function LuckRunePurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Luck Rune');
			priority = 20;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}