package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class TimeMetalDetectorPurchaseExtension extends RubyPurchaseExtension
	{
		public function TimeMetalDetectorPurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Metal Detector (Time)');
			priority = 10;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}