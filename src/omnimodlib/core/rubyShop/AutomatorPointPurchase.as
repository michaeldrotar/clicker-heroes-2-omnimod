package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class AutomatorPointPurchase extends RubyPurchaseExtension
	{
		public function AutomatorPointPurchase()
		{
			super();
		}

		public function get purchasedAutomatorPoints():Number
		{
			return (extensions.automatorPoints.spentAutomatorPoints + extensions.automatorPoints.availableAutomatorPoints) - unlockedAutomatorPoints;
		}

		public function get unlockedAutomatorPoints():Number
		{
			return extensions.levelGraph.getNodesByProperties({'purchased': true, 'name': 'Automator Point'}).length;
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Automator Point');
			priority = 4;
			canAppear = canAppearAlways;
			canPurchase = canPurchaseOncePerVisit;

			onPurchase = function():void
			{
				extensions.automatorPoints.availableAutomatorPoints += 1;
				currentCharacter.timeSinceLastAutomatorPointPurchase = 0;
			};

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}