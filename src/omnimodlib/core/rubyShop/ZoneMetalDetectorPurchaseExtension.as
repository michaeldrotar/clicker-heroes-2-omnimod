package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class ZoneMetalDetectorPurchaseExtension extends RubyPurchaseExtension
	{
		public function ZoneMetalDetectorPurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Metal Detector (Zone)');
			priority = 10;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}