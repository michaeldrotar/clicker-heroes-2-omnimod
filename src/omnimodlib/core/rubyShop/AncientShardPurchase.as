package omnimodlib.core.rubyShop
{
	import omnimodlib.RubyPurchaseExtension;

	public class AncientShardPurchase extends RubyPurchaseExtension
	{
		public function AncientShardPurchase()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Ancient Shard');
			priority = 2;
			price = 100;
			canAppear = canAppearAlways;
			canPurchase = canPurchaseOncePerVisit;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}