package omnimodlib.core.rubyShop
{
	import models.Character;
	import omnimodlib.RubyPurchaseExtension;

	public class MagicalBrewPurchase extends RubyPurchaseExtension
	{
		public function MagicalBrewPurchase()
		{
			super();
		}

		public function get magicalBrewAmount():Number
		{
			return Character.MAGICAL_BREW_MANA_AMOUNT;
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Magical Brew');
			priority = 10;
			canAppear = canAppearWithQuantity;
			canPurchase = canPurchaseWithQuantity;

			getDescription = function():String
			{
				return _('Gain {magicalBrewAmount} mana, may overflow mana pool.');
			};

			onPurchase = function():void
			{
				extensions.mana.addMana(magicalBrewAmount, true, true);
			};

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}

		override public function onRubyShopGenerating():void
		{
			super.onRubyShopGenerating();

			maxQuantity = Math.ceil(currentCharacter.maxMana.numberValue() / magicalBrewAmount);
		}
	}
}