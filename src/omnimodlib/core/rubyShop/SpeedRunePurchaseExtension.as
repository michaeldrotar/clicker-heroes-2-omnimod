package omnimodlib.core.rubyShop
{
	import models.Character;
	import omnimodlib.RubyPurchaseExtension;

	public class SpeedRunePurchaseExtension extends RubyPurchaseExtension
	{
		public function SpeedRunePurchaseExtension()
		{
			super();
		}

		override public function onPopulateRubyPurchaseOptions(rubyPurchaseOptions:Array):void
		{
			copyPurchaseOption('Speed Rune');
			priority = 20;

			super.onPopulateRubyPurchaseOptions(rubyPurchaseOptions);
		}
	}
}