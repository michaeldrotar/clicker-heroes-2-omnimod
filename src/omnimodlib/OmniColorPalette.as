package omnimodlib
{

	public class OmniColorPalette
	{
		public const white:Number = 0xffffff;
    public const lightest:Number = 0xeeeeee;
    public const lighter:Number = 0xcccccc;
    public const light:Number = 0xaaaaaa;
    public const dark:Number = 0x999999;
    public const darker:Number = 0x666666;
    public const darkest:Number = 0x333333;
		public const black:Number = 0x000000;

		// https://coolors.co/ef8b9a-f2b99f-cde8b7-a6b4d8-c18df4
		public const red:Number = 0xef8b9a;
		public const orange:Number = 0xf3bba1;
		public const yellow:Number = 0xf2e39f;
		public const green:Number = 0xcce7b6;
		public const blue:Number = 0xa6b3d8;
		public const purple:Number = 0xc18df4;

		public const damage:Number = red;
		public const energy:Number = yellow; // 0xffff00;
		public const energyPerAutoAttack:Number = yellow;
		public const energyPerClick:Number = yellow;
		public const energyPerSecond:Number = yellow;
		public const label:Number = 0xcccccc; // 0xffd098;
		public const mana:Number = blue; // 0x01a6ce;
		public const manaPerAutoAttack:Number = blue;
		public const manaPerClick:Number = blue;
		public const manaPerSecond:Number = blue;
		public const value:Number = green;
		public const warning:Number = red;

		public const enabledBackground:Number = 0xffffff;
		public const enabledBorder:Number = 0x333333;
		public const enabledText:Number = 0x333333;

		public const disabledBackground:Number = 0xeeeeee;
		public const disabledBorder:Number = 0xdddddd;
		public const disabledText:Number = 0xafafaf;

		public const buttonBorder:Number = 0xffde46;

		private const _colorStrings:Object = {};
		private static var _instance:OmniColorPalette;

		public function OmniColorPalette()
		{

		}

		public static function get instance():OmniColorPalette
		{
			_instance ||= new OmniColorPalette();
			return _instance;
		}

		public function getColorString(name:String):String
		{
			_colorStrings[name] ||= '#' + (this[name] as Number).toString(16);
			return _colorStrings[name];
		}

		public function hasColor(name:String):Boolean
		{
			return (name in this) && (this[name] is Number);
		}
	}
}