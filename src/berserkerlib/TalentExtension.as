package berserkerlib
{

	public class TalentExtension extends NodeTypeExtension
	{
		public function TalentExtension()
		{
			super();

			icon = 'damagex3';
			quality = QUALITY_DELUXE;
		}

		public function get enabled():Boolean
		{
			return data.getBoolean(id) || false;
		}

		public function set enabled(value:Boolean):void
		{
			data.setBoolean(id, value);
		}

		public function purchase():void
		{
			enabled = true
		}
	}
}