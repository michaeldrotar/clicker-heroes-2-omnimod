package berserkerlib
{
	import omnimodlib.Extension;
	import omnimodlib.Util;

	public class MonsterExtension extends Extension
	{
		public var attackRate:int = 5 * 1000;
		public var attackRange:int = Util.MELEE_RANGE;

		public function MonsterExtension()
		{

		}

		public function attack()
		{

		}
	}
}