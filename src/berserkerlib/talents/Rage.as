package berserkerlib.talents
{
	import berserkerlib.TalentExtension;
	import berserkerlib.Tooltip;
	import models.Buff;

	public class Rage extends TalentExtension
	{
		public var energy:Number = 10;
		public var duration:Number = 1000;

		public function Rage()
		{
			super();
			name = 'Rage';
			description = 'Bash now generates an additional { energy | energy } energy over { duration | duration | value }.';
		}

		public function bashEffectHook():void
		{
			if (enabled)
			{
				var buff:Buff = new Buff();
				buff.name = name;
				buff.iconId = 35;
				buff.duration = duration;
				buff.tickRate = duration / energy;
				buff.unhastened = true;
				buff.tickFunction = function():void
				{
					character.addEnergy(1, false);
				};
				character.buffs.addBuff(buff);
			}
			next();
		}

		public function bashDescriptionHook():Array
		{
			var talents:Array = next();
			if (enabled)
			{
				talents.push(Tooltip.format('\n{ "Rage:" | label } Generates an additional { energy | energy } energy over { duration | duration | value }.', this));
			}
			return talents;
		}

		public function onExtensionsCreated():void
		{
			hook(extensions.bash, 'effect', bashEffectHook);
			hook(extensions.bash, 'getTalentDescriptions', bashDescriptionHook);
		}
	}
}
