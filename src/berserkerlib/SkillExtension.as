package berserkerlib
{
	import omnimodlib.Extension;
	import heroclickerlib.CH2;
	import models.Character;
	import models.Monster;
	import models.Skill;

	public class SkillExtension extends Extension
	{
		public var name:String = '';
		public var description:String = '';
		public var iconId:Number = 0;

		public var castTime:Number = 0;
		public var cooldown:Number = 0;
		public var ignoresGCD:Boolean = false;
		public var energyCost:Number = 0;
		public var manaCost:Number = 0;
		public var minimumRange:Number = 0;
		public var maximumRange:Number = 0;

		public var learnAtLevel:Number = 0;
		public var dashesOnUse:Boolean = true;
		public var effect:Function;

		public var getCostDescriptions:Function;
		public var getTalentDescriptions:Function;

		public function SkillExtension()
		{
			effect = this['effectFunction'];

			getCostDescriptions = getCostDescriptionsDefault;
			getTalentDescriptions = getTalentDescriptionsDefault;
		}

		public function get readyToLearn():Boolean
		{
			return learnAtLevel > 0 && character.level >= learnAtLevel;
		}

		public function get nextMonster():Monster
		{
			return CH2.world.getNextMonster();
		}

		public function assignToSkill(skill:Skill):void
		{
			skill.castTime = castTime;
			skill.cooldown = cooldown;
			skill.ignoresGCD = ignoresGCD;
			skill.energyCost = energyCost;
			skill.manaCost = manaCost;
			skill.minimumRange = minimumRange;
			skill.maximumRange = maximumRange;
		}

		public function dashTo(target:Monster):void
		{
			var currentY:Number = character.y;
			var targetY:Number = target.y - character.attackRange;
			if (currentY < targetY)
			{
				character.y = targetY;
				character.changeState(Character.STATE_COMBAT);
				character.characterDisplay.playDash(character.y - currentY);
			}
		}

		public function getCooldownCostDescription():String
		{
			if (cooldown > 0)
			{
				return '{ "Cooldown:" | label } { cooldown | duration }';
			}
			return null;
		}

		public function getCostDescriptionsDefault():Array
		{
			var costs:Array = [];
			var energy:String = getEnergyCostDescription();
			var mana:String = getManaCostDescription();
			var cooldown:String = getCooldownCostDescription();
			if (energy)
			{
				costs.push(energy);
			}
			if (mana)
			{
				costs.push(mana);
			}
			if (cooldown)
			{
				costs.push(cooldown);
			}
			return costs;
		}

		public function getEnergyCostDescription():String
		{
			if (energyCost < 0)
			{
				return '{ "Generate:" | label } { energyCost | invert | energy } energy';
			}
			if (energyCost > 0)
			{
				return '{ "Cost:" | label } { energyCost | energy } energy';
			}
			return null;
		}

		public function getManaCostDescription():String
		{
			if (manaCost < 0)
			{
				return '{ "Generate:" | label } { manaCost | invert | mana } mana';
			}
			if (manaCost > 0)
			{
				return '{ "Cost:" | label } { manaCost | mana } mana';
			}
			return null;
		}

		public function getSkill():Skill
		{
			return character.getSkill(id);
		}

		public function getTalentDescriptionsDefault():Array
		{
			return [];
		}

		public function getTooltip():Object
		{
			var costs:Array = getCostDescriptions();
			var talents:Array = getTalentDescriptions();

			var sections:Array = [];
			if (costs.length > 0)
			{
				sections.push(costs.join('\n'));
			}
			sections.push(description + '\n');
			if (talents.length > 0)
			{
				sections.push(talents.join('\n'));
			}

			var body:String = sections.join('\n\n');
			return {'header': name, 'body': Tooltip.format(body, this)};
		}

		public function learn():void
		{
			character.hasPurchasedFirstSkill = true;
			character.activateSkill(id);
		}

		public function learnIfReady():void
		{
			if (readyToLearn)
			{
				learn();
			}
		}

		public function refreshSkill():void
		{
			assignToSkill(getSkill());
		}

		public function toSkill():Skill
		{
			var skill:Skill = new Skill();
			skill.name = id;
			skill.iconId = iconId;

			assignToSkill(skill);

			skill.consumableOnly = false;
			skill.isConsumable = false;
			skill.minimumAscensions = 0;
			skill.usesMaxEnergy = false;
			skill.usesMaxMana = false;

			// TODO: Removed in 0.08e
			if ('useTutorialArrow' in skill)
			{
				skill['useTutorialArrow'] = false;
			}

			skill.effectFunction = function():void
			{
				if (dashesOnUse && nextMonster)
				{
					dashTo(nextMonster);
				}
				call('effect');
			};

			skill.tooltipFunction = function():Object
			{
				return getTooltip();
			};

			return skill;
		}

		public function onCharacterTemplateCreated(characterTemplate:Character):void
		{
			var skill:Skill = toSkill();
			Character.staticSkillInstances[skill.name] = skill;
		}

		public function onCharacterStarted():void
		{
			refreshSkill();
			learnIfReady();
		}

		public function onCharacterPropertyChanged(event:Object):void
		{
			if (event.property === 'level')
			{
				learnIfReady();
			}
		}

		public function onNodePurchased():void
		{
			refreshSkill();
		}
	}
}