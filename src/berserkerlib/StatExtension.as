package berserkerlib
{
	import omnimodlib.Extension;
	import omnimodlib.Util;

	public class StatExtension extends Extension
	{
		public var name:String = '';

		public function get amount():Number
		{
			return data.getNumber(id) || 0;
		}

		public function set amount(value:Number):void
		{
			data.setNumber(id, value);
			apply();
		}

		public function apply():void
		{

		}

		public function onCharacterStarted():void
		{
			apply();
		}

		public function onCharacterPropertyChanged(event:Object):void
		{
			if (event.property === 'level')
			{
				apply();
			}
		}
	}
}