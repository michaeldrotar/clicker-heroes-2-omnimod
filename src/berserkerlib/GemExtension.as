package berserkerlib
{
	import omnimodlib.Extension;
	import models.Automator;
	import models.AutomatorGem;
	import models.Character;

	public class GemExtension extends Extension
	{
		public var name:String = '';
		public var description:String = '';
		public var assetNumber:Number = 0;
		public var cooldown:Number = 0;

		public function get ready():Boolean
		{
			return true;
		}

		public function execute():void
		{

		}

		public function toGem():AutomatorGem
		{
			var gem:AutomatorGem = new AutomatorGem();
			gem.id = id;
			gem.assetName = 'BitmapHUD_gem' + assetNumber;
			gem.name = name;
			gem.description = Tooltip.format(description, this);
			gem.iconId = assetNumber; // seems to be unused
			gem.cooldownMs = cooldown;
			gem.onActivate = function():Boolean
			{
				if (ready)
				{
					execute();
					return true;
				}
				return false;
			};
			gem.canActivate = function():Boolean
			{
				return ready;
			}
			return gem;
		}

		public function onCharacterTemplateCreated(characterTemplate:Character):void
		{
			Automator.allGems[id] = toGem();
		}
	}
}