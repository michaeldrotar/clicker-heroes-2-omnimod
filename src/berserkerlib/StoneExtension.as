package berserkerlib
{
	import omnimodlib.Extension;
	import models.Automator;
	import models.AutomatorStone;
	import models.Character;

	public class StoneExtension extends Extension
	{
		public var name:String = '';
		public var description:String = '';
		public var cooldown:Number = 0;

		public function get ready():Boolean
		{
			return true;
		}

		public function toStone():AutomatorStone
		{
			var stone:AutomatorStone = new AutomatorStone();
			stone.id = id;
			stone.name = name;
			stone.label = ''; // unused??
			stone.description = Tooltip.format(description, this);
			stone.cooldownMs = cooldown;
			stone.onActivate = function():Boolean
			{
				return ready;
			};
			return stone;
		}

		public function onCharacterTemplateCreated(characterTemplate:Character):void
		{
			Automator.allStones[id] = toStone();
		}
	}
}