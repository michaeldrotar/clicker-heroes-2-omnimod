package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class LongestWaitStone extends StoneExtension
	{
		public function LongestWaitStone()
		{
			super();
			name = '60s Cooldown';
			description = 'description';
			cooldown = 60 * 1000;
		}
	}
}