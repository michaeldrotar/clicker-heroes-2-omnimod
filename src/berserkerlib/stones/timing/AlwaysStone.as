package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class AlwaysStone extends StoneExtension
	{
		public function AlwaysStone()
		{
			super();
			name = 'Always';
			description = 'A stone that will always activate';
		}
	}
}
