package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class LongWaitStone extends StoneExtension
	{
		public function LongWaitStone()
		{
			super();
			name = '15s Cooldown';
			description = 'description';
			cooldown = 15 * 1000;
		}
	}
}