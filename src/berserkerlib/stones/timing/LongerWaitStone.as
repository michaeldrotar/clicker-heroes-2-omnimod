package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class LongerWaitStone extends StoneExtension
	{
		public function LongerWaitStone()
		{
			super();
			name = '30s Cooldown';
			description = 'description';
			cooldown = 30 * 1000;
		}
	}
}