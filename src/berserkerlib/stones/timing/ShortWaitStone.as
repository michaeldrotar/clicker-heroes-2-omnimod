package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class ShortWaitStone extends StoneExtension
	{
		public function ShortWaitStone()
		{
			super();
			name = '5s Cooldown';
			description = 'description';
			cooldown = 5000;
		}
	}
}