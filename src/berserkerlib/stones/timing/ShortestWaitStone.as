package berserkerlib.stones.timing
{
	import berserkerlib.StoneExtension;

	public class ShortestWaitStone extends StoneExtension
	{
		public function ShortestWaitStone()
		{
			super();
			name = '1s Cooldown';
			description = 'description';
			cooldown = 1000;
		}
	}
}
