package berserkerlib.stats
{
	import berserkerlib.StatExtension;
	import models.Character;

	public class Speed extends StatExtension
	{
		public var berserkAttackMultiplier:Function = Character.linear(0.3);
		public var berserkSpeedMultiplier:Function = Character.linear(0.03);
		public var gcdMultiplier:Function = Character.linear(20)

		public function Speed()
		{
			name = 'Speed';
		}

		override public function apply():void
		{
			character.gcdMinimum = 0.1;
			character.gcdBase = 2000 - gcdMultiplier(amount);
		}

		public function onExtensionsCreated():void
		{
			hook(extensions.berserk, 'minAdditionalAutoAttacks', function():Number
			{
				return next() + Math.floor(berserkAttackMultiplier(amount));
			});

			hook(extensions.berserk, 'maxAdditionalAutoAttacks', function():Number
			{
				return next() + Math.ceil(berserkAttackMultiplier(amount));
			});

			hook(extensions.berserk, 'speedBonus', function():Number
			{
				return next() + berserkSpeedMultiplier(amount);
			});
		}
	}
}