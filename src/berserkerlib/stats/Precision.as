package berserkerlib.stats
{
	import berserkerlib.StatExtension;
	import heroclickerlib.CH2;
	import models.Character;

	public class Precision extends StatExtension
	{
		public var critChanceBonus:Function = Character.linear(0.01);
		public var critDamageMultiplier:Function = Character.exponentialMultiplier(1.25);

		public function Precision()
		{
			name = 'Precision';
		}

		override public function apply():void
		{
			var critChanceLevel:int = character.statLevels[CH2.STAT_CRIT_CHANCE];
			var critChanceDiff:int = amount - critChanceLevel;
			if (critChanceDiff !== 0)
			{
				character.levelUpStat(CH2.STAT_CRIT_CHANCE, critChanceDiff);
			}

			var critDamageLevel:int = character.statLevels[CH2.STAT_CRIT_DAMAGE];
			var critDamageDiff:int = amount - critDamageLevel;
			if (critDamageDiff !== 0)
			{
				character.levelUpStat(CH2.STAT_CRIT_DAMAGE, critDamageDiff);
			}
		}

		public function onCharacterCreated():void
		{
			character.statValueFunctions[CH2.STAT_CRIT_CHANCE] = critChanceBonus;
			character.statValueFunctions[CH2.STAT_CRIT_DAMAGE] = critDamageMultiplier;
		}
	}
}