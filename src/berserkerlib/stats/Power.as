package berserkerlib.stats
{
	import berserkerlib.StatExtension;
	import heroclickerlib.CH2;
	import models.Character;

	public class Power extends StatExtension
	{
		public var damageMultiplier:Function = Character.exponentialMultiplier(1.2);

		public function Power()
		{
			name = 'Power';
		}

		override public function apply():void
		{
			var level:int = character.level;
			var statLevel:int = character.statLevels[CH2.STAT_DAMAGE];
			var diff:int = ((level * 2) + amount) - statLevel;
			if (diff !== 0)
			{
				character.levelUpStat(CH2.STAT_DAMAGE, diff);
			}
		}

		public function onCharacterCreated():void
		{
			character.statValueFunctions[CH2.STAT_DAMAGE] = damageMultiplier;
		}
	}
}