package berserkerlib
{
	import models.AutomatorGem;
	import models.Skill;

	public class SkillGemExtension extends GemExtension
	{
		public var skill:Skill;

		public function SkillGemExtension()
		{
			super();
		}

		public function get activeSkill():Skill
		{
			return character.getActiveSkill(skill.uid);
		}

		override public function get ready():Boolean
		{
			return activeSkill && character.canUseSkill(activeSkill);
		}

		override public function execute():void
		{
			activeSkill.useSkill();
		}

		override public function toGem():AutomatorGem
		{
			var gem:AutomatorGem = super.toGem();
			gem.name = skill.tooltip['header'];
			gem.iconId = skill.iconId;
			gem.description = 'Activates ' + skill.tooltip['header'] + ' on cooldown.';
			return gem;
		}
	}
}