package berserkerlib.skills
{
	import berserkerlib.SkillExtension;
	import models.Monster;
	import models.MonsterDebuff;

	public class Rupture extends SkillExtension
	{
		public var damagePerSecondMultiplier:Number;
		public var duration:Number;
		public var cooldownReduction:Number;

		public function Rupture()
		{
			name = 'Rupture';
			description = 'Doubles crit chance against the target and deals { damagePerSecondMultiplier | percent | damage } damage per second.\n';
			description += 'Critical hits reduce the cooldown by { cooldownReduction | duration | value }.\n';
			description += 'Lasts { duration | duration | value }.'
			iconId = 199;
			cooldown = 30 * 1000;
			energyCost = 20;
			learnAtLevel = 10;

			damagePerSecondMultiplier = 3;
			duration = 10 * 1000;
			cooldownReduction = 1 * 1000;
		}

		public function effectFunction():void
		{
			var ruptureTarget:Monster = nextMonster;

		}
	}
}