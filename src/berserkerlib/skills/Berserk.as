package berserkerlib.skills
{
	import omnimodlib.Extension;
	import berserkerlib.Tooltip;
	import heroclickerlib.CH2;
	import models.Buff;
	import models.Character;

	public class Berserk extends Extension
	{
		public var name:String;
		public var description:Function;
		public var iconId:Number = 0;

		public var duration:Number = 0;
		public var hastened:Boolean = false;
		public var tickRate:Number = 100;
		public var initialStacks:Number = 1;
		public var maximumStacks:Number = 1;

		public var attackChance:Number = 0.50;
		public var energyThreshold:Number = 50;
		public var speedBonus:Function;
		public var minAdditionalAutoAttacks:Function;
		public var maxAdditionalAutoAttacks:Function;

		public var getTalentDescriptions:Function;

		public function Berserk()
		{
			super();
			name = 'Berserk';
			description = descriptionDefault;
			iconId = 21;
			tickRate = 1000 / 60;

			speedBonus = function():Number
			{
				return 0.5;
			};

			minAdditionalAutoAttacks = function():Number
			{
				return 1;
			};

			maxAdditionalAutoAttacks = function():Number
			{
				return 2;
			};

			getTalentDescriptions = function():Array
			{
				return [];
			};
		}

		public function get currentThresholdBonus():Number
		{
			return (character.energy > energyThreshold ? 2 : 1);
		}

		public function descriptionDefault():String
		{
			var description:String = '';
			description += 'Grants a { attackChance | percent | value } chance to unleash { minAdditionalAutoAttacks | value }-{ maxAdditionalAutoAttacks | value } additional auto-attacks.\n';
			description += 'Grants { speedBonus | percent | value } bonus movement speed.\n';
			description += 'Chance and movement speed are doubled when energy is greater than { energyThreshold | value }.\n';
			return description;
		}

		public function enable():void
		{
			if (character)
			{
				character.buffs.addBuff(toBuff());
			}
		}

		public function getBuff():Buff
		{
			return character.buffs.getBuff(id);
		}

		public function tick():void
		{
			var buff:Buff = getBuff();
			var speedBonus:Number = this.speedBonus() * currentThresholdBonus;
			var isBeforeBoss:Boolean = character.currentZone === character.currentWorld.zonesPerWorld - 1 && character.monstersKilledOnCurrentZone >= character.monstersPerZone - 1;
			var isOnBoss:Boolean = character.currentZone === character.currentWorld.zonesPerWorld;
			if (isBeforeBoss || isOnBoss)
			{
				// Game locks up if speed is too high on boss stage
				speedBonus = 0;
			}
			buff.buffStat(CH2.STAT_MOVEMENT_SPEED, 1 + speedBonus);
		}

		public function tooltip():Object
		{
			return {'header': name, 'body': Tooltip.format(description(), this)};
		}

		public function toBuff():Buff
		{
			var buff:Buff = new Buff();
			buff.name = id;
			buff.iconId = iconId;

			buff.duration = duration;
			buff.isUntimedBuff = duration == 0;
			buff.unhastened = !hastened;
			buff.tickRate = tickRate;
			buff.stacks = initialStacks;
			buff.maximumStacks = maximumStacks;

			buff.tickFunction = tick;
			buff.tooltipFunction = tooltip;

			return buff;
		}

		public function onCharacterCreated():void
		{
			hook(character, 'onWorldStarted', function(worldNumber:Number):void
			{
				enable();
				next(worldNumber);
			});
		}

		public function onCharacterStarted():void
		{
			enable();
		}

		public function onAutoAttack():void
		{
			if (roller.boolean(attackChance * currentThresholdBonus))
			{
				var attacks:Number = roller.range(minAdditionalAutoAttacks(), maxAdditionalAutoAttacks());
				var buff:Buff = new Buff();
				buff.name = 'BerserkAutoAttacks';
				buff.iconId = 202;
				buff.duration = character.attackMsDelay;
				buff.tickRate = buff.duration / attacks;
				buff.tickFunction = function():void
				{
					if (character.isNextMonsterInRange)
					{
						extensions.characterAttack.attackNextMonster();
						character.addEnergy(character.energyRegeneration, false);
					}
					else
					{
						character.characterDisplay.playClickAttack();
					}
				}
				buff.finishFunction = function():void
				{
					if (character.state === Character.STATE_WALKING)
					{
						character.characterDisplay.playWalk();
					}
				};
				character.buffs.addBuff(buff);
			}
		}
	}
}