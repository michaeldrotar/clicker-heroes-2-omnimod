package berserkerlib.skills
{
	import berserkerlib.SkillExtension;
	import omnimodlib.Util;
	import heroclickerlib.CH2;
	import heroclickerlib.world.World;
	import models.AttackData;
	import models.Buff;
	import models.Character;
	import models.Monster;

	public class Shove extends SkillExtension
	{
		public var damageMultiplier:Number;
		public var distance:Function;
		public var maxMonsters:Number;

		public function Shove()
		{
			name = 'Shove';
			description = 'Shoves monsters { distance | distance |value }. Deals { damageMultiplier | percent | damage } damage to all shoved monsters.';
			iconId = 78;
			energyCost = 20;
			learnAtLevel = 3;

			damageMultiplier = 3;
			distance = distanceDefault;
			maxMonsters = 100;
		}

		public function distanceDefault():Number
		{
			return Util.MONSTER_POSITION_DISTANCE * 3;
		}

		public function effectFunction():void
		{
			// Adapted from World.updateMonsters
			var world:World = CH2.world;
			var newMonster:Monster;
			while ((world.monsters.monsters.length < maxMonsters) && (!world.monsters.isNextMonsterBoss() || !CH2.user.isProgressionModeActive) && (world.monsters.calculateZoneIndexOfNextSpawnedMonster() != 0 || world.monsters.monsters.length == 0 || !world.monsters.monsters[world.monsters.monsters.length - 1].isAlive))
			{
				newMonster = new Monster();
				newMonster.setup(world.monsters.getNextMonsterId());
				newMonster.x = CH2.currentCharacter.x;
				newMonster.y = world.newMonsterWorldY();
				world.addMonsterToWorld(newMonster);
			}

			var monsters:Array = CH2.world.monsters.getMonstersInRadius(character.x, character.y, character.attackRange * 2);
			while (monsters.length > maxMonsters)
			{
				monsters.pop();
			}

			var attackData:AttackData = new AttackData();
			attackData.isSkillAttack = true;
			attackData.damage = character.clickDamage.multiplyN(damageMultiplier);

			extensions.characterAttack.dealDamageToMonsters(attackData, monsters);
			character.characterDisplay.playClickAttack();
			character.playRandomHitSound(attackData);

			var lastMonster:Monster = monsters[monsters.length - 1];
			var nextMonster:Monster = getMonsterAfterMonster(lastMonster);

			if (!nextMonster)
			{
				return;
			}

			var distanceRemaining:Number = distance();

			var buff:Buff = new Buff();
			buff.name = name;
			buff.iconId = iconId;
			buff.tickRate = 1000 / 30;
			buff.isUntimedBuff = true;

			var moveFunction:Function = function(moveDistance:Number):void
			{
				var monster:Monster;
				for (var i:int = 0; i < monsters.length; i++)
				{
					monster = monsters[i];
					monster.y += moveDistance;
				}
				if (!character.isNextMonsterInRange && character.state === Character.STATE_COMBAT)
				{
					character.changeState(Character.STATE_ENDING_COMBAT);
				}
			};

			buff.tickFunction = function():void
			{
				var tickDistance:Number = (distanceRemaining * .6);
				while (nextMonster && nextMonster.y - lastMonster.y <= tickDistance)
				{
					var distanceBefore:Number = nextMonster.y - lastMonster.y;
					var distanceAfter:Number = tickDistance - distanceBefore;
					if (monsters.length == maxMonsters || nextMonster.isBoss || nextMonster.isMiniBoss || nextMonster.isFinalBoss)
					{
						tickDistance = distanceBefore;
						distanceRemaining = tickDistance;
					}
					monsters.push(nextMonster);

					attackData.isCritical = false; // these impacts are later so reset crit so all these hits don't always sound like a crit
					extensions.characterAttack.dealDamageToMonster(attackData, nextMonster);
					character.playRandomHitSound(attackData);

					nextMonster.y = lastMonster.y; // so nextMonster ends up in correct position after move
					lastMonster = nextMonster;
					nextMonster = getMonsterAfterMonster(nextMonster);
				}
				distanceRemaining -= tickDistance;
				if (distanceRemaining <= 1)
				{
					tickDistance += distanceRemaining;
					distanceRemaining = 0;
				}
				moveFunction(tickDistance);
				if (lastMonster.isBoss || lastMonster.isMiniBoss || lastMonster.isFinalBoss || !nextMonster || distanceRemaining === 0)
				{
					buff.isFinished = true;
					buff.onFinish();
				}
			}
			buff.startFunction = buff.tickFunction;
			character.buffs.addBuff(buff);
		}

		public function getMonsterAfterMonster(sourceMonster:Monster):Monster
		{
			if (!sourceMonster)
			{
				return null;
			}

			var monster:Monster = null;
			var foundSource:Boolean = false;
			for (var i:int = 0; i < CH2.world.monsters.monsters.length; i++)
			{
				monster = CH2.world.monsters.monsters[i];
				if (foundSource)
				{
					if (monster.isAlive)
					{
						return monster;
					}
				}
				if (monster === sourceMonster)
				{
					foundSource = true;
				}
			}
			return null;
		}
	}
}