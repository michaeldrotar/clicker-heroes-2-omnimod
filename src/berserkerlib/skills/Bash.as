package berserkerlib.skills
{
	import berserkerlib.SkillExtension;
	import berserkerlib.Tooltip;
	import omnimodlib.Util;
	import com.playsaurus.numbers.BigNumber;
	import models.AttackData;

	public class Bash extends SkillExtension
	{
		public var energyBonus:Number;
		public var damageMultiplier:Number;
		public var bonusDamageMultiplier:Number;

		public function Bash()
		{
			name = 'Bash';
			description = 'Bashes the nearest target for { damageMultiplier | percent | damage } damage.\n';
			description += 'Deals { bonusDamageMultiplier | value }{ "x" | value } damage when energy is full.';
			iconId = 19;
			cooldown = 3 * 1000;
			learnAtLevel = 1;

			energyBonus = 9;
			damageMultiplier = 3;
			bonusDamageMultiplier = 3;
		}

		override public function getTooltip():Object
		{
			var costs:Array = [];
			var descriptions:Array = [];
			var talents:Array = getTalentDescriptions();

			costs.push('{ "Generate:" | label } { energyBonus | energy } energy');
			costs.push('{ "Cooldown:" | label } { cooldown | duration }');

			descriptions.push(description);

			var sections:Array = [];
			if (costs.length > 0)
			{
				sections.push(costs.join('\n'));
			}
			if (descriptions.length > 0)
			{
				sections.push(descriptions.join('\n'));
			}
			if (talents.length > 0)
			{
				sections.push(talents.join('\n'));
			}

			var body:String = sections.join('\n\n');
			return {'header': name, 'body': Tooltip.format(body, this)};
		}

		public function effectFunction():void
		{
			var isFull:Boolean = character.energy == character.maxEnergy.numberValue();
			if (!isFull)
			{
				character.addEnergy(energyBonus);
			}

			var attackData:AttackData = new AttackData();
			attackData.isSkillAttack = true;
			attackData.damage = character.clickDamage.multiplyN(damageMultiplier);
			if (isFull)
			{
				attackData.damage = attackData.damage.multiplyN(bonusDamageMultiplier);
			}

			extensions.characterAttack.dealDamageToNextMonster(attackData);
			character.characterDisplay.playClickAttack();
			character.playRandomHitSound(attackData);
		}
	}
}