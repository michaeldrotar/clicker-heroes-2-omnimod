package berserkerlib.skills
{
	import berserkerlib.SkillExtension;

	public class SliceAndDice extends SkillExtension
	{
		public var damageMultiplier:Number;
		public var refreshChance:Number;

		public function SliceAndDice()
		{
			name = 'Slice and Dice';
			description = 'Delivers 2 rapid attacks to the target, dealing { damageMultiplier | percent | damage } damage per attack.\n';
			description += 'Each attack has a { refreshChance | percent | value } chance to refresh the duration of Rupture';
			iconId = 197;
			energyCost = 10;
			learnAtLevel = 15;

			damageMultiplier = 10;
			refreshChance = 0.2;
		}

		public function effectFunction():void
		{

		}
	}
}