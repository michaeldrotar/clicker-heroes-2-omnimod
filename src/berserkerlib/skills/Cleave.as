package berserkerlib.skills
{
	import berserkerlib.SkillExtension;
	import heroclickerlib.CH2;
	import models.AttackData;

	public class Cleave extends SkillExtension
	{
		public var damageMultiplier:Number;
		public var bonusDamageMultiplier:Number;

		public function Cleave()
		{
			name = 'Cleave';
			description = 'Cleaves all targets in melee range for { damageMultiplier | percent | damage } damage.\n';
			description += 'Deals an additional { bonusDamageMultiplier | percent | damage } damage per additional enemy in range.';
			iconId = 51;
			energyCost = 25;
			learnAtLevel = 5;

			damageMultiplier = 3;
			bonusDamageMultiplier = 3;
		}

		public function effectFunction():void
		{
			var monsters:Array = CH2.world.monsters.getMonstersInRadius(character.x, character.y, character.attackRange * 2);
			var attackData:AttackData = new AttackData();
			attackData.isSkillAttack = true;
			attackData.damage = character.clickDamage.multiplyN(damageMultiplier);
			attackData.damage = attackData.damage.add(character.clickDamage.multiplyN(bonusDamageMultiplier * (monsters.length - 1)));
			extensions.characterAttack.dealDamageToMonsters(attackData, monsters);
			character.characterDisplay.playClickAttack();
			character.playRandomHitSound(attackData);
		}
	}
}
