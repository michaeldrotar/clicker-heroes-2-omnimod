package berserkerlib
{
	import omnimodlib.Extension;
	import berserkerlib.NodeTypeExtension;
	import berserkerlib.nodeTypes.IncreasePower;
	import berserkerlib.nodeTypes.IncreasePowerBig;
	import berserkerlib.nodeTypes.IncreasePowerSuper;
	import berserkerlib.nodeTypes.IncreasePrecision;
	import berserkerlib.nodeTypes.IncreasePrecisionBig;
	import berserkerlib.nodeTypes.IncreasePrecisionSuper;
	import berserkerlib.nodeTypes.IncreaseSpeed;
	import berserkerlib.nodeTypes.IncreaseSpeedBig;
	import berserkerlib.nodeTypes.IncreaseSpeedSuper;
	import berserkerlib.talents.Fury;
	import berserkerlib.talents.Rage;
	import heroclickerlib.LevelGraph;
	import heroclickerlib.LevelGraphNode;
	import models.Character;

	public class SkillTree extends Extension
	{
		public var tiers:Array = [];

		public var nodes:Array = [];
		public var edges:Array = [];

		public function addNode(nodeType:NodeTypeExtension):LevelGraphNode
		{
			var levelGraphNode:LevelGraphNode = new LevelGraphNode();
			levelGraphNode.id = nodes.length;
			levelGraphNode.type = nodeType.id;
			levelGraphNode.alwaysAvailable = nodeType.alwaysAvailable;
			levelGraphNode.special = nodeType.quality == NodeTypeExtension.QUALITY_SPECIAL;
			levelGraphNode.deluxe = nodeType.quality == NodeTypeExtension.QUALITY_DELUXE;
			levelGraphNode.connections = [];
			nodes.push(levelGraphNode);
			return levelGraphNode;
		}

		public function addConnections(node:LevelGraphNode, ... connections):void
		{
			if (connections[0] is Array)
			{
				connections = connections[0];
			}
			for (var i:int = 0; i < connections.length; i++)
			{
				if (connections[i] is LevelGraphNode)
				{
					var connection:LevelGraphNode = connections[i];
					node.connections.push(connection.id);
					connection.connections.push(node.id);
					edges.push([node.id, connection.id]);
				}
			}
		}

		public function addTier(tier:int, pos:int, stat:NodeTypeExtension, bigStat:NodeTypeExtension, bonus:NodeTypeExtension):Array
		{
			var nodes:Array = [];
			var total:int = (tier * 5) + 5;
			for (var i:int = 0; i < total - 1; i++)
			{
				nodes.push(addNode((i + 1) % 5 === 0 ? bigStat : stat));
			}
			nodes.push(addNode(bonus));

			var padding:Number = 0.03;
			var arcSize:Number = 1 / 3;
			var offset:Number = (90 / 360) - (arcSize / 2);
			var radius:Number = total * 80;
			var startPercent:Number = (((pos - 1) * arcSize) + padding) + offset;
			arrangeCircle(nodes, 0, 0, radius, startPercent, (arcSize - (padding * 2)));

			if (tier === 1)
			{
				nodes[0].alwaysAvailable = true;
			}

			tiers[tier] ||= [];
			tiers[tier][pos] = nodes;

			return nodes;
		}

		public function addTierConnections():void
		{
			for (var i:int = 1; i < tiers.length - 1; i++)
			{
				var tier:Array = tiers[i];
				var nextTier:Array = tiers[i + 1];

				for (var p:int = 1; p < tier.length; p++)
				{
					var nodes:Array = tier[p];
					var nextTierNodes:Array = nextTier[p];
					var nextNeighborTierNodes:Array = (p === tier.length - 1 ? nextTier[1] : nextTier[p + 1]);

					var lastNode:LevelGraphNode = nodes[nodes.length - 1];
					var nextNode:LevelGraphNode = nextTierNodes[0];
					var neighborNode:LevelGraphNode = nextNeighborTierNodes[0];

					nextNode.connections.push(lastNode.id);
					neighborNode.connections.push(lastNode.id);
				}
			}
		}

		public function arrangeCircle(statNodes:Array, centerX:int, centerY:int, radius:Number, startPercent:Number = 0, sizePercent:Number = 1, clockwise:Boolean = false):void
		{
			var node:LevelGraphNode;
			var previous:LevelGraphNode;

			var maxRadians:Number = Math.PI * 2;
			var startRadians:Number = (1 - startPercent) * maxRadians;
			var circleRadians:Number = maxRadians * sizePercent;

			var angleRadians:Number = startRadians;
			var angleStep:Number = circleRadians / (statNodes.length - (sizePercent < 1 ? 1 : 0));
			if (!clockwise)
			{
				angleStep *= -1;
			}
			for (var i:int = 0; i < statNodes.length; i++)
			{
				node = statNodes[i];
				moveNode(node, centerX + (radius * Math.cos(angleRadians)), centerY + (radius * Math.sin(angleRadians)), previous);

				angleRadians += angleStep;
				previous = node;
			}
		}

		public function moveNode(node:LevelGraphNode, x:int, y:int, ... connections):void
		{
			node.x = x
			node.y = y

			addConnections(node, connections);
		}

		public function moveNodes(xOffset:Number, yOffset:Number):void
		{
			for (var i:int = 0; i < nodes.length; i++)
			{
				nodes[i].x += xOffset;
				nodes[i].y += yOffset;
			}
		}

		public function repurchaseNodes():void
		{
			// Cleanup potentially old nodes from past versions of mod
			data.removeMatched(/_(TALENT|STAT)_/);

			var id:String;
			var type:String;
			var nodeType:Object;
			for (id in character.nodesPurchased)
			{
				type = character.levelGraph.nodes[id].type;
				nodeType = character.levelGraphNodeTypes[type];
				nodeType.purchaseFunction();
			}
		}

		public function scaleNodes(scale:Number):void
		{
			for (var i:int = 0; i < nodes.length; i++)
			{
				nodes[i].x *= scale;
				nodes[i].y *= scale;
			}
		}

		public function onExtensionsCreated():void
		{
			var increasePower:IncreasePower = extensions.increasePower;
			var increasePowerBig:IncreasePowerBig = extensions.increasePowerBig;
			var increasePowerSuper:IncreasePowerSuper = extensions.increasePowerSuper;
			var increaseSpeed:IncreaseSpeed = extensions.increaseSpeed;
			var increaseSpeedBig:IncreaseSpeedBig = extensions.increaseSpeedBig;
			var increaseSpeedSuper:IncreaseSpeedSuper = extensions.increaseSpeedSuper;
			var increasePrecision:IncreasePrecision = extensions.increasePrecision;
			var increasePrecisionBig:IncreasePrecisionBig = extensions.increasePrecisionBig;
			var increasePrecisionSuper:IncreasePrecisionSuper = extensions.increasePrecisionSuper;

			var rage:Rage = extensions.rage;
			var fury:Fury = extensions.fury;

			addTier(1, 1, increasePower, increasePowerBig, rage);
			addTier(1, 2, increaseSpeed, increaseSpeedBig, increaseSpeedSuper);
			addTier(1, 3, increasePrecision, increasePrecisionBig, increasePrecisionSuper);

			addTier(2, 1, increasePower, increasePowerBig, fury);
			addTier(2, 2, increaseSpeed, increaseSpeedBig, increaseSpeedSuper);
			addTier(2, 3, increasePrecision, increasePrecisionBig, increasePrecisionSuper);

			addTier(3, 1, increasePower, increasePowerBig, increasePowerSuper);
			addTier(3, 2, increaseSpeed, increaseSpeedBig, increaseSpeedSuper);
			addTier(3, 3, increasePrecision, increasePrecisionBig, increasePrecisionSuper);

			addTier(4, 1, increasePower, increasePowerBig, increasePowerSuper);
			addTier(4, 2, increaseSpeed, increaseSpeedBig, increaseSpeedSuper);
			addTier(4, 3, increasePrecision, increasePrecisionBig, increasePrecisionSuper);

			addTier(5, 1, increasePower, increasePowerBig, increasePowerSuper);
			addTier(5, 2, increaseSpeed, increaseSpeedBig, increaseSpeedSuper);
			addTier(5, 3, increasePrecision, increasePrecisionBig, increasePrecisionSuper);

			var startingNodes:Array = [];
			startingNodes.push(tiers[1][1][0]);
			startingNodes.push(tiers[1][2][0]);
			startingNodes.push(tiers[1][3][0]);
			arrangeCircle(startingNodes, 0, 0, 80, 0.25);
			addConnections(startingNodes[0], startingNodes[1], startingNodes[2]);

			addTierConnections();
		}

		public function onCharacterTemplateCreated(characterTemplate:Character):void
		{
			var levelGraph:LevelGraph = characterTemplate.levelGraph;

			var offset:int = characterTemplate.levelGraph.nodes.length;

			var index:int;
			var node:LevelGraphNode;

			for (index = 0; index < nodes.length; index++)
			{
				node = nodes[index];
				node.id += offset;

				if (node.connections.length == 0)
				{
					node.alwaysAvailable = true;
				}

				for (var i:int = 0; i < node.connections.length; i++)
				{
					node.connections[i] += offset;
				}
			}

			for (index = 0; index < edges.length; index++)
			{
				var startId:int = edges[index][0] + offset;
				var endId:int = edges[index][1] + offset;

				edges[index] = [nodes[startId].x, nodes[startId].y, nodes[endId].x, nodes[endId].y, startId, endId];
			}

			levelGraph.nodes = levelGraph.nodes.concat(nodes);
			levelGraph.edges = levelGraph.edges.concat(edges);
		}

		public function onCharacterStarted():void
		{
			repurchaseNodes();
		}
	}
}
