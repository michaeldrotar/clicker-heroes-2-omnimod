package berserkerlib.gems.items
{
	import berserkerlib.ItemGemExtension;
	import models.Item;

	public class UpgradeCheapestItemFullGem extends ItemGemExtension
	{
		public function UpgradeCheapestItemFullGem()
		{
			super();
			name = 'Upgrade Cheapest Item to 100';
			description = 'Upgrades the cheapest item under level 100.';
			assetNumber = 5;
		}

		public function get cheapestItem():Item
		{
			var item:Item;
			var cheapestItem:Item;
			for each (item in upgradableItems)
			{
				if (!cheapestItem || item.cost().lt(cheapestItem.cost()))
				{
					if (item.level < 100)
					{
						cheapestItem = item;
					}
				}
			}
			return cheapestItem;
		}

		override public function get ready():Boolean
		{
			return super.ready && cheapestItem && character.gold.gte(cheapestItem.cost());
		}

		override public function execute():void
		{
			if (cheapestItem)
			{
				upgradeItem(cheapestItem);
			}
		}
	}
}