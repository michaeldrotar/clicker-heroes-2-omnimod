package berserkerlib.gems.items
{
	import berserkerlib.ItemGemExtension;
	import models.Item;

	public class UpgradeCheapestItemHalfGem extends ItemGemExtension
	{
		public function UpgradeCheapestItemHalfGem()
		{
			super();
			name = 'Upgrade Cheapest Item to 50';
			description = 'Upgrades the cheapest item under level 50.';
			assetNumber = 5;
		}

		public function get cheapestItem():Item
		{
			var item:Item;
			var cheapestItem:Item;
			for each (item in upgradableItems)
			{
				if (!cheapestItem || item.cost().lt(cheapestItem.cost()))
				{
					if (item.level < 50)
					{
						cheapestItem = item;
					}
				}
			}
			return cheapestItem;
		}

		override public function get ready():Boolean
		{
			return super.ready && cheapestItem && character.gold.gte(cheapestItem.cost());
		}

		override public function execute():void
		{
			if (cheapestItem)
			{
				upgradeItem(cheapestItem);
			}
		}
	}
}