package berserkerlib.gems.skills
{
	import berserkerlib.SkillGemExtension;

	public class RuptureSkillGem extends SkillGemExtension
	{
		public function onExtensionsCreated():void
		{
			skill = extensions.rupture.toSkill();
		}
	}
}