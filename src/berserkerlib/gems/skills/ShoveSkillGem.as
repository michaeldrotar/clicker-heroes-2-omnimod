package berserkerlib.gems.skills
{
	import berserkerlib.SkillGemExtension;

	public class ShoveSkillGem extends SkillGemExtension
	{
		public function onExtensionsCreated():void
		{
			skill = extensions.shove.toSkill();
		}
	}
}