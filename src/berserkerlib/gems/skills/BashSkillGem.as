package berserkerlib.gems.skills
{
	import berserkerlib.SkillGemExtension;

	public class BashSkillGem extends SkillGemExtension
	{
		public function onExtensionsCreated():void
		{
			skill = extensions.bash.toSkill();
		}
	}
}