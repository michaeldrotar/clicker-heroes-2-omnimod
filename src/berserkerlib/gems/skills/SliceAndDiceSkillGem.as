package berserkerlib.gems.skills
{
	import berserkerlib.SkillGemExtension;

	public class SliceAndDiceSkillGem extends SkillGemExtension
	{
		public function onExtensionsCreated():void
		{
			skill = extensions.sliceAndDice.toSkill();
		}
	}
}