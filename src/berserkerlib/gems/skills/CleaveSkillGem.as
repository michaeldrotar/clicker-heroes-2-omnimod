package berserkerlib.gems.skills
{
	import berserkerlib.SkillGemExtension;

	public class CleaveSkillGem extends SkillGemExtension
	{
		public function onExtensionsCreated():void
		{
			skill = extensions.cleave.toSkill();
		}
	}
}