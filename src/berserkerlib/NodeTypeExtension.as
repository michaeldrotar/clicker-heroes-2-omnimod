package berserkerlib
{
	import omnimodlib.Extension;
	import berserkerlib.Tooltip;
	import omnimodlib.Util;
	import models.Character;

	public class NodeTypeExtension extends Extension
	{
		public static const QUALITY_NORMAL:int = 0;
		public static const QUALITY_SPECIAL:int = 1;
		public static const QUALITY_DELUXE:int = 2;

		public var name:String = '';
		public var description:String = '';
		public var quality:int = QUALITY_NORMAL;
		public var icon:String = '';
		public var alwaysAvailable:Boolean = false;

		public function toObject():Object
		{
			var nodeType:Object = new Object();
			nodeType['id'] = id;
			nodeType['name'] = name;
			nodeType['special'] = quality == QUALITY_SPECIAL;
			nodeType['deluxe'] = quality == QUALITY_DELUXE;
			nodeType['icon'] = icon;
			nodeType['tooltip'] = Tooltip.format(description, this);
			nodeType['alwaysAvailable'] = alwaysAvailable;
			nodeType['setupFunction'] = function():void
			{
				call('setup');
			}
			nodeType['purchaseFunction'] = function():void
			{
				call('purchase');
				trigger('onNodePurchased');
			}
			return nodeType;
		}

		public function onCharacterTemplateCreated(characterTemplate:Character):void
		{
			var nodeType:Object = toObject();
			characterTemplate.levelGraphNodeTypes[nodeType.id] = nodeType;
		}
	}
}