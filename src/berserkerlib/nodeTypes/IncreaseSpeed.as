package berserkerlib.nodeTypes
{
	import berserkerlib.NodeTypeExtension;

	public class IncreaseSpeed extends NodeTypeExtension
	{
		public var amount:Number;

		public function IncreaseSpeed()
		{
			super();
			name = 'Speed';
			description = 'Increases Berserk\'s additional auto-attacks by { additionalAutoAttacksBonus | percent | value }.\n';
			description += 'Increases Berserk\'s movement speed by { speedBonus | percent | value }.\n';
			description += 'Reduces global cooldown by { gcdBonus | duration | value }.';
			icon = 'haste';
			amount = 1;
		}

		public function get additionalAutoAttacksBonus():Number
		{
			return extensions.speed.berserkAttackMultiplier(amount) - 1;
		}

		public function get gcdBonus():Number
		{
			return extensions.speed.gcdMultiplier(amount);
		}

		public function get speedBonus():Number
		{
			return extensions.speed.berserkSpeedMultiplier(amount);
		}

		public function purchase():void
		{
			extensions.speed.amount += amount;
		}
	}
}