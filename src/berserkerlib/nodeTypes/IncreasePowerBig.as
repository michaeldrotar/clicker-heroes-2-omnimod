package berserkerlib.nodeTypes
{

	public class IncreasePowerBig extends IncreasePower
	{
		public function IncreasePowerBig()
		{
			super();
			name = 'Power x3';
			amount = 3;
			quality = QUALITY_SPECIAL;
		}
	}
}
