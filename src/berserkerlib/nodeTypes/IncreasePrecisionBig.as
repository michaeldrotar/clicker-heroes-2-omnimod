package berserkerlib.nodeTypes
{

	public class IncreasePrecisionBig extends IncreasePrecision
	{
		public function IncreasePrecisionBig()
		{
			super();
			name = 'Precision x3';
			amount = 3;
			quality = QUALITY_SPECIAL;
		}
	}
}
