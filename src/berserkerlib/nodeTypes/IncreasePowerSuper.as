package berserkerlib.nodeTypes
{

	public class IncreasePowerSuper extends IncreasePower
	{
		public function IncreasePowerSuper()
		{
			super();
			name = 'Power x10';
			amount = 10;
			quality = QUALITY_DELUXE;
		}
	}
}
