package berserkerlib.nodeTypes
{
	import berserkerlib.NodeTypeExtension;

	public class IncreasePrecision extends NodeTypeExtension
	{
		public var amount:Number;

		public function IncreasePrecision()
		{
			super();
			name = 'Precision';
			description = 'Multiplies crit damage by { critDamageBonus | percent | value }.\n';
			description += 'Increases crit chance by { critChanceBonus | percent | value }.';
			icon = 'critChance';
			amount = 1;
		}

		public function get critChanceBonus():Number
		{
			return extensions.precision.critChanceBonus(amount);
		}

		public function get critDamageBonus():Number
		{
			return extensions.precision.critDamageMultiplier(amount);
		}

		public function purchase():void
		{
			extensions.precision.amount += amount;
		}
	}
}