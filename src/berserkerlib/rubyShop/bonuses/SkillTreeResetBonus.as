package berserkerlib.rubyShop.bonuses
{
	import com.playsaurus.numbers.BigNumber;
	import omnimodlib.RubyPurchaseExtension;
	import ui.IdleHeroUIManager;

	public class SkillTreeResetBonus extends RubyPurchaseExtension
	{
		public function SkillTreeResetBonus()
		{
			super();
			name = 'Skill Tree Reset';
			description = 'Resets the skill tree so new stats and talents can be picked.';
			iconId = 1;
			price = 100;
			priority = 3;
		}

		override public function purchase():void
		{
			data.removeMatched(/_(TALENT|STAT)_/);

			var id:String;
			for (id in character.nodesPurchased)
			{
				delete character.nodesPurchased[id];
			}
			character.spentStatPoints = new BigNumber(0);

			extensions.power.amount = 0;
			extensions.speed.amount = 0;
			extensions.precision.amount = 0;

			// Copied from Character#addExperience
			if (IdleHeroUIManager.instance.mainUI)
			{
				IdleHeroUIManager.instance.mainUI.mainPanel.graphPanel.redrawGraph();
				if (IdleHeroUIManager.instance.mainUI.mainPanel.isOnGraphPanel)
				{
					IdleHeroUIManager.instance.mainUI.mainPanel.graphPanel.updateInteractiveLayer();
				}
			}
		}
	}
}