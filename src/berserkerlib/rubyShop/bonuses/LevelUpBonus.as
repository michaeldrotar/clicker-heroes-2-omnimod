package berserkerlib.rubyShop.bonuses
{
	import omnimodlib.RubyPurchaseExtension;

	public class LevelUpBonus extends RubyPurchaseExtension
	{
		public function LevelUpBonus()
		{
			super();
			name = 'Level Up Bonus';
			description = 'Provides a level\'s worth of experience';
			iconId = 1;
			price = 70;
			priority = 3;
		}

		override public function purchase():void
		{
			character.addExperience(character.levelUpCost);
		}
	}
}