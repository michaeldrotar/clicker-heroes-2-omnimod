package berserkerlib.rubyShop.bonuses
{
	import omnimodlib.RubyPurchaseExtension;

	public class AutomatorSpeedBonus extends RubyPurchaseExtension
	{
		public function AutomatorSpeedBonus()
		{
			super();
			name = 'Automator Speed Bonus';
			description = 'Increases automator speed by 20%';
			iconId = 1;
			price = 100; // look into how to have the price increase with each purchase
		}
	}
}