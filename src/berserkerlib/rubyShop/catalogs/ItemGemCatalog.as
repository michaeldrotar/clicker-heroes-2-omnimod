package berserkerlib.rubyShop.catalogs
{
	import berserkerlib.GemCatalogExtension;

	public class ItemGemCatalog extends GemCatalogExtension
	{
		public function ItemGemCatalog()
		{
			super();
			name = 'Item Gem';
			description = 'Purchase a random gem that buys or upgrades items.';
		}

		public function onExtensionsCreated():void
		{
			catalog[extensions.buyCheapestItemGem.id] = 1000;
			catalog[extensions.upgradeCheapestItemFullGem.id] = 1000;
			catalog[extensions.upgradeCheapestItemHalfGem.id] = 1000;
		}
	}
}