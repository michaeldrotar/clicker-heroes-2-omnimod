package berserkerlib.rubyShop.catalogs
{
	import berserkerlib.GemCatalogExtension;

	public class SkillGemCatalog extends GemCatalogExtension
	{
		public function SkillGemCatalog()
		{
			super();
			name = 'Skill Gem';
			description = 'Purchase a random gem that activates a skill.';
		}

		public function onExtensionsCreated():void
		{
			catalog[extensions.bashSkillGem.id] = 1000;
			catalog[extensions.shoveSkillGem.id] = 100;
			catalog[extensions.cleaveSkillGem.id] = 100;
			//catalog[extensions.ruptureSkillGem.id] = 10;
			//catalog[extensions.sliceAndDiceSkillGem.id] = 10;
			//catalog[extensions.eviscerateSkillGem.id] = 1;
		}
	}
}