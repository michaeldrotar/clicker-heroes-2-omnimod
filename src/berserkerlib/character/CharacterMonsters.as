package berserkerlib.character
{
	import omnimodlib.Extension;
	import heroclickerlib.CH2;
	import models.Environment;
	import models.Monster;

	public class CharacterMonsters extends Extension
	{
		public function CharacterMonsters()
		{

		}

		public function get environment():Environment
		{
			return CH2.currentEnvironment;
		}

		public function get monsters():Vector.<Monster>
		{
			return CH2.world.monsters.monsters;
		}

		public function onUpdate(dt:int):void
		{
			// Adapted from World#updateMonsters
			while ((!CH2.world.monsters.isNextMonsterBoss() || !CH2.user.isProgressionModeActive) && (CH2.world.monsters.calculateZoneIndexOfNextSpawnedMonster() != 0 || monsters.length == 0 || !monsters[monsters.length - 1].isAlive))
			{
				extensions.monsterSpawner.spawnGroupMonsters();
			}

			var index:uint;
			var monster:Monster;
			for (index = 0; index < monsters.length; index++)
			{
				monster = monsters[index];
				if (monster && monster.isAlive)
				{
					//monster.display.monsterAnimation.rotation += (1 / dt);

				}
			}
		}
	}
}