package berserkerlib.character
{
	import omnimodlib.Extension;
	import omnimodlib.Util;
	import heroclickerlib.CH2;
	import models.AttackData;
	import models.Monster;

	public class CharacterAttack extends Extension
	{
		public function CharacterAttack()
		{
			super();
		}

		public function attackNextMonster(isAutoAttack:Boolean = false):void
		{
			var attackData:AttackData = new AttackData();
			attackData.isAutoAttack = true;
			attackData.damage = character.autoAttackDamage;
			dealDamageToNextMonster(attackData);
			if (isAutoAttack || roller.boolean(0.2))
			{
				character.characterDisplay.playAutoAttack();
			}
			else
			{
				character.characterDisplay.playClickAttack();
			}
			character.playRandomHitSound(attackData);
		}

		public function autoAttack():void
		{
			attackNextMonster(true);
			character.timeSinceLastAutoAttack = 0;
			character.addEnergy(character.energyRegeneration, false);
			trigger('onAutoAttack');
		}

		public function dealDamageToMonster(attackData:AttackData, monster:Monster):void
		{
			dealDamageToMonsters(attackData, [monster]);
		}

		public function dealDamageToMonsters(attackData:AttackData, monsters:Array):void
		{
			var index:int = 0;
			var monster:Monster;
			for (index = monsters.length - 1; index >= 0; index--)
			{
				monster = monsters[index];
				if (!monster || !monster.isAlive)
				{
					monsters.splice(index, 1);
				}
			}
			if (monsters.length === 0)
			{
				return;
			}

			var criticalChance:Number;
			var monsterAttackData:AttackData;
			var attackDatas:Array = [];
			for (index = 0; index < monsters.length; index++)
			{
				monster = monsters[index];
				monsterAttackData = attackData.getCopy();
				monsterAttackData.monster = monster;

				criticalChance = character.criticalChance;
				while (criticalChance >= 1)
				{
					monsterAttackData.damage = monsterAttackData.damage.multiplyN(character.criticalDamageMultiplier);
					criticalChance -= 1;
				}
				if (roller.boolean(criticalChance))
				{
					monsterAttackData.isCritical = true;
					monsterAttackData.damage = monsterAttackData.damage.multiplyN(character.criticalDamageMultiplier);
					attackData.isCritical = true; // alert source there's a crit, mostly so sfx can raise the volume of the hit
				}

				monster.takeDamage(monsterAttackData);
				attackDatas.push(monsterAttackData);
			}
			character.buffs.onAttack(attackDatas);
		}

		public function dealDamageToMonstersInArea(attackData:AttackData, centerX:Number, centerY:Number, radius:Number):void
		{
			dealDamageToMonsters(attackData, CH2.world.monsters.getMonstersInRadius(centerX, centerY, radius));
		}

		public function dealDamageToNextMonster(attackData:AttackData):void
		{
			dealDamageToMonster(attackData, CH2.world.getNextMonster());
		}

		public function onCharacterCreated():void
		{
			character.attack = dealDamageToNextMonster;
			character.autoAttack = autoAttack;
			character.clickAttack = Util.noop;
		}
	}
}