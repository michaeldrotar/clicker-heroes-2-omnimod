package berserkerlib
{
	import ui.IdleHeroUIManager;

	public class GemCatalogExtension extends CatalogExtension
	{
		public function GemCatalogExtension()
		{
			super();
			iconId = 1;
			price = 10;
			priority = 1;
		}

		override public function purchaseChoice(choice:String):void
		{
			character.automator.unlockGem(choice);
			character.hasUnlockedAutomator = true;
			IdleHeroUIManager.instance.mainUI.mainPanel.refreshOpenTab();
		}
	}
}