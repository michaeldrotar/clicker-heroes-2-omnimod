package berserkerlib.ui
{
	import omnimodlib.Extension;
	import flash.display.MovieClip;
	import ui.IdleHeroUIManager;

	/**
	 * ...
	 * @author Michael Drotar
	 */
	public class StatPanel extends Extension
	{
		public var name:String;
		public var iconId:Number;
		public var tooltipText:String;

		public var statSubPanel:StatSubPanel;

		public function StatPanel()
		{
			super();
			trace('init');
			name = 'statSubPanel';
			iconId = 6;
			tooltipText = 'Stats';
		}

		public function get glowing():Boolean
		{
			return false;
		}

		public function get visible():Boolean
		{
			return character && character.name === mod.characterName;
		}

		public function onUICreated():void
		{
			trace('on created');
			statSubPanel = new StatSubPanel();
			statSubPanel.setDisplay(new MovieClip());

			IdleHeroUIManager.instance.mainUI.mainPanel.registerTab(IdleHeroUIManager.instance.mainUI.mainPanel.tabs.length, name, statSubPanel, iconId, tooltipText, function():Boolean
			{
				return visible;
			}, function():Boolean
			{
				return glowing;
			}, function():void
			{
				call('onOpen');
			});
		}
	}
}

import flash.text.TextField;
import ui.SubPanel;

class StatSubPanel extends SubPanel
{
	public var textField:TextField;

	public function StatSubPanel()
	{
		super();
		trace('panel init')
	}

	override public function update(time:Number):void
	{
		// This is called every frame with delta time in milliseconds passed to it
	}

	// What happens when the tab is opened
	override public function activate():void
	{
		textField = new TextField();
		textField.text = "Hello World";
		textField.x = 10;
		textField.y = 100;

		// Adds textField as a child to the sub panel's display
		display.addChild(textField);
	}

	// What happens when the tab is closed
	override public function deactivate(refresh:Boolean = false):void
	{
		textField = null;

		// Removes all children that had been added to the sub panel's display
		display.removeChildren();
	}
}