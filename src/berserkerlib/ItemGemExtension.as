package berserkerlib
{
	import berserkerlib.GemExtension;
	import models.Item;

	public class ItemGemExtension extends GemExtension
	{
		public function ItemGemExtension()
		{
			super();
		}

		public function get buyableItems():Array
		{
			return character.catalogItemsForSale || [];
		}

		public function get upgradableItems():Array
		{
			return character.inventory.items || [];
		}

		public function buyItem(item:Item):void
		{
			var index:int;
			for (index = 0; index < buyableItems.length; index++)
			{
				if (buyableItems[index] === item)
				{
					character.purchaseCatalogItem(index);
					return;
				}
			}
		}

		public function upgradeItem(item:Item):void
		{
			character.levelUpItem(item);
		}

		override public function get ready():Boolean
		{
			return !character.didFinishWorld && !character.isPurchasingLocked;
		}
	}
}