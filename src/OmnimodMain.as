package
{
	import flash.display.Sprite;
	import heroclickerlib.CH2;
	import lib.managers.TextManager;
	import models.Character;
	import models.Characters;
	import omnimodlib.Extension;
	import omnimodlib.Extensions;
	import omnimodlib.Fn;
	import omnimodlib.Hooks;
	import omnimodlib.OmniLocalization;
	import omnimodlib.OmniStringUtil;
	import omnimodlib.Util;
	import omnimodlib.managers.OmniSaveManager;
	import omnimodlib.managers.OmniTemplateManager;
	import ui.CH2UI;

	public class OmnimodMain extends Sprite
	{
		public static const MOD_NAME:String = 'Omnimod';
		public static const MOD_DESCRIPTION:String = '';
		public static const MOD_VERSION:Number = 1;
		public static const MOD_AUTHOR:String = 'Michael Drotar';
		public static const TARGET_VERSION:String = null;

		public var MOD_INFO:Object;
		public var fn:Fn;
		public var hooks:Hooks;
		public var extensions:Extensions;
		public static var instance:OmnimodMain;

		public var saveManager:OmniSaveManager;
		public var templateManager:OmniTemplateManager;

		private var _currentCharacter:Character;
		private var _characterTracker:Character;
		private var _localization:OmniLocalization;
		private var _popupShown:Boolean;
		private var _stringUtil:OmniStringUtil;
		private var _versionMismatch:Boolean;

		private var _triggerCache:Object;

		public function OmnimodMain()
		{
			instance = this;

			MOD_INFO = {};
			MOD_INFO['name'] = MOD_NAME;
			MOD_INFO['description'] = MOD_DESCRIPTION;
			MOD_INFO['version'] = MOD_VERSION;
			MOD_INFO['author'] = MOD_AUTHOR;

			saveManager = OmniSaveManager.instance;
			templateManager = OmniTemplateManager.instance;

			_localization = OmniLocalization.instance;
			_stringUtil = OmniStringUtil.instance;

			if (TARGET_VERSION && IdleHeroMain.GAME_VERSION !== TARGET_VERSION)
			{
				_versionMismatch = true;
				return;
			}

			_triggerCache = new Object();

			fn = new Fn();
			hooks = new Hooks();
			extensions = new Extensions();
			trigger('onExtensionsCreated');
		}

		public function get currentCharacter():Character
		{
			return _currentCharacter || CH2.currentCharacter;
		}

		public function onStartup(game:IdleHeroMain):void
		{
			if (_versionMismatch)
			{
				return;
			}

			trigger('onStartup', game);
			trigger('onCreateSkills');
			trigger('onCreateCharacterTemplates');
			var templates:Object = Characters.startingDefaultInstances;
			var name:String;
			for (name in templates)
			{
				trigger('on' + name.replace(/[^\w]+/g, '') + 'CharacterTemplateCreated', templates[name]);
			}
			for (name in templates)
			{
				trigger('onCharacterTemplateCreated', templates[name]);
			}
		}

		public function onCharacterCreated(character:Character):void
		{
			_currentCharacter = character;
			hooks.hookOverride(character, 'update', updateOverride);
			trigger('on' + _currentCharacter.name.replace(/[^\w]+/g, '') + 'CharacterCreated');
			trigger('onCharacterCreated');
			_currentCharacter = null;
		}

		public function onUICreated():void
		{
			if (_versionMismatch)
			{
				return;
			}

			trigger('on' + currentCharacter.name.replace(/[^\w]+/g, '') + 'UICreated');
			trigger('onUICreated');
		}

		public function updateOverride(dt:int, next:Function):void
		{
			if (_versionMismatch && !_popupShown)
			{
				var header:String = 'Sorry, Omnimod Cannot Load';
				var body:String = '';
				body += 'Please install a version for ' + IdleHeroMain.GAME_VERSION + '.\n';
				body += 'NOTE: Gilding will lose data if not updated.\n';
				body += '\n';
				body += TextManager.textToColor('<a href="https://gitlab.com/michaeldrotar/clicker-heroes-2-omnimod/blob/master/README.md">Open Omnimod Site</a>', '#3366CC');
				CH2UI.instance.showSimpleMetaPopup(header, body, null, 'Dismiss');
				_popupShown = true;
			}

			if (_characterTracker !== currentCharacter)
			{
				_characterTracker = currentCharacter;
				trigger('onCharacterStarted');
			}
			next(dt);
			trigger('onCharacterUpdate', dt);
		}

		public function _(text:*, ... args):String
		{
			return _localization.localize.apply(_localization, [].concat(text, args));
		}

		public function format(text:*, ... args):String
		{
			return _stringUtil.formatString.apply(_stringUtil, [].concat(text, args));
		}

		public function trace(... args):void
		{
			Util.trace.apply(Util, args);
		}

		public function traceLine():void
		{
			Util.traceLine();
		}

		public function trigger(name:String, ... args):void
		{
			if (!extensions)
			{
				return;
			}

			if (!_triggerCache[name])
			{
				_triggerCache[name] = [];
				extensions.forEach(function(extension:Extension):void
				{
					if (name in extension && typeof(extension[name]) === 'function')
					{
						_triggerCache[name].push(extension);
					}
				});
			}

			var extension:Extension;
			var index:uint = 0;
			var triggers:Array = _triggerCache[name];
			var length:uint = triggers.length;
			for (; index < length; index++)
			{
				extension = triggers[index];
				extension[name].apply(extension, args);
			}
		}
	}
}