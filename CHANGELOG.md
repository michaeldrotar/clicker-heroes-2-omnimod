## 1.8.5 (2019-11-03)

### Core Version Update
- Fixed error where core changed Monster#goldReward to be a function instead of a property
- Fixed error where Wizard's update override was being replaced with Omnimod's, Omnimod now will call the Wizard's update in addition to its own.

## 1.8.4 (2019-06-08)

### Ruby Shop
- Disabled ruby shop extensions since they don't work with latest experimental changes.

## 1.8.3 (2019-05-15)

### System
- Due to the frequency of experimental updates, the version lock has been temporarily removed.

## 1.8.2 (2019-05-14)

### Ruby Shop
- Remove the option to purchase a Gild Talent Reset since Gild Talents no longer exist.

## 1.8.1 (2019-05-13)

### Performance Improvements
- Significantly improved how quickly **offline time** runs. Under the same conditions, it took 27 seconds to simulate 10 hours of game time with Omnimod vs 19 seconds without Omnimod. This roughly takes it from running at 10% speed to running at 70% speed (it'll never reach 100% since Omnimod does additional things).

## 1.8.0 (2019-05-12)

### Core Version Update
Omnimod has been updated to work on v0.9.2e. This version supercedes many of Omnimod's features so this release largely serves to document the changes necessary to get Omnimod moving forward again.

- **Monster groups** still work; however, since their animations can't be adjusted during offline progress, they'll appear completely in sync when coming out of the time lapse until the next zone. It may be possible to fix this in the future.

- The option to choose to **automatically gild** or not has been removed since the accompanying UI has been moved from the Automator to the World panel and the core game provides its own option for it now.

- The **gilding panel** and its talents have been removed since the core game now provides ethereal items.

- Omnimod no longer learns your **starter skills** automatically since the core game does it now too.

- **Offline progress** is significantly slower with Omnimod. Some rough testing took 19 seconds to time lapse an hour with Omnimod vs 2 seconds without it under the same conditions. The problem stems from Omnimod's flexible eventing and extension system which makes development significantly simpler and faster to code but apparently even slower to execute.

- Cid's **skills** and **tooltips** went through significant adjustments in the core game and have not been updated in places where Omnimod is overriding the default.


## 1.7.4 (Unreleased)

### Bug Fixes
- Fix storm tooltips to show the proper cooldown.

## 1.7.3 (2019-03-11)

### Bug Fixes
- Fix crash when Clicktorrent kills a zone boss.

## 1.7.2 (2019-03-11)

### Bug Fixes
- Cid will no longer lock up when using Clicktorrent on a boss that survives the final blow.

## 1.7.1 (2019-03-11)

### Bug Fixes
- Golden Clicks will no longer subtract money when dead monsters receive click damage.
- Huge Click's AoE will no longer damage dead monsters.

## 1.7.0 (2019-03-10)

### Automator
- The gems for activating Auto-attackstorm, Clickstorm, Clicktorrent, Critstorm, Energize, Golden Clicks, Huge Click, and Powersurge have been enhanced to only activate if the buff isn't active so that more purposeful stones can be used with them.
- The gem for activating Big Clicks has been enhanced to only activate if the buff isn't active unless Limitless Big Clicks has also been learned.

### Gilding
- If the Automator's world end options would take the player to a gilded world, but the Automatically Gild option is turned off, an indicator will display on the left panel and worlds panel to remind the player to manually advance to the gilded world when ready.

## Bug Fixes
- Fixed MultiClick skill tooltip to show cost and cooldown.

## 1.6.3 (2019-03-08)

### Bug Fixes
- Clicktorrent will no longer push corpses, only living monsters.
- Clicktorrent can no longer push any boss monsters. The skill tooltip has been updated to clarify.

## 1.6.2 (2019-03-08)

### Bug Fixes
- Big Clicks, Energize, Huge Click, and Powersurge have had their buff tooltips corrected to reflect the character's stats at the time the skill was used.

## 1.6.1 (2019-03-08)

### Skills
- Clicktorrent tooltip now clarifies that _all_ monsters in the zone are pushed and damaged once it finishes.

### Bug Fixes
- Golden Clicks no longer awards gold greater than the monster's max health in situations where the monster took multiple hits to kill but the final hit was greater than the monster's remaining health.
- All storms now properly refresh their buff if cast while the buff is active rather than adding a stack to the existing buff.
- All storm buffs are now based on the character's stats at the time the skill is used. Previously the stats could change when a stack was gained and the buff's tooltip could change at any time, causing things to be inconsistent and unintuitive.

## 1.6.0 (2019-03-05)

### Skills
- Auto-attackstorm, Clickstorm, Critstorm, and Golden Clicks have had their buff tooltips adjusted to clarify the energy/mana drain is only while clicking/auto-attacking.
- Clickstorm and Critstorm have had their clicks per second increased to 5 per stack (up from 2.5 per stack) for closer parity with planned changes to the core versions.
- Golden Clicks has had its gold bonus reduced to 75% (down from 100%) as its still a bit too powerful.
- Clicktorrent redesigned
  - Performs 50 clicks per second for 5 seconds or until you run out of energy. Once finished, monsters are pushed back 1 yard and damaged a random amount up to 100% of the damage Clicktorrent did.

> **Comments:** The new Clicktorrent should serve as a high cost/high reward burst of damage. It also provides some incentive to increase max energy and pairs nicely with the Zone Start stone. The pushback is mostly to serve as a visual indicator that was less effort than shooting a projectile down the lane.

## 1.5.1 (2019-02-25)

### Skills
- Fixed a typo in auto-attackstorm's buff to say say "mana per second" (was "mana per auto-attack") -- the number was still correct.
- Reduced Golden Clicks bonus to 100% per stack (was 500% per stack)

> **Comments:** I realized that outside of my initial testing, Golden Clicks was massively overpowered at 500%. The problem with Golden Clicks compared to Critstorm is that when the Critstorm expires, there's no long-term benefit.. but the gold earned from Golden Clicks is re-invested in gear, which brings more damage, which brings more gold from Golden Clicks. Golden Clicks essentially has 3 multipliers: the gold bonus, the attack speed increasing the per-second yield, and the gear purchases increasing damage to increase the per-hit yield. With this change, Critstorm will outperform Golden Clicks in short bursts, but Golden Clicks will win over time so it's better for pushing harder content.

## 1.5.0 (2019-02-24)

### Gilding
- The Gild Now button has been removed from the Gilding panel.
- The Automator has a new option for Attempt Next World and Attempt Highest World to specify if those automations can continue to gilded worlds. This option becomes available once unlocking the first gild.
- Players are now given a popup when unlocking their first gild to help explain what to do since it's no longer automatic.

> **Comments:** This change is aimed to bring Omnimod back in alignment with how Gilding works in the core game and allow players to automate gilding if they choose to. Previously, players had to be actively playing in order to manually gild, which may generally be desirable but not always. The option is off by default to maintain previous Omnimod behavior since it seems to be the more expected behavior and several players have been confused when hitting world 31 since the core game would gild automatically.

### Helpful Adventurer

#### Skills
- Auto-attackstorm, Clickstorm, Critstorm, Golden Clicks, Clicktorrent
  - Only drain energy/mana when actively clicking/auto-attacking.
  - Tooltips improved for clarity.
  - Buff now shows stacks in addition to time remaining until next stack.

> **Comments:** It didn't feel great that a huge storm could die during the time it took to walk to a boss because your resources were rapidly depleting. This change aims to make that time feel better by allowing the storm to continue stacking while Cid takes a breather. In addition, players can manually pause Cid (spacebar) to build stacks or wait for resource cooldowns -- this tactic also provides amazing synergy with Sleeper Cells to really give that sense that Cid is charging up to unleash something powerful. Players that master Sleeper Cell usage will be able to really maximize their storm uptime and stacks to deliver more damage than previously possible.

- Golden Clicks redesigned
  - Performs 2.5 clicks per second until you run out of energy. While active, all clicks award bonus gold based on the percent of damage done to the monster's health. Total bonus gold is worth 500% of the monster's total worth. Speed and total bonus gold increase every 60 seconds.
- Critstorm redesigned
  - Performs 2.5 clicks per second until you run out of energy. Increases crit damage by 50%. Speed and crit damage increase every 60 seconds.

> **Comments:** Critstorm's 100% crit chance was a little overpowered so this change may be a nerf for players relying on that, but this change is aimed to make Critstorm scale better with other mechanics like Critical Powersurge and stacking crit chance in the skill tree and on equipment. Using Critstorm by itself should still see a power increase over its Clickstorm predecessor. As for Golden Clicks, its value was completely unpredictable as it could provide a nice boost when already killing things but had no benefit at all when not killing things like harder bosses. These changes combined should make both options appealing. Critstorm is still more powerful in most circumstances, but Golden Clicks will shine when it helps reaching big equipment multipliers or when paired with proper boosts or hitting treasure chests (particularly treasure chest bosses). Also, the benefits of Golden Clicks continues after the ability wears off thanks to its gold bonus equating to new equipment purchases. Combining the two should also provide enticing gameplay.

### User Interface
- Most skills have received coloring to their buff tooltips and skill tree nodes.

### Bug Fixes
- Auto-attackstorm no longer continues to auto-attack when paused (spacebar) within attack range of a monster.
- Critstorm's buff icon now uses the green variant in order to match the skill icon (was blue).

## 1.4.0 (2019-02-16)

### Gild Talents
- Sleeper Cells
  - Increased energy regeneration to 1000% per second (up from 200% per second).
  - Increased mana regeneration to 2000% per second (up from 200% per second).
  - Clarified tooltip that it activates while paused.

### Helpful Adventurer

#### Automator Tree
- After first gild, additional connections will unlock to allow skipping sets since having too many can break the UI. Some speed upgrades will also be skippable. This change is experimental and largely to workaround the UI bug.

#### Gilding
- Cid automatically learns the first 5 nodes in the skill tree after gilding.
- Gilds no longer award bonus skill points per gild, all gilds award the base 6 points (leaving 1 point to choose what you want since the first 5 nodes are auto-purchased).
- Deluxe blue nodes learned in the skill tree now stay unlocked even after gilding to provide faster ramp-up and more build opportunities. Only works with gilds that have occurred with Omnimod running.

### Ruby Shop
- The shop's options have been reworked to use a weight scale rather than a priority-based system (see comments for more details).
- The Ruby Shop timers no longer speed up from lack of sales, Rufus will come once every 25 minutes and remain for 5 minutes.
- The Ruby Shop no longer leaves when a boss fight starts and can come during boss fights.
- Ancient Shards now cost 100 rubies (up from 50 rubies).
- Ancient Shards, Skill Points, and Automator Points no longer have an internal cooldown.
- Gild Talent Reset has been added as a purchase option with a weight of 2 and a price of 100 rubies.

> **Comments:** The more frequent visits weren't useful when there was still nothing worth purchasing. These changes are designed to make (nearly) every purchase have some value, even if it's not immediately apparent.  For a lengthy read on way too many details, lots of math, and my reasons for the change, see my [Ruby Shop Mechanics](https://gitlab.com/michaeldrotar/clicker-heroes-2-omnimod/wikis/Ruby-Shop-Mechanics) article.

### Bug Fixes
- Fixed Powersurge's auto-attack AOE to center around Cid, not the target. Overall this should act as a buff and allows the Charge talent to get Cid in closer to hit more monsters as originally intended.

## 1.3.1 (2019-02-11)

### Bug Fixes
- Purchasing an automator point from the ruby shop now properly refreshes the automator tree display.
- Fixed Killing Frenzy, Discharge, and Gift of Chronos to work with Omnimod.

## 1.3.0 (2019-02-07)

### Monsters
- Monsters now spawn in randomized group formations.

> **Comments:** Long-term, each zone will randomly choose between a few different monster flavors: groups, solo monsters, moving monsters, etc. In addition to visual interest, this helps prevent the game from bugging out during huge clickstorm runs or having floating clickables a mile behind Cid because she's dashing through so quickly.

### Helpful Adventurer

#### Skills
- Huge Click
  - Now also deals 50% as much damage to monsters within 3 meters.
- Powersurge
  - Auto-attacks during Powersurge now deal 50% auto-attack damage to monsters within 3 meters.

### User Interface
- Skill tooltips have had color treatments applied to their text: damage = red, energy = yellow, mana = blue, other values = green, labels = gray.
- Skill tooltips now display all numbers with a consistent amount of precision up to 2 decimal places.
  - Long cooldowns will now display in terms of minutes instead of seconds.
- Skill tooltips have added a Cost label
- Some tooltips have been slightly reworded to better support future localization

### Bug Fixes
- If a killing blow triggers Shadow Strikes, it'll hit the next living monster rather than skip to the second living monster.

## 1.2.0 (2019-02-02)

### Gilding
- Gilding is now available after beating each 30th world. Players cannot continue onto the next world until they choose to gild. This gives players the chance to continue enjoying their build or to spend automator points before losing them.
- Gilding no longer drains previous worlds of their xp, players may have to grind on gilded worlds a little longer than usual to get powerful enough to continue, but this will also provide more skill points to utilize.
- Gild talents are now available in tiers. Tiers unlock based on gild level, and each tier allows you to choose one of 3 talents. Gilding resets talents so that new ones may be chosen.

### Gild Talents
- Sleeper Cells added
  - Increases energy and mana regeneration when paused by 200%.
- Charge added
  - Increases damage dealt when dashing by 200% and you move closer to the enemy.
- Shadow Strikes added
  - Grants a 10% chance for a bonus auto-attack against the next enemy.

> **Comments:** Talent power scaling will be starting out relatively low. The goal is to provide more flavor and open up new builds without steamrolling the game. Sleeper Cells may be useful for active players that can hit the spacebar to pause and regen resources to reach higher storm multipliers. Charge should fit well into any click build and the new positioning will play a bigger role with upcoming monster formations and splash damage. Shadow Strikes should help boost auto-attack builds by providing more damage and energy.

### System
- Each version of Omnimod will now only run on a specific version of Clicker Heroes 2. Version mismatches will result in a dialog prompt to check the repo for a newer version.

> **Comments:** One of the fundamental tenants of this mod is to not do anything that would permanently break core functionality if the mod were removed, core characters should continue to load and play as normal. To avoid issues, Omnimod uses the most minimal amount of functionality possible in order to do a version check and if it fails it only loads the minimal amount necessary to show a popup to let the user know about it. This will allow me time to test Omnimod on newer versions once they're released and add any migration code necessary to avoid breaking things for users, and it'll force me to regularly test that removing Omnimod doesn't permanently break any core functionality.

## 1.1.1 (2019-01-19)

### Bug Fixes
- Fix Ruby Shop to wait some time between visits even at max speed.

## 1.1.0 (2019-01-19)

### Core

#### Characters
- Characters now receive an additional 4 skill tree points per gild after the first, the first still grants 6 points.

#### Ruby Shop
- Magical Brew can now fill mana past its cap.
- Magical Brew quantity is limited to however many it would take to fill mana from 0.
- Energy drinks can now be purchased as well and function the same as Magical Brew but for refilling energy (currently they also share an icon because icons are not moddable -- be sure to read the name).
- Skill points can now be purchased for 100 rubies on a 4 hour cooldown similar to Automator point purchases.
- The Ruby Shop timers have been reset to their core defaults, appearing once every 25 minutes (up from 1 minute) and remaining for 5 minutes (up from 30 seconds).
- Each time the Ruby Shop fails to make a sale, it will return in less time and will eventually start leaving sooner than 5 minutes (minimum of 1 minute) in order to come back faster with new choices.

> **Comments:** Hopefully the shop timer changes should help idle players find a good purchase or two in short time while active players are hassled less. Better choices should continue coming to make it feel good to spend rubies and hopefully open up some new strategies.

## 1.0.0 (2019-01-12)

### Core

#### Characters
- Characters now continue to gain experience after level 50 when fighting lower level monsters. (NOTE: The UI warning is still in place because it's not yet moddable.)

#### Ruby Shop
- The Ruby Shop will now appear every minute, down from 25 minutes.
- The Ruby Shop will now last for 30 seconds, down from 5 minutes. (NOTE: The time display is not yet moddable so still shows 5 minutes.)
- The Ruby Shop will no longer have empty choices or repeated choices.

> **Comments:** When you're playing idle and you have a couple minutes to pop in and spend points, it feels bad to wonder if that next ruby shop is just another minute away or 20 minutes away. When it finally does appear, it feels bad to get bad choices, especially since there's only 3 of them. These changes are aimed to improve the choices and lessen the sting of bad choices by having new choices more often.
