#!/bin/bash
set -e # abort on error

if [ -z "${ClickerHeroes2Path+x}" ]
then
  echo "Must set ClickerHeroes2Path environment variable before proceeding..."
  exit
fi

start=`date +%s`

(
  set -x # echo following commands
  npx mxmlc "src/OmnimodMain.as" -o "Omnimod.swf" -l "${ClickerHeroes2Path}/CH2Library.swc" -debug=true -swf-version=36 -static-link-runtime-shared-libraries
)

end=`date +%s`

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
echo -e "\033[0;32mFinished in `expr $end - $start` seconds.\033[0m"
