#!/bin/bash
set -e # abort on error

if [ -z "${ClickerHeroes2Path+x}" ]
then
  echo "Must set ClickerHeroes2Path environment variable before proceeding..."
  exit
fi

set +e
TaskKill //IM ClickerHeroes2.exe //F
set -e

nohup "${ClickerHeroes2Path}\ClickerHeroes2.exe" &>/dev/null &
