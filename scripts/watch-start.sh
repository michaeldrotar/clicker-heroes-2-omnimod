#!/bin/bash
set -e # abort on error

if [ -z "${ClickerHeroes2Path+x}" ]
then
  echo "Must set ClickerHeroes2Path environment variable before proceeding..."
  exit
fi

npx nodemon -w "${ClickerHeroes2Path}\\mods\\active" -w "." -e "sh,swf,txt" -x "npm run restart"
